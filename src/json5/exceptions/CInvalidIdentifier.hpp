#pragma once

#include "CTextException.hpp"

namespace json5
{
  class CInvalidIdentifier : public CTextException
  {
  public:

    /**
     * Constructor
     * @param iLine Line
     * @param iColumn Column
     */
    CInvalidIdentifier(int iLine, int iColumn);
  };
}
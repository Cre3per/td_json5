#pragma once

#include "CTextException.hpp"

namespace json5
{
  class CInvalidEncoding : public CTextException
  {
  public:

    /**
     * Constructor
     * @param iLine Line
     * @param iColumn Column
     */
    CInvalidEncoding(int iLine, int iColumn, const char *szMessage = nullptr);
  };
}
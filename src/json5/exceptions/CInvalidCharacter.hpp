#pragma once

#include "CTextException.hpp"

namespace json5
{
  class CInvalidCharacter : public CTextException
  {
  public:

    /**
     * Constructor
     * @param ch The character that caused the exception
     * @param iLine Line
     * @param iColumn Column
     */
    CInvalidCharacter(char32_t ch, int iLine, int iColumn);
  };
}
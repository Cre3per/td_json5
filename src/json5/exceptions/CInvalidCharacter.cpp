#include "CInvalidCharacter.hpp"

#include <codecvt>
#include <locale>
#include <sstream>

namespace json5
{
  /**
   * 
   */
  CInvalidCharacter::CInvalidCharacter(char32_t ch, int iLine, int iColumn)
  {
    std::stringstream ss{ };

    if (ch == 0)
    {
      ss << "JSON5: invalid end of input";
    }
    else
    {
      ss << "JSON5: invalid character ";
      static std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> conv32{ };
      ss << conv32.to_bytes(ch);
    }

    ss << " at " << iLine << ":" << iColumn;

    this->m_strMessage = ss.str();
  }
}
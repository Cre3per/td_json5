#pragma once

#include "CJSONValue.hpp"

#include <cstdint>

namespace json5
{
  // Must have NaN and Infinity values and be constructible from long double.
  using number_type = long double;

  class CJSONNumber : public CJSONValue
  {
  public:
    number_type value{ 0.0 };

  public:
    using CJSONValue::toString;

    /**
     * 
     */
    virtual std::string toString(const CFormatOptions &fmt) const override;

  public:
    /**
     * Default constructor
     */
    CJSONNumber(void);

    /**
     * Constructor
     */
    CJSONNumber(number_type dbValue);

    /**
     * 
     */
    virtual ~CJSONNumber(void);
  };
}
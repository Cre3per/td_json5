#include "CInvalidTypeConversion.hpp"

#include "json5/values/CJSONArray.hpp"
#include "json5/values/CJSONBoolean.hpp"
#include "json5/values/CJSONNumber.hpp"
#include "json5/values/CJSONString.hpp"
#include "json5/values/CJSONObject.hpp"

#include <codecvt>
#include <locale>
#include <sstream>

namespace json5
{
  /**
   * 
   */
  CInvalidTypeConversion::CInvalidTypeConversion(JSONValueType from, JSONValueType to)
  {
    std::stringstream ss{};

    ss << "JSON5: invalid conversion from " << CJSONValue::typeName(from) << " to " << CJSONValue::typeName(to);

    this->m_strMessage = ss.str();
  }

  /**
   * 
   */
  template <class TTo>
  CInvalidTypeConversion CInvalidTypeConversion::create(JSONValueType from)
  {
    JSONValueType toType{ JSONValueType::kUnknown };

    if (typeid(TTo) == typeid(CJSONBoolean))
    {
      toType = JSONValueType::kBoolean;
    }
    else if (typeid(TTo) == typeid(CJSONNumber))
    {
      toType = JSONValueType::kNumber;
    }
    else if (typeid(TTo) == typeid(CJSONString))
    {
      toType = JSONValueType::kString;
    }
    else if (typeid(TTo) == typeid(CJSONArray))
    {
      toType = JSONValueType::kArray;
    }
    else if (typeid(TTo) == typeid(CJSONObject))
    {
      toType = JSONValueType::kObject;
    }

    return CInvalidTypeConversion{ from, toType };
  }

  template CInvalidTypeConversion CInvalidTypeConversion::create<CJSONBoolean>(JSONValueType);
  template CInvalidTypeConversion CInvalidTypeConversion::create<CJSONNumber>(JSONValueType);
  template CInvalidTypeConversion CInvalidTypeConversion::create<CJSONString>(JSONValueType);
  template CInvalidTypeConversion CInvalidTypeConversion::create<CJSONArray>(JSONValueType);
  template CInvalidTypeConversion CInvalidTypeConversion::create<CJSONObject>(JSONValueType);
  template CInvalidTypeConversion CInvalidTypeConversion::create<CJSONValue>(JSONValueType);
}
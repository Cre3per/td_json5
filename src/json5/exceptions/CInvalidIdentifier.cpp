#include "CInvalidIdentifier.hpp"

#include <sstream>

namespace json5
{
  /**
   * 
   */
  CInvalidIdentifier::CInvalidIdentifier(int iLine, int iColumn)
  {
    std::stringstream ss{};

    ss << "JSON5: invalid identifier at " << iLine << ":" << iColumn;

    this->m_strMessage = ss.str();
  }
}
#include "json5/json.hpp"
#include "json5/tests/test.hpp"
#include "json5/util.hpp"

#include <codecvt>
#include <locale>

#include <iostream>

#include <cassert>

int main(int argc, char *argv[])
{
  json5::test::run();

  return 0;

  json5::CFormatOptions fmt{};

  fmt.bArrayValueOnNewLine = true;
  fmt.bObjectSpaceAfterColon = true;
  fmt.bObjectSpaceAfterComma = true;
  fmt.bObjectPairsOnNewLine = true;

  json5::let value{ json5::JSON.parse("[ 1, 2, 3, \'Hello World\', false, [ \'sub\', \'array\', { sub: \'object\', yes: \'please\', child: { mega:\'object\'} } ] ]") };

  std::printf("%s\n", value->toString(fmt).c_str());

  return 0;
}
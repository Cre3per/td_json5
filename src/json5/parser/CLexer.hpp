#pragma once

#include "CToken.hpp"
#include "state.hpp"

#include "json5/unicode/CUString.hpp"
#include "json5/values/CJSONValue.hpp"

#include <memory>
#include <string>

namespace json5
{
  class CStringStream;
  class CParser;

  class CLexer
  {
  private:
    /**
     * The parser state
     */
    State m_sParser{ State::kInvalid };

    /**
     * The text stream
     */
    CStringStream *m_pInput{ nullptr };

    /**
     * Current state
     */
    State m_state{ State::kDefault };

    /**
     * Buffer used to hold temporary values (Identifier names, numbers, etc.)
     */
    CUString m_strBuffer{ "" };

    /**
     * Whether or not the current parameter name is escaped using double quotes.
     */
    bool m_bDoubleQuote{ false };

    /**
     * Sign of the current number.
     */
    int m_iSign{ 1 };

  private:
    /**
     * Escapes a hexadecimal ASCII sequence of the next 2 characters
     * @return The escaped character
     */
    char32_t escapeHexCharacter(void);

    /**
     * Escapes a hexadecimal unicode sequence of the next 4 characters
     * @return The escaped character
     */
    char32_t escapeUnicodeCharacter(void);

    /**
     * Unescapes a character
     * @param ch The first character to escape
     * @param pSkip Will be set to true if this character should be skipped
     * @return The escape character
     */
    char32_t unescapeCharacter(char32_t ch, bool *pSkip);

    /**
     * Generates a token at the current position
     */
    CToken newToken(TokenType type, std::shared_ptr<CJSONValue> pValue = nullptr);

    /**
     * Extracts @str from @m_pParser.
     * If the @str doesn't match what's extracted from @m_pParser, a
     * {@link CInvalidChar} exception will be thrown.
     */
    void literal(const CUString &str) const;

    /**
     * 
     */
    CToken lexDefault(char32_t ch);

    /**
     * 
     */
    CToken lexComment(char32_t ch);

    /**
     * 
     */
    CToken lexCommentMultiLine(char32_t ch);

    /**
     * 
     */
    CToken lexCommentMultiLineAsterisk(char32_t ch);

    /**
     * 
     */
    CToken lexCommentSingleLine(char32_t ch);

    /**
     * 
     */
    CToken lexValue(char32_t ch);

    /**
     * 
     */
    CToken lexIdentifierNameStartEscape(char32_t ch);

    /**
     * 
     */
    CToken lexIdentifierName(char32_t ch);

    /**
     * 
     */
    CToken lexIdentifierNameEscape(char32_t ch);

    /**
     * 
     */
    CToken lexSign(char32_t ch);

    /**
     * 
     */
    CToken lexZero(char32_t ch);

    /**
     * 
     */
    CToken lexDecimalInteger(char32_t ch);

    /**
     * 
     */
    CToken lexDecimalPointLeading(char32_t ch);

    /**
     * 
     */
    CToken lexDecimalPoint(char32_t ch);

    /**
     * 
     */
    CToken lexDecimalFraction(char32_t ch);

    /**
     * 
     */
    CToken lexDecimalExponent(char32_t ch);

    /**
     * 
     */
    CToken lexDecimalExponentSign(char32_t ch);

    /**
     * 
     */
    CToken lexDecimalExponentInteger(char32_t ch);

    /**
     * 
     */
    CToken lexHexadecimal(char32_t ch);

    /**
     * 
     */
    CToken lexHexadecimalInteger(char32_t ch);

    /**
     * 
     */
    CToken lexString(char32_t ch);

    /**
     * 
     */
    CToken lexStart(char32_t ch);

    /**
     * 
     */
    CToken lexBeforePropertyName(char32_t ch);

    /**
     * 
     */
    CToken lexAfterPropertyName(char32_t ch);

    /**
     * 
     */
    CToken lexAfterPropertyValue(char32_t ch);

    /**
     * 
     */
    CToken lexBeforeArrayValue(char32_t ch);

    /**
     * 
     */
    CToken lexAfterArrayValue(char32_t ch);

    /**
     * 
     */
    CToken lexEnd(char32_t ch);

    /**
     * Lexes a character.
     * @ch The peeked character in @m_pInput
     */
    CToken lex(char32_t ch);

  public:
    /**
     * Lexes the next token
     * @param sParser Parser state
     * @return The lexed token
     */
    CToken lex(State sParser);

  public:
    /**
     * Default constructor
     */
    CLexer(void) = delete;

    /**
     * Constructor
     * @param pInput The input stream
     */
    CLexer(CStringStream *pInput);
  };
}
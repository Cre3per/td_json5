#pragma once

#include "exceptions/CInvalidTypeConversion.hpp"

#include <cassert>
#include <memory>
#include <type_traits>

namespace json5
{
  class CJSONValue;

  class CJSONArray;
  class CJSONBoolean;
  class CJSONNumber;
  class CJSONObject;
  class CJSONString;

  template <class T>
  class CSharedPointer
  {
  private:
    /**
     * 
     */
    std::shared_ptr<T> m_ptr{ nullptr };

  public:
    /**
     * True if this pointer has an object attached to it
     */
    inline bool defined(void)
    {
      return this->m_ptr.operator bool();
    }

  public:
    /**
     * @return @m_ptr
     */
    inline std::shared_ptr<T> &get(void)
    {
      return this->m_ptr;
    }

    /**
     * @return @m_ptr
     */
    inline const std::shared_ptr<T> &get(void) const
    {
      return this->m_ptr;
    }

    /**
     * 
     */
    inline T *operator->(void)
    {
      return this->m_ptr.operator->();
    }

    /**
     * 
     */
    inline const T *operator->(void)const
    {
      return this->m_ptr.operator->();
    }

    /**
     * 
     */
    bool operator==(std::nullptr_t &np) const
    {
      return (this->m_ptr == np);
    }

    /**
     *
     */
    operator bool(void) const
    {
      return (this->m_ptr.operator bool());
    }

  public:
    /**
     * 
     */
    CSharedPointer(void)
        : m_ptr{ nullptr }
    {
    }

    /**
     * 
     */
    CSharedPointer(T *p)
        : m_ptr{ p }
    {
    }

    /**
     * 
     */
    CSharedPointer(const CSharedPointer<CJSONValue> &that)
        : m_ptr{ std::dynamic_pointer_cast<T>(that.get()) }
    {
      if (this->m_ptr == nullptr)
      {
        if (that.get() != nullptr)
        {
          throw CInvalidTypeConversion::create<T>(that.get()->getType());
        }
      }
    }

    /**
     * 
     */
    // template <class U, class = std::enable_if_t<!std::is_same_v<CJSONValue, U>>>
    // CSharedPointer(const CSharedPointer<U> &that) = delete;

    /**
     * 
     */
    template <class U, class = std::enable_if_t<!std::is_same_v<CJSONValue, U>>>
    CSharedPointer(const std::shared_ptr<U> &that)
        : m_ptr{ that }
    {
    }

    /**
     * 
     */
    template <class U, class = std::enable_if_t<!std::is_same_v<CJSONValue, U>>>
    CSharedPointer(const CSharedPointer<U> &that)
        : m_ptr{ that.get() }
    {
    }

    /**
     * 
     */
    CSharedPointer(const std::shared_ptr<CJSONValue> &that)
        : m_ptr{ std::dynamic_pointer_cast<T>(that) }
    {
      if (this->m_ptr == nullptr)
      {
        if (that.get() != nullptr)
        {
          throw CInvalidTypeConversion::create<T>(that.get()->getType());
        }
      }
    }
  };
}
#include "CTest.hpp"

#include <iostream>

namespace json5::test
{
  /**
   * 
   */
  bool CTest::run(void)
  {
    bool bSuccess{ this->m_fnTest() };

    std::printf("%s [%s]\n", bSuccess ? "o" : "x", this->m_strName.c_str());

    return bSuccess;
  }

  /**
   * 
   */
  CTest::CTest(const std::string &strName, const std::function<bool(void)> fnTest)
      : m_strName{ strName },
        m_fnTest{ fnTest }
  {

  }
}
#pragma once

namespace json5::test
{
  /**
   * Runs all tests.
   * @return True if all tests were successful
   */
  bool run(void);
}
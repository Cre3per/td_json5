#include "json.hpp"

#include "parser/CParser.hpp"

namespace json5
{
  CJSON JSON{};

  /**
   * 
   */
  var CJSON::parse(const std::string &strSource) const
  {
    CParser parser{};

    return parser.parse(strSource);
  }

  /**
   * 
   */
  std::string CJSON::stringify(var obj) const
  {
    return obj->toString();
  }

  /**
   * 
   */
  std::string CJSON::stringify(var obj, const CFormatOptions &fmt) const
  {
    return obj->toString(fmt);
  }
}
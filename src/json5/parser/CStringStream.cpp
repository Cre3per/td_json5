#include "CStringStream.hpp"

namespace json5
{
  /**
   * 
   */
  bool CStringStream::eof(void) const
  {
    return (this->m_iPosition >= static_cast<int>(this->m_strSource.length()));
  }

  /**
   * 
   */
  char32_t CStringStream::peek(void) const
  {
    if (this->eof())
    {
      return 0;
    }
    else
    {
      return this->m_strSource.at(this->m_iPosition);
    }
  }

  /**
   * 
   */
  char32_t CStringStream::read(void)
  {
    char32_t ch{ this->peek() };

    if (ch == '\n')
    {
      ++this->m_iLine;
    }
    else
    {
      ++this->m_iColumn;
    }

    ++this->m_iPosition;

    return ch;
  }

  /**
   * 
   */
  int CStringStream::getLine(void) const
  {
    return this->m_iLine;
  }

  /**
  * 
  */
  int CStringStream::getColumn(void) const
  {
    return this->m_iColumn;
  }

  /**
   * 
   */
  CUString CStringStream::excerpt(int iLeading, int iTrailing) const
  {
    CUString strExcerpt{ "" };

    for (int i{ std::max(0, this->m_iPosition - iLeading) }; i < this->m_iPosition; ++i)
    {
      strExcerpt.append(this->m_strSource.at(i));
    }

    strExcerpt.append("[");
    strExcerpt.append(this->m_strSource.at(this->m_iPosition));
    strExcerpt.append("]");

    for (int i{ std::min(this->m_strSource.length(), this->m_iPosition + iTrailing) }; i > this->m_iPosition; --i)
    {
      strExcerpt.append(this->m_strSource.at(i));
    }

    return strExcerpt;
  }

  /**
   * 
   */
  void CStringStream::reset(const CUString &strText)
  {
    this->m_iColumn = 1;
    this->m_iLine = 1;
    this->m_iPosition = 0;

    this->m_strSource = strText;
  }

  /**
   * 
   */
  CStringStream::CStringStream(void)
  {
  }

  /**
   * 
   */
  CStringStream::CStringStream(const CUString &strText)
      : m_strSource{ strText }
  {
  }
}
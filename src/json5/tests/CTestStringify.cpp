#include "CTestStringify.hpp"

#include "json5/json.hpp"

#include <cmath>

namespace json5::test::stringify
{
  static CFormatOptions s_fmt{};

  /**
   * 
   */
  bool emptyObject(void)
  {
    object value{ std::make_shared<CJSONObject>() };

    return (JSON.stringify(value, s_fmt) == "{}");
  }

  /**
   * 
   */
  bool unquotedPropertyName(void)
  {
    object value{ std::make_shared<CJSONObject>() };

    value->set("a", 1);

    return (JSON.stringify(value, s_fmt) == "{a:1}");
  }

  /**
   * 
   */
  bool singleQuotePropertyName(void)
  {
    object value{ std::make_shared<CJSONObject>() };

    value->set("a-b", 1);

    return (JSON.stringify(value, s_fmt) == "{\'a-b\':1}");
  }

  /**
   * 
   */
  bool emptyPropertyName(void)
  {
    object value{ std::make_shared<CJSONObject>() };

    value->set("", 1);

    return (JSON.stringify(value, s_fmt) == "{\'\':1}");
  }

  /**
   * 
   */
  bool specialCharacterPropertyName(void)
  {
    object value{ std::make_shared<CJSONObject>() };

    value->set("$_", 1);
    value->set("_$", 2);
    value->set(u8"a\u200C", 3);

    return (JSON.stringify(value, s_fmt) == u8"{$_:1,_$:2,a\u200C:3}");
  }

  /**
   * 
   */
  bool unicodePropertyName(void)
  {
    object value{ std::make_shared<CJSONObject>() };

    value->set("ùńîċõďë", 1);

    return (JSON.stringify(value, s_fmt) == "{ùńîċõďë:1}");
  }

  /**
   * 
   */
  bool escapedPropertyNames(void)
  {
    object value{ std::make_shared<CJSONObject>() };

    value->set(CUString{ "\\\b\f\n\r\t\v\0", 9 }, 1);
    CUString strStringified{ JSON.stringify(value, s_fmt) };

    return (strStringified == "{\'\\\\b\\f\\n\\r\\t\\v\\0\':1}");
  }

  /**
   * 
   */
  bool multipleProperties(void)
  {
    object value{ std::make_shared<CJSONObject>() };

    value->set("abc", 1);
    value->set("def", 2);

    return (JSON.stringify(value, s_fmt) == "{abc:1,def:2}");
  }

  /**
   * 
   */
  bool nestedObject(void)
  {
    object value{ std::make_shared<CJSONObject>() };
    CJSONObject nested{  };

    nested.set("b", 2);

    value->set("a", nested);

    return (JSON.stringify(value, s_fmt) == "{a:{b:2}}");
  }

  /**
   * 
   */
  bool emptyArray(void)
  {
    array value{ std::make_shared<CJSONArray>() };

    return (JSON.stringify(value, s_fmt) == "[]");
  }

  /**
   * 
   */
  bool arrayValue(void)
  {
    array value{ std::make_shared<CJSONArray>() };

    value->push(1);

    return (JSON.stringify(value, s_fmt) == "[1]");
  }

  /**
   * 
   */
  bool multipleArrayValues(void)
  {
    array value{ std::make_shared<CJSONArray>() };

    value->push(1);
    value->push(2);

    return (JSON.stringify(value, s_fmt) == "[1,2]");
  }

  /**
   * 
   */
  bool nestedArray(void)
  {
    array value{ std::make_shared<CJSONArray>() };
    CJSONArray nested{ };

    nested.push(2);
    nested.push(3);

    value->push(1);
    value->push(nested);

    return (JSON.stringify(value, s_fmt) == "[1,[2,3]]");
  }

  /**
   * 
   */
  bool stringifyNull(void)
  {
    let value{ std::make_shared<CJSONNull>() };

    return (JSON.stringify(value, s_fmt) == "null");
  }

  /**
   * 
   */
  bool stringifyTrue(void)
  {
    boolean value{ std::make_shared<CJSONBoolean>() };

    value->value = true;

    return (JSON.stringify(value, s_fmt) == "true");
  }

  /**
   * 
   */
  bool stringifyFalse(void)
  {
    boolean value{ std::make_shared<CJSONBoolean>() };

    value->value = false;

    return (JSON.stringify(value, s_fmt) == "false");
  }

  /**
   * 
   */
  bool stringifyNumber(void)
  {
    number value{ std::make_shared<CJSONNumber>() };

    value->value = -1.2;

    return (JSON.stringify(value, s_fmt) == "-1.2");
  }

  /**
   * 
   */
  bool nonFiniteNumbers(void)
  {
    array value{ std::make_shared<CJSONArray>() };

    value->push(std::numeric_limits<number_type>::infinity());
    value->push(-std::numeric_limits<number_type>::infinity());
    value->push(std::numeric_limits<number_type>::quiet_NaN());

    return (JSON.stringify(value, s_fmt) == "[Infinity,-Infinity,NaN]");
  }

  /**
   * 
   */
  bool singleQuoteString(void)
  {
    string value{ std::make_shared<CJSONString>() };

    value->value = "abc";

    return (JSON.stringify(value, s_fmt) == "\'abc\'");
  }

  /**
   * 
   */
  bool escapedString(void)
  {
    string value{ std::make_shared<CJSONString>() };

    value->value = CUString{ "\\\b\f\n\r\t\v\0", 9 };

    return (JSON.stringify(value, s_fmt) == "\'\\\\b\\f\\n\\r\\t\\v\\0\'");
  }

  /**
   * 
   */
  bool escapeSingleQuoteString(void)
  {
    string value{ std::make_shared<CJSONString>() };

    value->value = "\'\"";

    return (JSON.stringify(value, s_fmt) == "\'\\\'\"\'");
  }
  

  /**
   * 
   */
  bool CTestStringify::run(int *pSuccess, int *pFail)
  {
    s_fmt.bSpaceBeforeFirstArrayValue = false;
    s_fmt.bSpaceAfterLastArrayValue = false;
    s_fmt.bSpaceAfterArrayComma = false;
    s_fmt.bArrayTrailingComma = false;
    s_fmt.bArrayValueOnNewLine = false;
    s_fmt.bObjectPairsOnNewLine = false;
    s_fmt.bObjectSpaceAfterColon = false;
    s_fmt.bObjectSpaceAfterComma = false;
    s_fmt.bObjectSpaceBeforeFirstPair = false;
    s_fmt.bObjectSpaceAfterLastPair = false;
    s_fmt.chKeyQuotationMark = '\x00';
    s_fmt.chQuotationMark = '\'';

    this->addTest({ "parses empty objects", emptyObject });
    this->addTest({ "stringifies unquoted property names", unquotedPropertyName });
    this->addTest({ "stringifies single quoted string property names", singleQuotePropertyName });
    this->addTest({ "stringifies empty string property names", emptyPropertyName });
    this->addTest({ "stringifies special character property names", specialCharacterPropertyName });
    this->addTest({ "stringifies unicode property names", unicodePropertyName });
    this->addTest({ "stringifies escaped property names", escapedPropertyNames });
    this->addTest({ "stringifies multiple properties", multipleProperties });
    this->addTest({ "stringifies nested objects", nestedObject });
    this->addTest({ "stringifies empty arrays", emptyArray });
    this->addTest({ "stringifies array values", arrayValue });
    this->addTest({ "stringifies multiple array values", multipleArrayValues });
    this->addTest({ "stringifies nested arrays", nestedArray });
    this->addTest({ "stringifies nulls", stringifyNull });
    this->addTest({ "stringifies true", stringifyTrue });
    this->addTest({ "stringifies false", stringifyFalse });
    this->addTest({ "stringifies numbers", stringifyNumber });
    this->addTest({ "stringifies non-finite numbers", nonFiniteNumbers });
    this->addTest({ "stringifies single quoted strings", singleQuoteString });
    this->addTest({ "stringifies escaped characters", escapedString });
    this->addTest({ "stringifies escaped single quotes", escapeSingleQuoteString });

    return CTestHost::run(pSuccess, pFail);
  }

  /**
   * 
   */
  CTestStringify::~CTestStringify(void)
  {
  }
}
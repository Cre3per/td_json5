#pragma once

#include "CTest.hpp"

#include <vector>

namespace json5::test
{
  class CTestHost
  {
  private:

    /**
     * Tests
     */
    std::vector<CTest> m_vTests{ };

  protected:

    /**
     * Registers a new test
     * @param test The test
     */
    void addTest(const CTest &test);

  public:

    /**
     * Runs all child tests.
     * Inheriting children should set up tests and return CTestHost::run.
     * @param pSuccess Will be set to the number of successful tests
     * @param pFail Will be set to the number of failed tests
     * @return True if all tests were successful
     */
    virtual bool run(int *pSuccess, int *pFail);

    /**
     * Destructor
     */
    virtual ~CTestHost(void);
  };
}
#pragma once

#include "CJSONBoolean.hpp"
#include "CJSONNumber.hpp"
#include "CJSONString.hpp"
#include "CJSONValue.hpp"

#include "json5/CSharedPointer.hpp"
#include "json5/unicode/CUString.hpp"

#include <map>
#include <memory>
#include <type_traits>

namespace json5
{
  class CJSONArray;
  class CJSONObject;

  template <class TKey>
  class CJSONContainer : public CJSONValue
  {
  public:
    /**
     * 
     */
    std::map<TKey, std::shared_ptr<CJSONValue>> value{};

  private:
    /**
     * @return Element at @key or a nullptr
     */
    template <class T>
    std::shared_ptr<T> get(const TKey &key)
    {
      if (auto obj{ this->get(key) })
      {
        return std::dynamic_pointer_cast<T>(obj);
      }
      else
      {
        return nullptr;
      }
    }

  public:
    /**
     * @return Element at @key or a nullptr
     */
    CSharedPointer<CJSONValue> get(const TKey &key)
    {
      auto found = this->value.find(key);

      if (found == this->value.end())
      {
        return nullptr;
      }
      else
      {
        return found->second;
      }
    }

    /**
     * @return Boolean at @key
     */
    CSharedPointer<CJSONBoolean> getBoolean(const TKey &key)
    {
      return this->get(key);
    }

    /**
     * @return Number at @key
     */
    CSharedPointer<CJSONNumber> getNumber(const TKey &key)
    {
      return this->get(key);
    }

    /**
     * @return String at @key
     */
    CSharedPointer<CJSONString> getString(const TKey &key)
    {
      return this->get(key);
    }

    /**
     * @return Array at @key
     */
    CSharedPointer<CJSONArray> getArray(const TKey &key)
    {
      return this->get(key);
    }

    /**
     * @return Object at @key
     */
    CSharedPointer<CJSONObject> getObject(const TKey &key)
    {
      return this->get(key);
    }

    /**
     * Adds or updates a generic object
     */
    void set(const TKey &key, std::shared_ptr<CJSONValue> value)
    {
      this->value[key] = value;
    }

    /**
     * Adds or updates a boolean
     */
    template <class T>
    void set(const TKey &key, T bValue, typename std::enable_if_t<std::is_same_v<bool, std::remove_cv_t<T>>> * = nullptr)
    {
      auto ptr{ std::make_shared<CJSONBoolean>() };

      ptr->value = bValue;

      this->value[key] = ptr;
    }

    /**
     * Adds or updates a number
     */
    template <class T>
    void set(const TKey &key, T dbValue, typename std::enable_if_t<std::is_arithmetic_v<T> && !std::is_same_v<bool, std::remove_cv_t<T>>> * = nullptr)
    {
      auto ptr{ std::make_shared<CJSONNumber>() };

      ptr->value = dbValue;

      this->value[key] = ptr;
    }

    /**
     * Adds or updates a string
     */
    void set(const TKey &key, const std::string &strValue)
    {
      auto ptr{ std::make_shared<CJSONString>() };

      ptr->value = strValue;

      this->value[key] = ptr;
    }

    /**
     * Adds or updates an array
     */
    void set(const TKey &key, const CJSONArray &arrValue)
    {
      auto ptr{ std::make_shared<CJSONArray>(arrValue) };

      this->value[key] = ptr;
    }

    /**
     * Adds or updates an object
     */
    void set(const TKey &key, const CJSONObject &jsValue)
    {
      auto ptr{ std::make_shared<CJSONObject>(jsValue) };

      this->value[key] = ptr;
    }

    /**
     * 
     */
    virtual ~CJSONContainer(void)
    {
    }
  };
}
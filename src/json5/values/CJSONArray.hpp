#pragma once

#include "CJSONContainer.hpp"
#include "CJSONObject.hpp"

#include <cstdint>

namespace json5
{
  class CJSONArray : public CJSONContainer<std::int64_t>
  {
  public:
    using CJSONValue::toString;
    
    /**
     * 
     */
    virtual std::string toString(const CFormatOptions &fmt) const override;

  public:
    /**
     * @return max(Highest element index + 1, 0)
     */
    std::int64_t length(void) const;

    /**
     * Adds a generic value
     * @param value The value to add
     */
    void push(std::shared_ptr<CJSONValue> value);

    /**
     * Adds a boolean
     */
    template <class T>
    void push(T bValue, typename std::enable_if_t<std::is_same_v<bool, std::remove_cv_t<T>>> * = nullptr)
    {
      auto ptr{ std::make_shared<CJSONBoolean>() };

      ptr->value = bValue;

      this->value[this->length()] = ptr;
    }

    /**
     * Adds a number
     */
    template <class T>
    void push(T dbValue, typename std::enable_if_t<std::is_arithmetic_v<T> && !std::is_same_v<bool, std::remove_cv_t<T>>> * = nullptr)
    {
      auto ptr{ std::make_shared<CJSONNumber>() };

      ptr->value = dbValue;

      this->value[this->length()] = ptr;
    }

    /**
     * Adds a string
     */
    void push(const std::string &strValue)
    {
      auto ptr{ std::make_shared<CJSONString>() };

      ptr->value = strValue;

      this->value[this->length()] = ptr;
    }

    /**
     * Adds an array
     */
    void push(const CJSONArray &arrValue)
    {
      auto ptr{ std::make_shared<CJSONArray>(arrValue) };

      this->value[this->length()] = ptr;
    }

    /**
     * Adds an object
     */
    void push(const CJSONObject &jsValue)
    {
      auto ptr{ std::make_shared<CJSONObject>(jsValue) };

      this->value[this->length()] = ptr;
    }

  public:
    /**
     * Destructor
     */
    virtual ~CJSONArray(void);
  };
}
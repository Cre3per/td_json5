#include "CTestErrors.hpp"

#include "json5/exceptions/CInvalidCharacter.hpp"
#include "json5/exceptions/CInvalidIdentifier.hpp"

#include "json5/json.hpp"

namespace json5::test::errors
{

  /**
   * 
   */
  bool emptyDocument(void)
  {
    try
    {
      JSON.parse("");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool onlyComment(void)
  {
    bool bCaught{ true };

    try
    {
      JSON.parse("// single line comment");
    }
    catch (const CInvalidCharacter &)
    {
      bCaught = true;
    }
    catch (...)
    {
    }

    if (bCaught)
    {
      bCaught = false;
    }
    else
    {
      return false;
    }

    try
    {
      JSON.parse("/* multi line comment */");
    }
    catch (const CInvalidCharacter &)
    {
      bCaught = true;
    }
    catch (...)
    {
    }

    return bCaught;
  }

  /**
   * 
   */
  bool incompleteSingleLineComment(void)
  {
    try
    {
      JSON.parse("/a");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool unterminatedMultilineComment(void)
  {
    try
    {
      JSON.parse("/**");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool invalidCharacterInValue(void)
  {
    try
    {
      JSON.parse("a");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool invalidIdentifierStartEscape(void)
  {
    try
    {
      JSON.parse("{ \\a: 1 }");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool invalidIdentifierStartCharacter(void)
  {
    try
    {
      JSON.parse("{ \\u0021: 1 }");
    }
    catch (const CInvalidIdentifier &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool invalidIdentifierContinueEscape(void)
  {
    try
    {
      JSON.parse("{ a\\a: 1 }");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool invalidIdentifierContinueCharacter(void)
  {
    try
    {
      JSON.parse("{ a\\u0021: 1 }");
    }
    catch (const CInvalidIdentifier &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool invalidSignFollowCharacter(void)
  {
    try
    {
      JSON.parse("-a");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool invalidDecimalPointFollowCharacter(void)
  {
    try
    {
      JSON.parse(".a");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool invalidExponentFollowCharacter(void)
  {
    try
    {
      JSON.parse("1ea");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool invalidExponentSignFollowCharacter(void)
  {
    try
    {
      JSON.parse("1e-a");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool invalidHexFollowCharacter(void)
  {
    try
    {
      JSON.parse("0xg");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool invalidStringLineFeed(void)
  {
    try
    {
      JSON.parse("\"\n\"");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool unterminatedString(void)
  {
    try
    {
      JSON.parse("\"");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool invalidPropertyIdentifierStartCharacter(void)
  {
    try
    {
      JSON.parse("{ !: 1 }");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool invalidPropertyNameFollowCharacter(void)
  {
    try
    {
      JSON.parse("{ a! 1 }");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool invalidPropertyValueFollowCharacter(void)
  {
    try
    {
      JSON.parse("{ a: 1! }");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool invalidArrayValueFollowCharacter(void)
  {
    try
    {
      JSON.parse("[ 1! ]");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool invalidLiteralCharacter(void)
  {
    try
    {
      JSON.parse("fa!se");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool unterminatedEscape(void)
  {
    try
    {
      JSON.parse("\"\\");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool invalidHexEscapeStartCharacter(void)
  {
    try
    {
      JSON.parse("\"\\xg\"");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool invalidHexEscapeSecondCharacter(void)
  {
    try
    {
      JSON.parse("\"\\x0g\"");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool invalidUnicodeEscape(void)
  {
    try
    {
      JSON.parse("\"\\u000g\"");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool invalidNonZeroEscape(void)
  {
    for (int i{ 1 }; i <= 9; ++i)
    {
      bool bCaught{ false };

      try
      {
        JSON.parse(std::string{ "\"\\" } + std::to_string(i) + "\"");
      }
      catch (const CInvalidCharacter &)
      {
        bCaught = true;
      }
      catch (...)
      {
      }

      if (!bCaught)
      {
        return false;
      }
    }

    return true;
  }

  /**
   * 
   */
  bool octalEscape(void)
  {
    try
    {
      JSON.parse("\\01");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool multipleValues(void)
  {
    try
    {
      JSON.parse("1 2");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool escapedControlCharacter(void)
  {
    try
    {
      JSON.parse("\x01");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool unterminatedObjectNoProperties(void)
  {
    try
    {
      JSON.parse("{");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool unterminatedObjectNoColon(void)
  {
    try
    {
      JSON.parse("{ a");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool unterminatedObjectNoPropertyValue(void)
  {
    try
    {
      JSON.parse("{ a:");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool unterminatedObject(void)
  {
    try
    {
      JSON.parse("{ a: 1");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool unterminatedArrayNoValues(void)
  {
    try
    {
      JSON.parse("[");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool unterminatedArrayValue(void)
  {
    try
    {
      JSON.parse("[ 1");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool unterminatedArrayValueComma(void)
  {
    try
    {
      JSON.parse("[ 1, ");
    }
    catch (const CInvalidCharacter &)
    {
      return true;
    }
    catch (...)
    {
    }

    return false;
  }

  /**
   * 
   */
  bool CTestErrors::run(int *pSuccess, int *pFail)
  {
    this->addTest({ "throws on empty documents", emptyDocument });
    this->addTest({ "throws on documents with only comments", onlyComment });
    this->addTest({ "throws on incomplete single line comments", incompleteSingleLineComment });
    this->addTest({ "throws on unterminated multiline comment", unterminatedMultilineComment });
    this->addTest({ "throws on invalid characters in values", invalidCharacterInValue });
    this->addTest({ "throws on invalid characters in identifier start escapes", invalidIdentifierStartEscape });
    this->addTest({ "throws on invalid identifier start characters", invalidIdentifierStartCharacter });
    this->addTest({ "throws on invalid characters in identifier continue escapes", invalidIdentifierContinueEscape });
    this->addTest({ "throws on invalid identifier continue characters", invalidIdentifierContinueCharacter });
    this->addTest({ "throws on invalid characters following a sign", invalidSignFollowCharacter });
    this->addTest({ "throws on invalid characters following a leading decimal point", invalidDecimalPointFollowCharacter });
    this->addTest({ "throws on invalid characters following an exponent indicator", invalidExponentFollowCharacter });
    this->addTest({ "throws on invalid characters following an exponent sign", invalidExponentSignFollowCharacter });
    this->addTest({ "throws on invalid characters following a hexadecimal indicator", invalidHexFollowCharacter });
    this->addTest({ "throws on invalid new lines in strings", invalidStringLineFeed });
    this->addTest({ "throws on unterminated strings", unterminatedString });
    this->addTest({ "throws on invalid identifier start characters in property names", invalidPropertyIdentifierStartCharacter });
    this->addTest({ "throws on invalid characters following a property name", invalidPropertyNameFollowCharacter });
    this->addTest({ "throws on invalid characters following a property value", invalidPropertyValueFollowCharacter });
    this->addTest({ "throws on invalid characters following an array value", invalidArrayValueFollowCharacter });
    this->addTest({ "throws on invalid characters in literals", invalidLiteralCharacter });
    this->addTest({ "throws on unterminated escapes", unterminatedEscape });
    this->addTest({ "throws on invalid first digits in hexadecimal escapes", invalidHexEscapeStartCharacter });
    this->addTest({ "throws on invalid second digits in hexadecimal escapes", invalidHexEscapeSecondCharacter });
    this->addTest({ "throws on invalid unicode escapes", invalidUnicodeEscape });
    this->addTest({ "throws on escaped digits other than 0", invalidNonZeroEscape });
    this->addTest({ "throws on octal escapes", octalEscape });
    this->addTest({ "throws on multiple values", multipleValues });
    this->addTest({ "throws with control characters escaped in the message", escapedControlCharacter });
    this->addTest({ "throws on unclosed objects before property names", unterminatedObjectNoProperties });
    this->addTest({ "throws on unclosed objects after property names", unterminatedObjectNoColon });
    this->addTest({ "throws on unclosed objects before property values", unterminatedObjectNoPropertyValue });
    this->addTest({ "throws on unclosed objects after property values", unterminatedObject });
    this->addTest({ "throws on unclosed arrays before values", unterminatedArrayNoValues });
    this->addTest({ "throws on unclosed arrays after values", unterminatedArrayValue });
    this->addTest({ "throws on unclosed arrays after values with comma", unterminatedArrayValueComma });

    return CTestHost::run(pSuccess, pFail);
  }

  /**
   * 
   */
  CTestErrors::~CTestErrors(void)
  {

  }
}
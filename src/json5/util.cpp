#include "util.hpp"

namespace json5::util
{
  /**
   * 
   */
  bool isDecDigit(char32_t ch)
  {
    return ((ch >= '0') && (ch <= '9'));
  }

  /**
   * 
   */
  bool isHexDigit(char32_t ch)
  {
    switch (ch)
    {
    case 'a':
    case 'b':
    case 'c':
    case 'd':
    case 'e':
    case 'f':
    case 'A':
    case 'B':
    case 'C':
    case 'D':
    case 'E':
    case 'F':
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
      return true;
      break;
    default:
      return false;
      break;
    }
  }

  /**
   * 
   */
  bool isIdStartChar(char32_t ch)
  {
    // Using a bitmap for this would be faster, but would require 20+ KiB

    if ((ch == '$') || (ch == '_'))
    {
      return true;
    }
    else if ((ch >= U'\U00000041') && (ch <= U'\U0000005A'))
    {
      return true;
    }
    else if ((ch >= U'\U00000061') && (ch <= U'\U0000007A'))
    {
      return true;
    }
    else if ((ch >= U'\U000000AA') && (ch <= U'\U000000AA'))
    {
      return true;
    }
    else if ((ch >= U'\U000000B5') && (ch <= U'\U000000B5'))
    {
      return true;
    }
    else if ((ch >= U'\U000000BA') && (ch <= U'\U000000BA'))
    {
      return true;
    }
    else if ((ch >= U'\U000000C0') && (ch <= U'\U000000D6'))
    {
      return true;
    }
    else if ((ch >= U'\U000000D8') && (ch <= U'\U000000F6'))
    {
      return true;
    }
    else if ((ch >= U'\U000000F8') && (ch <= U'\U000002C1'))
    {
      return true;
    }
    else if ((ch >= U'\U000002C6') && (ch <= U'\U000002D1'))
    {
      return true;
    }
    else if ((ch >= U'\U000002E0') && (ch <= U'\U000002E4'))
    {
      return true;
    }
    else if ((ch >= U'\U000002EC') && (ch <= U'\U000002EC'))
    {
      return true;
    }
    else if ((ch >= U'\U000002EE') && (ch <= U'\U000002EE'))
    {
      return true;
    }
    else if ((ch >= U'\U00000370') && (ch <= U'\U00000374'))
    {
      return true;
    }
    else if ((ch >= U'\U00000376') && (ch <= U'\U00000377'))
    {
      return true;
    }
    else if ((ch >= U'\U0000037A') && (ch <= U'\U0000037D'))
    {
      return true;
    }
    else if ((ch >= U'\U0000037F') && (ch <= U'\U0000037F'))
    {
      return true;
    }
    else if ((ch >= U'\U00000386') && (ch <= U'\U00000386'))
    {
      return true;
    }
    else if ((ch >= U'\U00000388') && (ch <= U'\U0000038A'))
    {
      return true;
    }
    else if ((ch >= U'\U0000038C') && (ch <= U'\U0000038C'))
    {
      return true;
    }
    else if ((ch >= U'\U0000038E') && (ch <= U'\U000003A1'))
    {
      return true;
    }
    else if ((ch >= U'\U000003A3') && (ch <= U'\U000003F5'))
    {
      return true;
    }
    else if ((ch >= U'\U000003F7') && (ch <= U'\U00000481'))
    {
      return true;
    }
    else if ((ch >= U'\U0000048A') && (ch <= U'\U0000052F'))
    {
      return true;
    }
    else if ((ch >= U'\U00000531') && (ch <= U'\U00000556'))
    {
      return true;
    }
    else if ((ch >= U'\U00000559') && (ch <= U'\U00000559'))
    {
      return true;
    }
    else if ((ch >= U'\U00000561') && (ch <= U'\U00000587'))
    {
      return true;
    }
    else if ((ch >= U'\U000005D0') && (ch <= U'\U000005EA'))
    {
      return true;
    }
    else if ((ch >= U'\U000005F0') && (ch <= U'\U000005F2'))
    {
      return true;
    }
    else if ((ch >= U'\U00000620') && (ch <= U'\U0000064A'))
    {
      return true;
    }
    else if ((ch >= U'\U0000066E') && (ch <= U'\U0000066F'))
    {
      return true;
    }
    else if ((ch >= U'\U00000671') && (ch <= U'\U000006D3'))
    {
      return true;
    }
    else if ((ch >= U'\U000006D5') && (ch <= U'\U000006D5'))
    {
      return true;
    }
    else if ((ch >= U'\U000006E5') && (ch <= U'\U000006E6'))
    {
      return true;
    }
    else if ((ch >= U'\U000006EE') && (ch <= U'\U000006EF'))
    {
      return true;
    }
    else if ((ch >= U'\U000006FA') && (ch <= U'\U000006FC'))
    {
      return true;
    }
    else if ((ch >= U'\U000006FF') && (ch <= U'\U000006FF'))
    {
      return true;
    }
    else if ((ch >= U'\U00000710') && (ch <= U'\U00000710'))
    {
      return true;
    }
    else if ((ch >= U'\U00000712') && (ch <= U'\U0000072F'))
    {
      return true;
    }
    else if ((ch >= U'\U0000074D') && (ch <= U'\U000007A5'))
    {
      return true;
    }
    else if ((ch >= U'\U000007B1') && (ch <= U'\U000007B1'))
    {
      return true;
    }
    else if ((ch >= U'\U000007CA') && (ch <= U'\U000007EA'))
    {
      return true;
    }
    else if ((ch >= U'\U000007F4') && (ch <= U'\U000007F5'))
    {
      return true;
    }
    else if ((ch >= U'\U000007FA') && (ch <= U'\U000007FA'))
    {
      return true;
    }
    else if ((ch >= U'\U00000800') && (ch <= U'\U00000815'))
    {
      return true;
    }
    else if ((ch >= U'\U0000081A') && (ch <= U'\U0000081A'))
    {
      return true;
    }
    else if ((ch >= U'\U00000824') && (ch <= U'\U00000824'))
    {
      return true;
    }
    else if ((ch >= U'\U00000828') && (ch <= U'\U00000828'))
    {
      return true;
    }
    else if ((ch >= U'\U00000840') && (ch <= U'\U00000858'))
    {
      return true;
    }
    else if ((ch >= U'\U000008A0') && (ch <= U'\U000008B4'))
    {
      return true;
    }
    else if ((ch >= U'\U000008B6') && (ch <= U'\U000008BD'))
    {
      return true;
    }
    else if ((ch >= U'\U00000904') && (ch <= U'\U00000939'))
    {
      return true;
    }
    else if ((ch >= U'\U0000093D') && (ch <= U'\U0000093D'))
    {
      return true;
    }
    else if ((ch >= U'\U00000950') && (ch <= U'\U00000950'))
    {
      return true;
    }
    else if ((ch >= U'\U00000958') && (ch <= U'\U00000961'))
    {
      return true;
    }
    else if ((ch >= U'\U00000971') && (ch <= U'\U00000980'))
    {
      return true;
    }
    else if ((ch >= U'\U00000985') && (ch <= U'\U0000098C'))
    {
      return true;
    }
    else if ((ch >= U'\U0000098F') && (ch <= U'\U00000990'))
    {
      return true;
    }
    else if ((ch >= U'\U00000993') && (ch <= U'\U000009A8'))
    {
      return true;
    }
    else if ((ch >= U'\U000009AA') && (ch <= U'\U000009B0'))
    {
      return true;
    }
    else if ((ch >= U'\U000009B2') && (ch <= U'\U000009B2'))
    {
      return true;
    }
    else if ((ch >= U'\U000009B6') && (ch <= U'\U000009B9'))
    {
      return true;
    }
    else if ((ch >= U'\U000009BD') && (ch <= U'\U000009BD'))
    {
      return true;
    }
    else if ((ch >= U'\U000009CE') && (ch <= U'\U000009CE'))
    {
      return true;
    }
    else if ((ch >= U'\U000009DC') && (ch <= U'\U000009DD'))
    {
      return true;
    }
    else if ((ch >= U'\U000009DF') && (ch <= U'\U000009E1'))
    {
      return true;
    }
    else if ((ch >= U'\U000009F0') && (ch <= U'\U000009F1'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A05') && (ch <= U'\U00000A0A'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A0F') && (ch <= U'\U00000A10'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A13') && (ch <= U'\U00000A28'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A2A') && (ch <= U'\U00000A30'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A32') && (ch <= U'\U00000A33'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A35') && (ch <= U'\U00000A36'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A38') && (ch <= U'\U00000A39'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A59') && (ch <= U'\U00000A5C'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A5E') && (ch <= U'\U00000A5E'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A72') && (ch <= U'\U00000A74'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A85') && (ch <= U'\U00000A8D'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A8F') && (ch <= U'\U00000A91'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A93') && (ch <= U'\U00000AA8'))
    {
      return true;
    }
    else if ((ch >= U'\U00000AAA') && (ch <= U'\U00000AB0'))
    {
      return true;
    }
    else if ((ch >= U'\U00000AB2') && (ch <= U'\U00000AB3'))
    {
      return true;
    }
    else if ((ch >= U'\U00000AB5') && (ch <= U'\U00000AB9'))
    {
      return true;
    }
    else if ((ch >= U'\U00000ABD') && (ch <= U'\U00000ABD'))
    {
      return true;
    }
    else if ((ch >= U'\U00000AD0') && (ch <= U'\U00000AD0'))
    {
      return true;
    }
    else if ((ch >= U'\U00000AE0') && (ch <= U'\U00000AE1'))
    {
      return true;
    }
    else if ((ch >= U'\U00000AF9') && (ch <= U'\U00000AF9'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B05') && (ch <= U'\U00000B0C'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B0F') && (ch <= U'\U00000B10'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B13') && (ch <= U'\U00000B28'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B2A') && (ch <= U'\U00000B30'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B32') && (ch <= U'\U00000B33'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B35') && (ch <= U'\U00000B39'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B3D') && (ch <= U'\U00000B3D'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B5C') && (ch <= U'\U00000B5D'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B5F') && (ch <= U'\U00000B61'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B71') && (ch <= U'\U00000B71'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B83') && (ch <= U'\U00000B83'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B85') && (ch <= U'\U00000B8A'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B8E') && (ch <= U'\U00000B90'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B92') && (ch <= U'\U00000B95'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B99') && (ch <= U'\U00000B9A'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B9C') && (ch <= U'\U00000B9C'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B9E') && (ch <= U'\U00000B9F'))
    {
      return true;
    }
    else if ((ch >= U'\U00000BA3') && (ch <= U'\U00000BA4'))
    {
      return true;
    }
    else if ((ch >= U'\U00000BA8') && (ch <= U'\U00000BAA'))
    {
      return true;
    }
    else if ((ch >= U'\U00000BAE') && (ch <= U'\U00000BB9'))
    {
      return true;
    }
    else if ((ch >= U'\U00000BD0') && (ch <= U'\U00000BD0'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C05') && (ch <= U'\U00000C0C'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C0E') && (ch <= U'\U00000C10'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C12') && (ch <= U'\U00000C28'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C2A') && (ch <= U'\U00000C39'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C3D') && (ch <= U'\U00000C3D'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C58') && (ch <= U'\U00000C5A'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C60') && (ch <= U'\U00000C61'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C80') && (ch <= U'\U00000C80'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C85') && (ch <= U'\U00000C8C'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C8E') && (ch <= U'\U00000C90'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C92') && (ch <= U'\U00000CA8'))
    {
      return true;
    }
    else if ((ch >= U'\U00000CAA') && (ch <= U'\U00000CB3'))
    {
      return true;
    }
    else if ((ch >= U'\U00000CB5') && (ch <= U'\U00000CB9'))
    {
      return true;
    }
    else if ((ch >= U'\U00000CBD') && (ch <= U'\U00000CBD'))
    {
      return true;
    }
    else if ((ch >= U'\U00000CDE') && (ch <= U'\U00000CDE'))
    {
      return true;
    }
    else if ((ch >= U'\U00000CE0') && (ch <= U'\U00000CE1'))
    {
      return true;
    }
    else if ((ch >= U'\U00000CF1') && (ch <= U'\U00000CF2'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D05') && (ch <= U'\U00000D0C'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D0E') && (ch <= U'\U00000D10'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D12') && (ch <= U'\U00000D3A'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D3D') && (ch <= U'\U00000D3D'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D4E') && (ch <= U'\U00000D4E'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D54') && (ch <= U'\U00000D56'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D5F') && (ch <= U'\U00000D61'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D7A') && (ch <= U'\U00000D7F'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D85') && (ch <= U'\U00000D96'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D9A') && (ch <= U'\U00000DB1'))
    {
      return true;
    }
    else if ((ch >= U'\U00000DB3') && (ch <= U'\U00000DBB'))
    {
      return true;
    }
    else if ((ch >= U'\U00000DBD') && (ch <= U'\U00000DBD'))
    {
      return true;
    }
    else if ((ch >= U'\U00000DC0') && (ch <= U'\U00000DC6'))
    {
      return true;
    }
    else if ((ch >= U'\U00000E01') && (ch <= U'\U00000E30'))
    {
      return true;
    }
    else if ((ch >= U'\U00000E32') && (ch <= U'\U00000E33'))
    {
      return true;
    }
    else if ((ch >= U'\U00000E40') && (ch <= U'\U00000E46'))
    {
      return true;
    }
    else if ((ch >= U'\U00000E81') && (ch <= U'\U00000E82'))
    {
      return true;
    }
    else if ((ch >= U'\U00000E84') && (ch <= U'\U00000E84'))
    {
      return true;
    }
    else if ((ch >= U'\U00000E87') && (ch <= U'\U00000E88'))
    {
      return true;
    }
    else if ((ch >= U'\U00000E8A') && (ch <= U'\U00000E8A'))
    {
      return true;
    }
    else if ((ch >= U'\U00000E8D') && (ch <= U'\U00000E8D'))
    {
      return true;
    }
    else if ((ch >= U'\U00000E94') && (ch <= U'\U00000E97'))
    {
      return true;
    }
    else if ((ch >= U'\U00000E99') && (ch <= U'\U00000E9F'))
    {
      return true;
    }
    else if ((ch >= U'\U00000EA1') && (ch <= U'\U00000EA3'))
    {
      return true;
    }
    else if ((ch >= U'\U00000EA5') && (ch <= U'\U00000EA5'))
    {
      return true;
    }
    else if ((ch >= U'\U00000EA7') && (ch <= U'\U00000EA7'))
    {
      return true;
    }
    else if ((ch >= U'\U00000EAA') && (ch <= U'\U00000EAB'))
    {
      return true;
    }
    else if ((ch >= U'\U00000EAD') && (ch <= U'\U00000EB0'))
    {
      return true;
    }
    else if ((ch >= U'\U00000EB2') && (ch <= U'\U00000EB3'))
    {
      return true;
    }
    else if ((ch >= U'\U00000EBD') && (ch <= U'\U00000EBD'))
    {
      return true;
    }
    else if ((ch >= U'\U00000EC0') && (ch <= U'\U00000EC4'))
    {
      return true;
    }
    else if ((ch >= U'\U00000EC6') && (ch <= U'\U00000EC6'))
    {
      return true;
    }
    else if ((ch >= U'\U00000EDC') && (ch <= U'\U00000EDF'))
    {
      return true;
    }
    else if ((ch >= U'\U00000F00') && (ch <= U'\U00000F00'))
    {
      return true;
    }
    else if ((ch >= U'\U00000F40') && (ch <= U'\U00000F47'))
    {
      return true;
    }
    else if ((ch >= U'\U00000F49') && (ch <= U'\U00000F6C'))
    {
      return true;
    }
    else if ((ch >= U'\U00000F88') && (ch <= U'\U00000F8C'))
    {
      return true;
    }
    else if ((ch >= U'\U00001000') && (ch <= U'\U0000102A'))
    {
      return true;
    }
    else if ((ch >= U'\U0000103F') && (ch <= U'\U0000103F'))
    {
      return true;
    }
    else if ((ch >= U'\U00001050') && (ch <= U'\U00001055'))
    {
      return true;
    }
    else if ((ch >= U'\U0000105A') && (ch <= U'\U0000105D'))
    {
      return true;
    }
    else if ((ch >= U'\U00001061') && (ch <= U'\U00001061'))
    {
      return true;
    }
    else if ((ch >= U'\U00001065') && (ch <= U'\U00001066'))
    {
      return true;
    }
    else if ((ch >= U'\U0000106E') && (ch <= U'\U00001070'))
    {
      return true;
    }
    else if ((ch >= U'\U00001075') && (ch <= U'\U00001081'))
    {
      return true;
    }
    else if ((ch >= U'\U0000108E') && (ch <= U'\U0000108E'))
    {
      return true;
    }
    else if ((ch >= U'\U000010A0') && (ch <= U'\U000010C5'))
    {
      return true;
    }
    else if ((ch >= U'\U000010C7') && (ch <= U'\U000010C7'))
    {
      return true;
    }
    else if ((ch >= U'\U000010CD') && (ch <= U'\U000010CD'))
    {
      return true;
    }
    else if ((ch >= U'\U000010D0') && (ch <= U'\U000010FA'))
    {
      return true;
    }
    else if ((ch >= U'\U000010FC') && (ch <= U'\U00001248'))
    {
      return true;
    }
    else if ((ch >= U'\U0000124A') && (ch <= U'\U0000124D'))
    {
      return true;
    }
    else if ((ch >= U'\U00001250') && (ch <= U'\U00001256'))
    {
      return true;
    }
    else if ((ch >= U'\U00001258') && (ch <= U'\U00001258'))
    {
      return true;
    }
    else if ((ch >= U'\U0000125A') && (ch <= U'\U0000125D'))
    {
      return true;
    }
    else if ((ch >= U'\U00001260') && (ch <= U'\U00001288'))
    {
      return true;
    }
    else if ((ch >= U'\U0000128A') && (ch <= U'\U0000128D'))
    {
      return true;
    }
    else if ((ch >= U'\U00001290') && (ch <= U'\U000012B0'))
    {
      return true;
    }
    else if ((ch >= U'\U000012B2') && (ch <= U'\U000012B5'))
    {
      return true;
    }
    else if ((ch >= U'\U000012B8') && (ch <= U'\U000012BE'))
    {
      return true;
    }
    else if ((ch >= U'\U000012C0') && (ch <= U'\U000012C0'))
    {
      return true;
    }
    else if ((ch >= U'\U000012C2') && (ch <= U'\U000012C5'))
    {
      return true;
    }
    else if ((ch >= U'\U000012C8') && (ch <= U'\U000012D6'))
    {
      return true;
    }
    else if ((ch >= U'\U000012D8') && (ch <= U'\U00001310'))
    {
      return true;
    }
    else if ((ch >= U'\U00001312') && (ch <= U'\U00001315'))
    {
      return true;
    }
    else if ((ch >= U'\U00001318') && (ch <= U'\U0000135A'))
    {
      return true;
    }
    else if ((ch >= U'\U00001380') && (ch <= U'\U0000138F'))
    {
      return true;
    }
    else if ((ch >= U'\U000013A0') && (ch <= U'\U000013F5'))
    {
      return true;
    }
    else if ((ch >= U'\U000013F8') && (ch <= U'\U000013FD'))
    {
      return true;
    }
    else if ((ch >= U'\U00001401') && (ch <= U'\U0000166C'))
    {
      return true;
    }
    else if ((ch >= U'\U0000166F') && (ch <= U'\U0000167F'))
    {
      return true;
    }
    else if ((ch >= U'\U00001681') && (ch <= U'\U0000169A'))
    {
      return true;
    }
    else if ((ch >= U'\U000016A0') && (ch <= U'\U000016EA'))
    {
      return true;
    }
    else if ((ch >= U'\U000016EE') && (ch <= U'\U000016F8'))
    {
      return true;
    }
    else if ((ch >= U'\U00001700') && (ch <= U'\U0000170C'))
    {
      return true;
    }
    else if ((ch >= U'\U0000170E') && (ch <= U'\U00001711'))
    {
      return true;
    }
    else if ((ch >= U'\U00001720') && (ch <= U'\U00001731'))
    {
      return true;
    }
    else if ((ch >= U'\U00001740') && (ch <= U'\U00001751'))
    {
      return true;
    }
    else if ((ch >= U'\U00001760') && (ch <= U'\U0000176C'))
    {
      return true;
    }
    else if ((ch >= U'\U0000176E') && (ch <= U'\U00001770'))
    {
      return true;
    }
    else if ((ch >= U'\U00001780') && (ch <= U'\U000017B3'))
    {
      return true;
    }
    else if ((ch >= U'\U000017D7') && (ch <= U'\U000017D7'))
    {
      return true;
    }
    else if ((ch >= U'\U000017DC') && (ch <= U'\U000017DC'))
    {
      return true;
    }
    else if ((ch >= U'\U00001820') && (ch <= U'\U00001877'))
    {
      return true;
    }
    else if ((ch >= U'\U00001880') && (ch <= U'\U000018A8'))
    {
      return true;
    }
    else if ((ch >= U'\U000018AA') && (ch <= U'\U000018AA'))
    {
      return true;
    }
    else if ((ch >= U'\U000018B0') && (ch <= U'\U000018F5'))
    {
      return true;
    }
    else if ((ch >= U'\U00001900') && (ch <= U'\U0000191E'))
    {
      return true;
    }
    else if ((ch >= U'\U00001950') && (ch <= U'\U0000196D'))
    {
      return true;
    }
    else if ((ch >= U'\U00001970') && (ch <= U'\U00001974'))
    {
      return true;
    }
    else if ((ch >= U'\U00001980') && (ch <= U'\U000019AB'))
    {
      return true;
    }
    else if ((ch >= U'\U000019B0') && (ch <= U'\U000019C9'))
    {
      return true;
    }
    else if ((ch >= U'\U00001A00') && (ch <= U'\U00001A16'))
    {
      return true;
    }
    else if ((ch >= U'\U00001A20') && (ch <= U'\U00001A54'))
    {
      return true;
    }
    else if ((ch >= U'\U00001AA7') && (ch <= U'\U00001AA7'))
    {
      return true;
    }
    else if ((ch >= U'\U00001B05') && (ch <= U'\U00001B33'))
    {
      return true;
    }
    else if ((ch >= U'\U00001B45') && (ch <= U'\U00001B4B'))
    {
      return true;
    }
    else if ((ch >= U'\U00001B83') && (ch <= U'\U00001BA0'))
    {
      return true;
    }
    else if ((ch >= U'\U00001BAE') && (ch <= U'\U00001BAF'))
    {
      return true;
    }
    else if ((ch >= U'\U00001BBA') && (ch <= U'\U00001BE5'))
    {
      return true;
    }
    else if ((ch >= U'\U00001C00') && (ch <= U'\U00001C23'))
    {
      return true;
    }
    else if ((ch >= U'\U00001C4D') && (ch <= U'\U00001C4F'))
    {
      return true;
    }
    else if ((ch >= U'\U00001C5A') && (ch <= U'\U00001C7D'))
    {
      return true;
    }
    else if ((ch >= U'\U00001C80') && (ch <= U'\U00001C88'))
    {
      return true;
    }
    else if ((ch >= U'\U00001CE9') && (ch <= U'\U00001CEC'))
    {
      return true;
    }
    else if ((ch >= U'\U00001CEE') && (ch <= U'\U00001CF1'))
    {
      return true;
    }
    else if ((ch >= U'\U00001CF5') && (ch <= U'\U00001CF6'))
    {
      return true;
    }
    else if ((ch >= U'\U00001D00') && (ch <= U'\U00001DBF'))
    {
      return true;
    }
    else if ((ch >= U'\U00001E00') && (ch <= U'\U00001F15'))
    {
      return true;
    }
    else if ((ch >= U'\U00001F18') && (ch <= U'\U00001F1D'))
    {
      return true;
    }
    else if ((ch >= U'\U00001F20') && (ch <= U'\U00001F45'))
    {
      return true;
    }
    else if ((ch >= U'\U00001F48') && (ch <= U'\U00001F4D'))
    {
      return true;
    }
    else if ((ch >= U'\U00001F50') && (ch <= U'\U00001F57'))
    {
      return true;
    }
    else if ((ch >= U'\U00001F59') && (ch <= U'\U00001F59'))
    {
      return true;
    }
    else if ((ch >= U'\U00001F5B') && (ch <= U'\U00001F5B'))
    {
      return true;
    }
    else if ((ch >= U'\U00001F5D') && (ch <= U'\U00001F5D'))
    {
      return true;
    }
    else if ((ch >= U'\U00001F5F') && (ch <= U'\U00001F7D'))
    {
      return true;
    }
    else if ((ch >= U'\U00001F80') && (ch <= U'\U00001FB4'))
    {
      return true;
    }
    else if ((ch >= U'\U00001FB6') && (ch <= U'\U00001FBC'))
    {
      return true;
    }
    else if ((ch >= U'\U00001FBE') && (ch <= U'\U00001FBE'))
    {
      return true;
    }
    else if ((ch >= U'\U00001FC2') && (ch <= U'\U00001FC4'))
    {
      return true;
    }
    else if ((ch >= U'\U00001FC6') && (ch <= U'\U00001FCC'))
    {
      return true;
    }
    else if ((ch >= U'\U00001FD0') && (ch <= U'\U00001FD3'))
    {
      return true;
    }
    else if ((ch >= U'\U00001FD6') && (ch <= U'\U00001FDB'))
    {
      return true;
    }
    else if ((ch >= U'\U00001FE0') && (ch <= U'\U00001FEC'))
    {
      return true;
    }
    else if ((ch >= U'\U00001FF2') && (ch <= U'\U00001FF4'))
    {
      return true;
    }
    else if ((ch >= U'\U00001FF6') && (ch <= U'\U00001FFC'))
    {
      return true;
    }
    else if ((ch >= U'\U00002071') && (ch <= U'\U00002071'))
    {
      return true;
    }
    else if ((ch >= U'\U0000207F') && (ch <= U'\U0000207F'))
    {
      return true;
    }
    else if ((ch >= U'\U00002090') && (ch <= U'\U0000209C'))
    {
      return true;
    }
    else if ((ch >= U'\U00002102') && (ch <= U'\U00002102'))
    {
      return true;
    }
    else if ((ch >= U'\U00002107') && (ch <= U'\U00002107'))
    {
      return true;
    }
    else if ((ch >= U'\U0000210A') && (ch <= U'\U00002113'))
    {
      return true;
    }
    else if ((ch >= U'\U00002115') && (ch <= U'\U00002115'))
    {
      return true;
    }
    else if ((ch >= U'\U00002118') && (ch <= U'\U0000211D'))
    {
      return true;
    }
    else if ((ch >= U'\U00002124') && (ch <= U'\U00002124'))
    {
      return true;
    }
    else if ((ch >= U'\U00002126') && (ch <= U'\U00002126'))
    {
      return true;
    }
    else if ((ch >= U'\U00002128') && (ch <= U'\U00002128'))
    {
      return true;
    }
    else if ((ch >= U'\U0000212A') && (ch <= U'\U00002139'))
    {
      return true;
    }
    else if ((ch >= U'\U0000213C') && (ch <= U'\U0000213F'))
    {
      return true;
    }
    else if ((ch >= U'\U00002145') && (ch <= U'\U00002149'))
    {
      return true;
    }
    else if ((ch >= U'\U0000214E') && (ch <= U'\U0000214E'))
    {
      return true;
    }
    else if ((ch >= U'\U00002160') && (ch <= U'\U00002188'))
    {
      return true;
    }
    else if ((ch >= U'\U00002C00') && (ch <= U'\U00002C2E'))
    {
      return true;
    }
    else if ((ch >= U'\U00002C30') && (ch <= U'\U00002C5E'))
    {
      return true;
    }
    else if ((ch >= U'\U00002C60') && (ch <= U'\U00002CE4'))
    {
      return true;
    }
    else if ((ch >= U'\U00002CEB') && (ch <= U'\U00002CEE'))
    {
      return true;
    }
    else if ((ch >= U'\U00002CF2') && (ch <= U'\U00002CF3'))
    {
      return true;
    }
    else if ((ch >= U'\U00002D00') && (ch <= U'\U00002D25'))
    {
      return true;
    }
    else if ((ch >= U'\U00002D27') && (ch <= U'\U00002D27'))
    {
      return true;
    }
    else if ((ch >= U'\U00002D2D') && (ch <= U'\U00002D2D'))
    {
      return true;
    }
    else if ((ch >= U'\U00002D30') && (ch <= U'\U00002D67'))
    {
      return true;
    }
    else if ((ch >= U'\U00002D6F') && (ch <= U'\U00002D6F'))
    {
      return true;
    }
    else if ((ch >= U'\U00002D80') && (ch <= U'\U00002D96'))
    {
      return true;
    }
    else if ((ch >= U'\U00002DA0') && (ch <= U'\U00002DA6'))
    {
      return true;
    }
    else if ((ch >= U'\U00002DA8') && (ch <= U'\U00002DAE'))
    {
      return true;
    }
    else if ((ch >= U'\U00002DB0') && (ch <= U'\U00002DB6'))
    {
      return true;
    }
    else if ((ch >= U'\U00002DB8') && (ch <= U'\U00002DBE'))
    {
      return true;
    }
    else if ((ch >= U'\U00002DC0') && (ch <= U'\U00002DC6'))
    {
      return true;
    }
    else if ((ch >= U'\U00002DC8') && (ch <= U'\U00002DCE'))
    {
      return true;
    }
    else if ((ch >= U'\U00002DD0') && (ch <= U'\U00002DD6'))
    {
      return true;
    }
    else if ((ch >= U'\U00002DD8') && (ch <= U'\U00002DDE'))
    {
      return true;
    }
    else if ((ch >= U'\U00003005') && (ch <= U'\U00003007'))
    {
      return true;
    }
    else if ((ch >= U'\U00003021') && (ch <= U'\U00003029'))
    {
      return true;
    }
    else if ((ch >= U'\U00003031') && (ch <= U'\U00003035'))
    {
      return true;
    }
    else if ((ch >= U'\U00003038') && (ch <= U'\U0000303C'))
    {
      return true;
    }
    else if ((ch >= U'\U00003041') && (ch <= U'\U00003096'))
    {
      return true;
    }
    else if ((ch >= U'\U0000309B') && (ch <= U'\U0000309F'))
    {
      return true;
    }
    else if ((ch >= U'\U000030A1') && (ch <= U'\U000030FA'))
    {
      return true;
    }
    else if ((ch >= U'\U000030FC') && (ch <= U'\U000030FF'))
    {
      return true;
    }
    else if ((ch >= U'\U00003105') && (ch <= U'\U0000312D'))
    {
      return true;
    }
    else if ((ch >= U'\U00003131') && (ch <= U'\U0000318E'))
    {
      return true;
    }
    else if ((ch >= U'\U000031A0') && (ch <= U'\U000031BA'))
    {
      return true;
    }
    else if ((ch >= U'\U000031F0') && (ch <= U'\U000031FF'))
    {
      return true;
    }
    else if ((ch >= U'\U00003400') && (ch <= U'\U00004DB5'))
    {
      return true;
    }
    else if ((ch >= U'\U00004E00') && (ch <= U'\U00009FD5'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A000') && (ch <= U'\U0000A48C'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A4D0') && (ch <= U'\U0000A4FD'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A500') && (ch <= U'\U0000A60C'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A610') && (ch <= U'\U0000A61F'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A62A') && (ch <= U'\U0000A62B'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A640') && (ch <= U'\U0000A66E'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A67F') && (ch <= U'\U0000A69D'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A6A0') && (ch <= U'\U0000A6EF'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A717') && (ch <= U'\U0000A71F'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A722') && (ch <= U'\U0000A788'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A78B') && (ch <= U'\U0000A7AE'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A7B0') && (ch <= U'\U0000A7B7'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A7F7') && (ch <= U'\U0000A801'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A803') && (ch <= U'\U0000A805'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A807') && (ch <= U'\U0000A80A'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A80C') && (ch <= U'\U0000A822'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A840') && (ch <= U'\U0000A873'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A882') && (ch <= U'\U0000A8B3'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A8F2') && (ch <= U'\U0000A8F7'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A8FB') && (ch <= U'\U0000A8FB'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A8FD') && (ch <= U'\U0000A8FD'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A90A') && (ch <= U'\U0000A925'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A930') && (ch <= U'\U0000A946'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A960') && (ch <= U'\U0000A97C'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A984') && (ch <= U'\U0000A9B2'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A9CF') && (ch <= U'\U0000A9CF'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A9E0') && (ch <= U'\U0000A9E4'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A9E6') && (ch <= U'\U0000A9EF'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A9FA') && (ch <= U'\U0000A9FE'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AA00') && (ch <= U'\U0000AA28'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AA40') && (ch <= U'\U0000AA42'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AA44') && (ch <= U'\U0000AA4B'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AA60') && (ch <= U'\U0000AA76'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AA7A') && (ch <= U'\U0000AA7A'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AA7E') && (ch <= U'\U0000AAAF'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AAB1') && (ch <= U'\U0000AAB1'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AAB5') && (ch <= U'\U0000AAB6'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AAB9') && (ch <= U'\U0000AABD'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AAC0') && (ch <= U'\U0000AAC0'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AAC2') && (ch <= U'\U0000AAC2'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AADB') && (ch <= U'\U0000AADD'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AAE0') && (ch <= U'\U0000AAEA'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AAF2') && (ch <= U'\U0000AAF4'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AB01') && (ch <= U'\U0000AB06'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AB09') && (ch <= U'\U0000AB0E'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AB11') && (ch <= U'\U0000AB16'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AB20') && (ch <= U'\U0000AB26'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AB28') && (ch <= U'\U0000AB2E'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AB30') && (ch <= U'\U0000AB5A'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AB5C') && (ch <= U'\U0000AB65'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AB70') && (ch <= U'\U0000ABE2'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AC00') && (ch <= U'\U0000D7A3'))
    {
      return true;
    }
    else if ((ch >= U'\U0000D7B0') && (ch <= U'\U0000D7C6'))
    {
      return true;
    }
    else if ((ch >= U'\U0000D7CB') && (ch <= U'\U0000D7FB'))
    {
      return true;
    }
    else if ((ch >= U'\U0000F900') && (ch <= U'\U0000FA6D'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FA70') && (ch <= U'\U0000FAD9'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FB00') && (ch <= U'\U0000FB06'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FB13') && (ch <= U'\U0000FB17'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FB1D') && (ch <= U'\U0000FB1D'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FB1F') && (ch <= U'\U0000FB28'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FB2A') && (ch <= U'\U0000FB36'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FB38') && (ch <= U'\U0000FB3C'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FB3E') && (ch <= U'\U0000FB3E'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FB40') && (ch <= U'\U0000FB41'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FB43') && (ch <= U'\U0000FB44'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FB46') && (ch <= U'\U0000FBB1'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FBD3') && (ch <= U'\U0000FD3D'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FD50') && (ch <= U'\U0000FD8F'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FD92') && (ch <= U'\U0000FDC7'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FDF0') && (ch <= U'\U0000FDFB'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FE70') && (ch <= U'\U0000FE74'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FE76') && (ch <= U'\U0000FEFC'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FF21') && (ch <= U'\U0000FF3A'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FF41') && (ch <= U'\U0000FF5A'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FF66') && (ch <= U'\U0000FFBE'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FFC2') && (ch <= U'\U0000FFC7'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FFCA') && (ch <= U'\U0000FFCF'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FFD2') && (ch <= U'\U0000FFD7'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FFDA') && (ch <= U'\U0000FFDC'))
    {
      return true;
    }
    else if ((ch >= U'\U00010000') && (ch <= U'\U0001000B'))
    {
      return true;
    }
    else if ((ch >= U'\U0001000D') && (ch <= U'\U00010026'))
    {
      return true;
    }
    else if ((ch >= U'\U00010028') && (ch <= U'\U0001003A'))
    {
      return true;
    }
    else if ((ch >= U'\U0001003C') && (ch <= U'\U0001003D'))
    {
      return true;
    }
    else if ((ch >= U'\U0001003F') && (ch <= U'\U0001004D'))
    {
      return true;
    }
    else if ((ch >= U'\U00010050') && (ch <= U'\U0001005D'))
    {
      return true;
    }
    else if ((ch >= U'\U00010080') && (ch <= U'\U000100FA'))
    {
      return true;
    }
    else if ((ch >= U'\U00010140') && (ch <= U'\U00010174'))
    {
      return true;
    }
    else if ((ch >= U'\U00010280') && (ch <= U'\U0001029C'))
    {
      return true;
    }
    else if ((ch >= U'\U000102A0') && (ch <= U'\U000102D0'))
    {
      return true;
    }
    else if ((ch >= U'\U00010300') && (ch <= U'\U0001031F'))
    {
      return true;
    }
    else if ((ch >= U'\U00010330') && (ch <= U'\U0001034A'))
    {
      return true;
    }
    else if ((ch >= U'\U00010350') && (ch <= U'\U00010375'))
    {
      return true;
    }
    else if ((ch >= U'\U00010380') && (ch <= U'\U0001039D'))
    {
      return true;
    }
    else if ((ch >= U'\U000103A0') && (ch <= U'\U000103C3'))
    {
      return true;
    }
    else if ((ch >= U'\U000103C8') && (ch <= U'\U000103CF'))
    {
      return true;
    }
    else if ((ch >= U'\U000103D1') && (ch <= U'\U000103D5'))
    {
      return true;
    }
    else if ((ch >= U'\U00010400') && (ch <= U'\U0001049D'))
    {
      return true;
    }
    else if ((ch >= U'\U000104B0') && (ch <= U'\U000104D3'))
    {
      return true;
    }
    else if ((ch >= U'\U000104D8') && (ch <= U'\U000104FB'))
    {
      return true;
    }
    else if ((ch >= U'\U00010500') && (ch <= U'\U00010527'))
    {
      return true;
    }
    else if ((ch >= U'\U00010530') && (ch <= U'\U00010563'))
    {
      return true;
    }
    else if ((ch >= U'\U00010600') && (ch <= U'\U00010736'))
    {
      return true;
    }
    else if ((ch >= U'\U00010740') && (ch <= U'\U00010755'))
    {
      return true;
    }
    else if ((ch >= U'\U00010760') && (ch <= U'\U00010767'))
    {
      return true;
    }
    else if ((ch >= U'\U00010800') && (ch <= U'\U00010805'))
    {
      return true;
    }
    else if ((ch >= U'\U00010808') && (ch <= U'\U00010808'))
    {
      return true;
    }
    else if ((ch >= U'\U0001080A') && (ch <= U'\U00010835'))
    {
      return true;
    }
    else if ((ch >= U'\U00010837') && (ch <= U'\U00010838'))
    {
      return true;
    }
    else if ((ch >= U'\U0001083C') && (ch <= U'\U0001083C'))
    {
      return true;
    }
    else if ((ch >= U'\U0001083F') && (ch <= U'\U00010855'))
    {
      return true;
    }
    else if ((ch >= U'\U00010860') && (ch <= U'\U00010876'))
    {
      return true;
    }
    else if ((ch >= U'\U00010880') && (ch <= U'\U0001089E'))
    {
      return true;
    }
    else if ((ch >= U'\U000108E0') && (ch <= U'\U000108F2'))
    {
      return true;
    }
    else if ((ch >= U'\U000108F4') && (ch <= U'\U000108F5'))
    {
      return true;
    }
    else if ((ch >= U'\U00010900') && (ch <= U'\U00010915'))
    {
      return true;
    }
    else if ((ch >= U'\U00010920') && (ch <= U'\U00010939'))
    {
      return true;
    }
    else if ((ch >= U'\U00010980') && (ch <= U'\U000109B7'))
    {
      return true;
    }
    else if ((ch >= U'\U000109BE') && (ch <= U'\U000109BF'))
    {
      return true;
    }
    else if ((ch >= U'\U00010A00') && (ch <= U'\U00010A00'))
    {
      return true;
    }
    else if ((ch >= U'\U00010A10') && (ch <= U'\U00010A13'))
    {
      return true;
    }
    else if ((ch >= U'\U00010A15') && (ch <= U'\U00010A17'))
    {
      return true;
    }
    else if ((ch >= U'\U00010A19') && (ch <= U'\U00010A33'))
    {
      return true;
    }
    else if ((ch >= U'\U00010A60') && (ch <= U'\U00010A7C'))
    {
      return true;
    }
    else if ((ch >= U'\U00010A80') && (ch <= U'\U00010A9C'))
    {
      return true;
    }
    else if ((ch >= U'\U00010AC0') && (ch <= U'\U00010AC7'))
    {
      return true;
    }
    else if ((ch >= U'\U00010AC9') && (ch <= U'\U00010AE4'))
    {
      return true;
    }
    else if ((ch >= U'\U00010B00') && (ch <= U'\U00010B35'))
    {
      return true;
    }
    else if ((ch >= U'\U00010B40') && (ch <= U'\U00010B55'))
    {
      return true;
    }
    else if ((ch >= U'\U00010B60') && (ch <= U'\U00010B72'))
    {
      return true;
    }
    else if ((ch >= U'\U00010B80') && (ch <= U'\U00010B91'))
    {
      return true;
    }
    else if ((ch >= U'\U00010C00') && (ch <= U'\U00010C48'))
    {
      return true;
    }
    else if ((ch >= U'\U00010C80') && (ch <= U'\U00010CB2'))
    {
      return true;
    }
    else if ((ch >= U'\U00010CC0') && (ch <= U'\U00010CF2'))
    {
      return true;
    }
    else if ((ch >= U'\U00011003') && (ch <= U'\U00011037'))
    {
      return true;
    }
    else if ((ch >= U'\U00011083') && (ch <= U'\U000110AF'))
    {
      return true;
    }
    else if ((ch >= U'\U000110D0') && (ch <= U'\U000110E8'))
    {
      return true;
    }
    else if ((ch >= U'\U00011103') && (ch <= U'\U00011126'))
    {
      return true;
    }
    else if ((ch >= U'\U00011150') && (ch <= U'\U00011172'))
    {
      return true;
    }
    else if ((ch >= U'\U00011176') && (ch <= U'\U00011176'))
    {
      return true;
    }
    else if ((ch >= U'\U00011183') && (ch <= U'\U000111B2'))
    {
      return true;
    }
    else if ((ch >= U'\U000111C1') && (ch <= U'\U000111C4'))
    {
      return true;
    }
    else if ((ch >= U'\U000111DA') && (ch <= U'\U000111DA'))
    {
      return true;
    }
    else if ((ch >= U'\U000111DC') && (ch <= U'\U000111DC'))
    {
      return true;
    }
    else if ((ch >= U'\U00011200') && (ch <= U'\U00011211'))
    {
      return true;
    }
    else if ((ch >= U'\U00011213') && (ch <= U'\U0001122B'))
    {
      return true;
    }
    else if ((ch >= U'\U00011280') && (ch <= U'\U00011286'))
    {
      return true;
    }
    else if ((ch >= U'\U00011288') && (ch <= U'\U00011288'))
    {
      return true;
    }
    else if ((ch >= U'\U0001128A') && (ch <= U'\U0001128D'))
    {
      return true;
    }
    else if ((ch >= U'\U0001128F') && (ch <= U'\U0001129D'))
    {
      return true;
    }
    else if ((ch >= U'\U0001129F') && (ch <= U'\U000112A8'))
    {
      return true;
    }
    else if ((ch >= U'\U000112B0') && (ch <= U'\U000112DE'))
    {
      return true;
    }
    else if ((ch >= U'\U00011305') && (ch <= U'\U0001130C'))
    {
      return true;
    }
    else if ((ch >= U'\U0001130F') && (ch <= U'\U00011310'))
    {
      return true;
    }
    else if ((ch >= U'\U00011313') && (ch <= U'\U00011328'))
    {
      return true;
    }
    else if ((ch >= U'\U0001132A') && (ch <= U'\U00011330'))
    {
      return true;
    }
    else if ((ch >= U'\U00011332') && (ch <= U'\U00011333'))
    {
      return true;
    }
    else if ((ch >= U'\U00011335') && (ch <= U'\U00011339'))
    {
      return true;
    }
    else if ((ch >= U'\U0001133D') && (ch <= U'\U0001133D'))
    {
      return true;
    }
    else if ((ch >= U'\U00011350') && (ch <= U'\U00011350'))
    {
      return true;
    }
    else if ((ch >= U'\U0001135D') && (ch <= U'\U00011361'))
    {
      return true;
    }
    else if ((ch >= U'\U00011400') && (ch <= U'\U00011434'))
    {
      return true;
    }
    else if ((ch >= U'\U00011447') && (ch <= U'\U0001144A'))
    {
      return true;
    }
    else if ((ch >= U'\U00011480') && (ch <= U'\U000114AF'))
    {
      return true;
    }
    else if ((ch >= U'\U000114C4') && (ch <= U'\U000114C5'))
    {
      return true;
    }
    else if ((ch >= U'\U000114C7') && (ch <= U'\U000114C7'))
    {
      return true;
    }
    else if ((ch >= U'\U00011580') && (ch <= U'\U000115AE'))
    {
      return true;
    }
    else if ((ch >= U'\U000115D8') && (ch <= U'\U000115DB'))
    {
      return true;
    }
    else if ((ch >= U'\U00011600') && (ch <= U'\U0001162F'))
    {
      return true;
    }
    else if ((ch >= U'\U00011644') && (ch <= U'\U00011644'))
    {
      return true;
    }
    else if ((ch >= U'\U00011680') && (ch <= U'\U000116AA'))
    {
      return true;
    }
    else if ((ch >= U'\U00011700') && (ch <= U'\U00011719'))
    {
      return true;
    }
    else if ((ch >= U'\U000118A0') && (ch <= U'\U000118DF'))
    {
      return true;
    }
    else if ((ch >= U'\U000118FF') && (ch <= U'\U000118FF'))
    {
      return true;
    }
    else if ((ch >= U'\U00011AC0') && (ch <= U'\U00011AF8'))
    {
      return true;
    }
    else if ((ch >= U'\U00011C00') && (ch <= U'\U00011C08'))
    {
      return true;
    }
    else if ((ch >= U'\U00011C0A') && (ch <= U'\U00011C2E'))
    {
      return true;
    }
    else if ((ch >= U'\U00011C40') && (ch <= U'\U00011C40'))
    {
      return true;
    }
    else if ((ch >= U'\U00011C72') && (ch <= U'\U00011C8F'))
    {
      return true;
    }
    else if ((ch >= U'\U00012000') && (ch <= U'\U00012399'))
    {
      return true;
    }
    else if ((ch >= U'\U00012400') && (ch <= U'\U0001246E'))
    {
      return true;
    }
    else if ((ch >= U'\U00012480') && (ch <= U'\U00012543'))
    {
      return true;
    }
    else if ((ch >= U'\U00013000') && (ch <= U'\U0001342E'))
    {
      return true;
    }
    else if ((ch >= U'\U00014400') && (ch <= U'\U00014646'))
    {
      return true;
    }
    else if ((ch >= U'\U00016800') && (ch <= U'\U00016A38'))
    {
      return true;
    }
    else if ((ch >= U'\U00016A40') && (ch <= U'\U00016A5E'))
    {
      return true;
    }
    else if ((ch >= U'\U00016AD0') && (ch <= U'\U00016AED'))
    {
      return true;
    }
    else if ((ch >= U'\U00016B00') && (ch <= U'\U00016B2F'))
    {
      return true;
    }
    else if ((ch >= U'\U00016B40') && (ch <= U'\U00016B43'))
    {
      return true;
    }
    else if ((ch >= U'\U00016B63') && (ch <= U'\U00016B77'))
    {
      return true;
    }
    else if ((ch >= U'\U00016B7D') && (ch <= U'\U00016B8F'))
    {
      return true;
    }
    else if ((ch >= U'\U00016F00') && (ch <= U'\U00016F44'))
    {
      return true;
    }
    else if ((ch >= U'\U00016F50') && (ch <= U'\U00016F50'))
    {
      return true;
    }
    else if ((ch >= U'\U00016F93') && (ch <= U'\U00016F9F'))
    {
      return true;
    }
    else if ((ch >= U'\U00016FE0') && (ch <= U'\U00016FE0'))
    {
      return true;
    }
    else if ((ch >= U'\U00017000') && (ch <= U'\U000187EC'))
    {
      return true;
    }
    else if ((ch >= U'\U00018800') && (ch <= U'\U00018AF2'))
    {
      return true;
    }
    else if ((ch >= U'\U0001B000') && (ch <= U'\U0001B001'))
    {
      return true;
    }
    else if ((ch >= U'\U0001BC00') && (ch <= U'\U0001BC6A'))
    {
      return true;
    }
    else if ((ch >= U'\U0001BC70') && (ch <= U'\U0001BC7C'))
    {
      return true;
    }
    else if ((ch >= U'\U0001BC80') && (ch <= U'\U0001BC88'))
    {
      return true;
    }
    else if ((ch >= U'\U0001BC90') && (ch <= U'\U0001BC99'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D400') && (ch <= U'\U0001D454'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D456') && (ch <= U'\U0001D49C'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D49E') && (ch <= U'\U0001D49F'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D4A2') && (ch <= U'\U0001D4A2'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D4A5') && (ch <= U'\U0001D4A6'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D4A9') && (ch <= U'\U0001D4AC'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D4AE') && (ch <= U'\U0001D4B9'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D4BB') && (ch <= U'\U0001D4BB'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D4BD') && (ch <= U'\U0001D4C3'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D4C5') && (ch <= U'\U0001D505'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D507') && (ch <= U'\U0001D50A'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D50D') && (ch <= U'\U0001D514'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D516') && (ch <= U'\U0001D51C'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D51E') && (ch <= U'\U0001D539'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D53B') && (ch <= U'\U0001D53E'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D540') && (ch <= U'\U0001D544'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D546') && (ch <= U'\U0001D546'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D54A') && (ch <= U'\U0001D550'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D552') && (ch <= U'\U0001D6A5'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D6A8') && (ch <= U'\U0001D6C0'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D6C2') && (ch <= U'\U0001D6DA'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D6DC') && (ch <= U'\U0001D6FA'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D6FC') && (ch <= U'\U0001D714'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D716') && (ch <= U'\U0001D734'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D736') && (ch <= U'\U0001D74E'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D750') && (ch <= U'\U0001D76E'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D770') && (ch <= U'\U0001D788'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D78A') && (ch <= U'\U0001D7A8'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D7AA') && (ch <= U'\U0001D7C2'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D7C4') && (ch <= U'\U0001D7CB'))
    {
      return true;
    }
    else if ((ch >= U'\U0001E800') && (ch <= U'\U0001E8C4'))
    {
      return true;
    }
    else if ((ch >= U'\U0001E900') && (ch <= U'\U0001E943'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE00') && (ch <= U'\U0001EE03'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE05') && (ch <= U'\U0001EE1F'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE21') && (ch <= U'\U0001EE22'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE24') && (ch <= U'\U0001EE24'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE27') && (ch <= U'\U0001EE27'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE29') && (ch <= U'\U0001EE32'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE34') && (ch <= U'\U0001EE37'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE39') && (ch <= U'\U0001EE39'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE3B') && (ch <= U'\U0001EE3B'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE42') && (ch <= U'\U0001EE42'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE47') && (ch <= U'\U0001EE47'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE49') && (ch <= U'\U0001EE49'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE4B') && (ch <= U'\U0001EE4B'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE4D') && (ch <= U'\U0001EE4F'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE51') && (ch <= U'\U0001EE52'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE54') && (ch <= U'\U0001EE54'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE57') && (ch <= U'\U0001EE57'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE59') && (ch <= U'\U0001EE59'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE5B') && (ch <= U'\U0001EE5B'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE5D') && (ch <= U'\U0001EE5D'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE5F') && (ch <= U'\U0001EE5F'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE61') && (ch <= U'\U0001EE62'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE64') && (ch <= U'\U0001EE64'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE67') && (ch <= U'\U0001EE6A'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE6C') && (ch <= U'\U0001EE72'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE74') && (ch <= U'\U0001EE77'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE79') && (ch <= U'\U0001EE7C'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE7E') && (ch <= U'\U0001EE7E'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE80') && (ch <= U'\U0001EE89'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE8B') && (ch <= U'\U0001EE9B'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EEA1') && (ch <= U'\U0001EEA3'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EEA5') && (ch <= U'\U0001EEA9'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EEAB') && (ch <= U'\U0001EEBB'))
    {
      return true;
    }
    else if ((ch >= U'\U00020000') && (ch <= U'\U0002A6D6'))
    {
      return true;
    }
    else if ((ch >= U'\U0002A700') && (ch <= U'\U0002B734'))
    {
      return true;
    }
    else if ((ch >= U'\U0002B740') && (ch <= U'\U0002B81D'))
    {
      return true;
    }
    else if ((ch >= U'\U0002B820') && (ch <= U'\U0002CEA1'))
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * 
   */
  bool isIdContinueChar(char32_t ch)
  {
    if ((ch == '$') || (ch == '_'))
    {
      return true;
    }
    else if ((ch == u'\u200C') || (ch == u'\u200D'))
    {
      return true;
    }
    else if ((ch >= U'\U00000030') && (ch <= U'\U00000039'))
    {
      return true;
    }
    else if ((ch >= U'\U00000041') && (ch <= U'\U0000005A'))
    {
      return true;
    }
    else if ((ch >= U'\U0000005F') && (ch <= U'\U0000005F'))
    {
      return true;
    }
    else if ((ch >= U'\U00000061') && (ch <= U'\U0000007A'))
    {
      return true;
    }
    else if ((ch >= U'\U000000AA') && (ch <= U'\U000000AA'))
    {
      return true;
    }
    else if ((ch >= U'\U000000B5') && (ch <= U'\U000000B5'))
    {
      return true;
    }
    else if ((ch >= U'\U000000B7') && (ch <= U'\U000000B7'))
    {
      return true;
    }
    else if ((ch >= U'\U000000BA') && (ch <= U'\U000000BA'))
    {
      return true;
    }
    else if ((ch >= U'\U000000C0') && (ch <= U'\U000000D6'))
    {
      return true;
    }
    else if ((ch >= U'\U000000D8') && (ch <= U'\U000000F6'))
    {
      return true;
    }
    else if ((ch >= U'\U000000F8') && (ch <= U'\U000002C1'))
    {
      return true;
    }
    else if ((ch >= U'\U000002C6') && (ch <= U'\U000002D1'))
    {
      return true;
    }
    else if ((ch >= U'\U000002E0') && (ch <= U'\U000002E4'))
    {
      return true;
    }
    else if ((ch >= U'\U000002EC') && (ch <= U'\U000002EC'))
    {
      return true;
    }
    else if ((ch >= U'\U000002EE') && (ch <= U'\U000002EE'))
    {
      return true;
    }
    else if ((ch >= U'\U00000300') && (ch <= U'\U00000374'))
    {
      return true;
    }
    else if ((ch >= U'\U00000376') && (ch <= U'\U00000377'))
    {
      return true;
    }
    else if ((ch >= U'\U0000037A') && (ch <= U'\U0000037D'))
    {
      return true;
    }
    else if ((ch >= U'\U0000037F') && (ch <= U'\U0000037F'))
    {
      return true;
    }
    else if ((ch >= U'\U00000386') && (ch <= U'\U0000038A'))
    {
      return true;
    }
    else if ((ch >= U'\U0000038C') && (ch <= U'\U0000038C'))
    {
      return true;
    }
    else if ((ch >= U'\U0000038E') && (ch <= U'\U000003A1'))
    {
      return true;
    }
    else if ((ch >= U'\U000003A3') && (ch <= U'\U000003F5'))
    {
      return true;
    }
    else if ((ch >= U'\U000003F7') && (ch <= U'\U00000481'))
    {
      return true;
    }
    else if ((ch >= U'\U00000483') && (ch <= U'\U00000487'))
    {
      return true;
    }
    else if ((ch >= U'\U0000048A') && (ch <= U'\U0000052F'))
    {
      return true;
    }
    else if ((ch >= U'\U00000531') && (ch <= U'\U00000556'))
    {
      return true;
    }
    else if ((ch >= U'\U00000559') && (ch <= U'\U00000559'))
    {
      return true;
    }
    else if ((ch >= U'\U00000561') && (ch <= U'\U00000587'))
    {
      return true;
    }
    else if ((ch >= U'\U00000591') && (ch <= U'\U000005BD'))
    {
      return true;
    }
    else if ((ch >= U'\U000005BF') && (ch <= U'\U000005BF'))
    {
      return true;
    }
    else if ((ch >= U'\U000005C1') && (ch <= U'\U000005C2'))
    {
      return true;
    }
    else if ((ch >= U'\U000005C4') && (ch <= U'\U000005C5'))
    {
      return true;
    }
    else if ((ch >= U'\U000005C7') && (ch <= U'\U000005C7'))
    {
      return true;
    }
    else if ((ch >= U'\U000005D0') && (ch <= U'\U000005EA'))
    {
      return true;
    }
    else if ((ch >= U'\U000005F0') && (ch <= U'\U000005F2'))
    {
      return true;
    }
    else if ((ch >= U'\U00000610') && (ch <= U'\U0000061A'))
    {
      return true;
    }
    else if ((ch >= U'\U00000620') && (ch <= U'\U00000669'))
    {
      return true;
    }
    else if ((ch >= U'\U0000066E') && (ch <= U'\U000006D3'))
    {
      return true;
    }
    else if ((ch >= U'\U000006D5') && (ch <= U'\U000006DC'))
    {
      return true;
    }
    else if ((ch >= U'\U000006DF') && (ch <= U'\U000006E8'))
    {
      return true;
    }
    else if ((ch >= U'\U000006EA') && (ch <= U'\U000006FC'))
    {
      return true;
    }
    else if ((ch >= U'\U000006FF') && (ch <= U'\U000006FF'))
    {
      return true;
    }
    else if ((ch >= U'\U00000710') && (ch <= U'\U0000074A'))
    {
      return true;
    }
    else if ((ch >= U'\U0000074D') && (ch <= U'\U000007B1'))
    {
      return true;
    }
    else if ((ch >= U'\U000007C0') && (ch <= U'\U000007F5'))
    {
      return true;
    }
    else if ((ch >= U'\U000007FA') && (ch <= U'\U000007FA'))
    {
      return true;
    }
    else if ((ch >= U'\U00000800') && (ch <= U'\U0000082D'))
    {
      return true;
    }
    else if ((ch >= U'\U00000840') && (ch <= U'\U0000085B'))
    {
      return true;
    }
    else if ((ch >= U'\U000008A0') && (ch <= U'\U000008B4'))
    {
      return true;
    }
    else if ((ch >= U'\U000008B6') && (ch <= U'\U000008BD'))
    {
      return true;
    }
    else if ((ch >= U'\U000008D4') && (ch <= U'\U000008E1'))
    {
      return true;
    }
    else if ((ch >= U'\U000008E3') && (ch <= U'\U00000963'))
    {
      return true;
    }
    else if ((ch >= U'\U00000966') && (ch <= U'\U0000096F'))
    {
      return true;
    }
    else if ((ch >= U'\U00000971') && (ch <= U'\U00000983'))
    {
      return true;
    }
    else if ((ch >= U'\U00000985') && (ch <= U'\U0000098C'))
    {
      return true;
    }
    else if ((ch >= U'\U0000098F') && (ch <= U'\U00000990'))
    {
      return true;
    }
    else if ((ch >= U'\U00000993') && (ch <= U'\U000009A8'))
    {
      return true;
    }
    else if ((ch >= U'\U000009AA') && (ch <= U'\U000009B0'))
    {
      return true;
    }
    else if ((ch >= U'\U000009B2') && (ch <= U'\U000009B2'))
    {
      return true;
    }
    else if ((ch >= U'\U000009B6') && (ch <= U'\U000009B9'))
    {
      return true;
    }
    else if ((ch >= U'\U000009BC') && (ch <= U'\U000009C4'))
    {
      return true;
    }
    else if ((ch >= U'\U000009C7') && (ch <= U'\U000009C8'))
    {
      return true;
    }
    else if ((ch >= U'\U000009CB') && (ch <= U'\U000009CE'))
    {
      return true;
    }
    else if ((ch >= U'\U000009D7') && (ch <= U'\U000009D7'))
    {
      return true;
    }
    else if ((ch >= U'\U000009DC') && (ch <= U'\U000009DD'))
    {
      return true;
    }
    else if ((ch >= U'\U000009DF') && (ch <= U'\U000009E3'))
    {
      return true;
    }
    else if ((ch >= U'\U000009E6') && (ch <= U'\U000009F1'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A01') && (ch <= U'\U00000A03'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A05') && (ch <= U'\U00000A0A'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A0F') && (ch <= U'\U00000A10'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A13') && (ch <= U'\U00000A28'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A2A') && (ch <= U'\U00000A30'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A32') && (ch <= U'\U00000A33'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A35') && (ch <= U'\U00000A36'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A38') && (ch <= U'\U00000A39'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A3C') && (ch <= U'\U00000A3C'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A3E') && (ch <= U'\U00000A42'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A47') && (ch <= U'\U00000A48'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A4B') && (ch <= U'\U00000A4D'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A51') && (ch <= U'\U00000A51'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A59') && (ch <= U'\U00000A5C'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A5E') && (ch <= U'\U00000A5E'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A66') && (ch <= U'\U00000A75'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A81') && (ch <= U'\U00000A83'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A85') && (ch <= U'\U00000A8D'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A8F') && (ch <= U'\U00000A91'))
    {
      return true;
    }
    else if ((ch >= U'\U00000A93') && (ch <= U'\U00000AA8'))
    {
      return true;
    }
    else if ((ch >= U'\U00000AAA') && (ch <= U'\U00000AB0'))
    {
      return true;
    }
    else if ((ch >= U'\U00000AB2') && (ch <= U'\U00000AB3'))
    {
      return true;
    }
    else if ((ch >= U'\U00000AB5') && (ch <= U'\U00000AB9'))
    {
      return true;
    }
    else if ((ch >= U'\U00000ABC') && (ch <= U'\U00000AC5'))
    {
      return true;
    }
    else if ((ch >= U'\U00000AC7') && (ch <= U'\U00000AC9'))
    {
      return true;
    }
    else if ((ch >= U'\U00000ACB') && (ch <= U'\U00000ACD'))
    {
      return true;
    }
    else if ((ch >= U'\U00000AD0') && (ch <= U'\U00000AD0'))
    {
      return true;
    }
    else if ((ch >= U'\U00000AE0') && (ch <= U'\U00000AE3'))
    {
      return true;
    }
    else if ((ch >= U'\U00000AE6') && (ch <= U'\U00000AEF'))
    {
      return true;
    }
    else if ((ch >= U'\U00000AF9') && (ch <= U'\U00000AF9'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B01') && (ch <= U'\U00000B03'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B05') && (ch <= U'\U00000B0C'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B0F') && (ch <= U'\U00000B10'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B13') && (ch <= U'\U00000B28'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B2A') && (ch <= U'\U00000B30'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B32') && (ch <= U'\U00000B33'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B35') && (ch <= U'\U00000B39'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B3C') && (ch <= U'\U00000B44'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B47') && (ch <= U'\U00000B48'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B4B') && (ch <= U'\U00000B4D'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B56') && (ch <= U'\U00000B57'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B5C') && (ch <= U'\U00000B5D'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B5F') && (ch <= U'\U00000B63'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B66') && (ch <= U'\U00000B6F'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B71') && (ch <= U'\U00000B71'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B82') && (ch <= U'\U00000B83'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B85') && (ch <= U'\U00000B8A'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B8E') && (ch <= U'\U00000B90'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B92') && (ch <= U'\U00000B95'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B99') && (ch <= U'\U00000B9A'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B9C') && (ch <= U'\U00000B9C'))
    {
      return true;
    }
    else if ((ch >= U'\U00000B9E') && (ch <= U'\U00000B9F'))
    {
      return true;
    }
    else if ((ch >= U'\U00000BA3') && (ch <= U'\U00000BA4'))
    {
      return true;
    }
    else if ((ch >= U'\U00000BA8') && (ch <= U'\U00000BAA'))
    {
      return true;
    }
    else if ((ch >= U'\U00000BAE') && (ch <= U'\U00000BB9'))
    {
      return true;
    }
    else if ((ch >= U'\U00000BBE') && (ch <= U'\U00000BC2'))
    {
      return true;
    }
    else if ((ch >= U'\U00000BC6') && (ch <= U'\U00000BC8'))
    {
      return true;
    }
    else if ((ch >= U'\U00000BCA') && (ch <= U'\U00000BCD'))
    {
      return true;
    }
    else if ((ch >= U'\U00000BD0') && (ch <= U'\U00000BD0'))
    {
      return true;
    }
    else if ((ch >= U'\U00000BD7') && (ch <= U'\U00000BD7'))
    {
      return true;
    }
    else if ((ch >= U'\U00000BE6') && (ch <= U'\U00000BEF'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C00') && (ch <= U'\U00000C03'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C05') && (ch <= U'\U00000C0C'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C0E') && (ch <= U'\U00000C10'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C12') && (ch <= U'\U00000C28'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C2A') && (ch <= U'\U00000C39'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C3D') && (ch <= U'\U00000C44'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C46') && (ch <= U'\U00000C48'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C4A') && (ch <= U'\U00000C4D'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C55') && (ch <= U'\U00000C56'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C58') && (ch <= U'\U00000C5A'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C60') && (ch <= U'\U00000C63'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C66') && (ch <= U'\U00000C6F'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C80') && (ch <= U'\U00000C83'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C85') && (ch <= U'\U00000C8C'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C8E') && (ch <= U'\U00000C90'))
    {
      return true;
    }
    else if ((ch >= U'\U00000C92') && (ch <= U'\U00000CA8'))
    {
      return true;
    }
    else if ((ch >= U'\U00000CAA') && (ch <= U'\U00000CB3'))
    {
      return true;
    }
    else if ((ch >= U'\U00000CB5') && (ch <= U'\U00000CB9'))
    {
      return true;
    }
    else if ((ch >= U'\U00000CBC') && (ch <= U'\U00000CC4'))
    {
      return true;
    }
    else if ((ch >= U'\U00000CC6') && (ch <= U'\U00000CC8'))
    {
      return true;
    }
    else if ((ch >= U'\U00000CCA') && (ch <= U'\U00000CCD'))
    {
      return true;
    }
    else if ((ch >= U'\U00000CD5') && (ch <= U'\U00000CD6'))
    {
      return true;
    }
    else if ((ch >= U'\U00000CDE') && (ch <= U'\U00000CDE'))
    {
      return true;
    }
    else if ((ch >= U'\U00000CE0') && (ch <= U'\U00000CE3'))
    {
      return true;
    }
    else if ((ch >= U'\U00000CE6') && (ch <= U'\U00000CEF'))
    {
      return true;
    }
    else if ((ch >= U'\U00000CF1') && (ch <= U'\U00000CF2'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D01') && (ch <= U'\U00000D03'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D05') && (ch <= U'\U00000D0C'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D0E') && (ch <= U'\U00000D10'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D12') && (ch <= U'\U00000D3A'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D3D') && (ch <= U'\U00000D44'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D46') && (ch <= U'\U00000D48'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D4A') && (ch <= U'\U00000D4E'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D54') && (ch <= U'\U00000D57'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D5F') && (ch <= U'\U00000D63'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D66') && (ch <= U'\U00000D6F'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D7A') && (ch <= U'\U00000D7F'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D82') && (ch <= U'\U00000D83'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D85') && (ch <= U'\U00000D96'))
    {
      return true;
    }
    else if ((ch >= U'\U00000D9A') && (ch <= U'\U00000DB1'))
    {
      return true;
    }
    else if ((ch >= U'\U00000DB3') && (ch <= U'\U00000DBB'))
    {
      return true;
    }
    else if ((ch >= U'\U00000DBD') && (ch <= U'\U00000DBD'))
    {
      return true;
    }
    else if ((ch >= U'\U00000DC0') && (ch <= U'\U00000DC6'))
    {
      return true;
    }
    else if ((ch >= U'\U00000DCA') && (ch <= U'\U00000DCA'))
    {
      return true;
    }
    else if ((ch >= U'\U00000DCF') && (ch <= U'\U00000DD4'))
    {
      return true;
    }
    else if ((ch >= U'\U00000DD6') && (ch <= U'\U00000DD6'))
    {
      return true;
    }
    else if ((ch >= U'\U00000DD8') && (ch <= U'\U00000DDF'))
    {
      return true;
    }
    else if ((ch >= U'\U00000DE6') && (ch <= U'\U00000DEF'))
    {
      return true;
    }
    else if ((ch >= U'\U00000DF2') && (ch <= U'\U00000DF3'))
    {
      return true;
    }
    else if ((ch >= U'\U00000E01') && (ch <= U'\U00000E3A'))
    {
      return true;
    }
    else if ((ch >= U'\U00000E40') && (ch <= U'\U00000E4E'))
    {
      return true;
    }
    else if ((ch >= U'\U00000E50') && (ch <= U'\U00000E59'))
    {
      return true;
    }
    else if ((ch >= U'\U00000E81') && (ch <= U'\U00000E82'))
    {
      return true;
    }
    else if ((ch >= U'\U00000E84') && (ch <= U'\U00000E84'))
    {
      return true;
    }
    else if ((ch >= U'\U00000E87') && (ch <= U'\U00000E88'))
    {
      return true;
    }
    else if ((ch >= U'\U00000E8A') && (ch <= U'\U00000E8A'))
    {
      return true;
    }
    else if ((ch >= U'\U00000E8D') && (ch <= U'\U00000E8D'))
    {
      return true;
    }
    else if ((ch >= U'\U00000E94') && (ch <= U'\U00000E97'))
    {
      return true;
    }
    else if ((ch >= U'\U00000E99') && (ch <= U'\U00000E9F'))
    {
      return true;
    }
    else if ((ch >= U'\U00000EA1') && (ch <= U'\U00000EA3'))
    {
      return true;
    }
    else if ((ch >= U'\U00000EA5') && (ch <= U'\U00000EA5'))
    {
      return true;
    }
    else if ((ch >= U'\U00000EA7') && (ch <= U'\U00000EA7'))
    {
      return true;
    }
    else if ((ch >= U'\U00000EAA') && (ch <= U'\U00000EAB'))
    {
      return true;
    }
    else if ((ch >= U'\U00000EAD') && (ch <= U'\U00000EB9'))
    {
      return true;
    }
    else if ((ch >= U'\U00000EBB') && (ch <= U'\U00000EBD'))
    {
      return true;
    }
    else if ((ch >= U'\U00000EC0') && (ch <= U'\U00000EC4'))
    {
      return true;
    }
    else if ((ch >= U'\U00000EC6') && (ch <= U'\U00000EC6'))
    {
      return true;
    }
    else if ((ch >= U'\U00000EC8') && (ch <= U'\U00000ECD'))
    {
      return true;
    }
    else if ((ch >= U'\U00000ED0') && (ch <= U'\U00000ED9'))
    {
      return true;
    }
    else if ((ch >= U'\U00000EDC') && (ch <= U'\U00000EDF'))
    {
      return true;
    }
    else if ((ch >= U'\U00000F00') && (ch <= U'\U00000F00'))
    {
      return true;
    }
    else if ((ch >= U'\U00000F18') && (ch <= U'\U00000F19'))
    {
      return true;
    }
    else if ((ch >= U'\U00000F20') && (ch <= U'\U00000F29'))
    {
      return true;
    }
    else if ((ch >= U'\U00000F35') && (ch <= U'\U00000F35'))
    {
      return true;
    }
    else if ((ch >= U'\U00000F37') && (ch <= U'\U00000F37'))
    {
      return true;
    }
    else if ((ch >= U'\U00000F39') && (ch <= U'\U00000F39'))
    {
      return true;
    }
    else if ((ch >= U'\U00000F3E') && (ch <= U'\U00000F47'))
    {
      return true;
    }
    else if ((ch >= U'\U00000F49') && (ch <= U'\U00000F6C'))
    {
      return true;
    }
    else if ((ch >= U'\U00000F71') && (ch <= U'\U00000F84'))
    {
      return true;
    }
    else if ((ch >= U'\U00000F86') && (ch <= U'\U00000F97'))
    {
      return true;
    }
    else if ((ch >= U'\U00000F99') && (ch <= U'\U00000FBC'))
    {
      return true;
    }
    else if ((ch >= U'\U00000FC6') && (ch <= U'\U00000FC6'))
    {
      return true;
    }
    else if ((ch >= U'\U00001000') && (ch <= U'\U00001049'))
    {
      return true;
    }
    else if ((ch >= U'\U00001050') && (ch <= U'\U0000109D'))
    {
      return true;
    }
    else if ((ch >= U'\U000010A0') && (ch <= U'\U000010C5'))
    {
      return true;
    }
    else if ((ch >= U'\U000010C7') && (ch <= U'\U000010C7'))
    {
      return true;
    }
    else if ((ch >= U'\U000010CD') && (ch <= U'\U000010CD'))
    {
      return true;
    }
    else if ((ch >= U'\U000010D0') && (ch <= U'\U000010FA'))
    {
      return true;
    }
    else if ((ch >= U'\U000010FC') && (ch <= U'\U00001248'))
    {
      return true;
    }
    else if ((ch >= U'\U0000124A') && (ch <= U'\U0000124D'))
    {
      return true;
    }
    else if ((ch >= U'\U00001250') && (ch <= U'\U00001256'))
    {
      return true;
    }
    else if ((ch >= U'\U00001258') && (ch <= U'\U00001258'))
    {
      return true;
    }
    else if ((ch >= U'\U0000125A') && (ch <= U'\U0000125D'))
    {
      return true;
    }
    else if ((ch >= U'\U00001260') && (ch <= U'\U00001288'))
    {
      return true;
    }
    else if ((ch >= U'\U0000128A') && (ch <= U'\U0000128D'))
    {
      return true;
    }
    else if ((ch >= U'\U00001290') && (ch <= U'\U000012B0'))
    {
      return true;
    }
    else if ((ch >= U'\U000012B2') && (ch <= U'\U000012B5'))
    {
      return true;
    }
    else if ((ch >= U'\U000012B8') && (ch <= U'\U000012BE'))
    {
      return true;
    }
    else if ((ch >= U'\U000012C0') && (ch <= U'\U000012C0'))
    {
      return true;
    }
    else if ((ch >= U'\U000012C2') && (ch <= U'\U000012C5'))
    {
      return true;
    }
    else if ((ch >= U'\U000012C8') && (ch <= U'\U000012D6'))
    {
      return true;
    }
    else if ((ch >= U'\U000012D8') && (ch <= U'\U00001310'))
    {
      return true;
    }
    else if ((ch >= U'\U00001312') && (ch <= U'\U00001315'))
    {
      return true;
    }
    else if ((ch >= U'\U00001318') && (ch <= U'\U0000135A'))
    {
      return true;
    }
    else if ((ch >= U'\U0000135D') && (ch <= U'\U0000135F'))
    {
      return true;
    }
    else if ((ch >= U'\U00001369') && (ch <= U'\U00001371'))
    {
      return true;
    }
    else if ((ch >= U'\U00001380') && (ch <= U'\U0000138F'))
    {
      return true;
    }
    else if ((ch >= U'\U000013A0') && (ch <= U'\U000013F5'))
    {
      return true;
    }
    else if ((ch >= U'\U000013F8') && (ch <= U'\U000013FD'))
    {
      return true;
    }
    else if ((ch >= U'\U00001401') && (ch <= U'\U0000166C'))
    {
      return true;
    }
    else if ((ch >= U'\U0000166F') && (ch <= U'\U0000167F'))
    {
      return true;
    }
    else if ((ch >= U'\U00001681') && (ch <= U'\U0000169A'))
    {
      return true;
    }
    else if ((ch >= U'\U000016A0') && (ch <= U'\U000016EA'))
    {
      return true;
    }
    else if ((ch >= U'\U000016EE') && (ch <= U'\U000016F8'))
    {
      return true;
    }
    else if ((ch >= U'\U00001700') && (ch <= U'\U0000170C'))
    {
      return true;
    }
    else if ((ch >= U'\U0000170E') && (ch <= U'\U00001714'))
    {
      return true;
    }
    else if ((ch >= U'\U00001720') && (ch <= U'\U00001734'))
    {
      return true;
    }
    else if ((ch >= U'\U00001740') && (ch <= U'\U00001753'))
    {
      return true;
    }
    else if ((ch >= U'\U00001760') && (ch <= U'\U0000176C'))
    {
      return true;
    }
    else if ((ch >= U'\U0000176E') && (ch <= U'\U00001770'))
    {
      return true;
    }
    else if ((ch >= U'\U00001772') && (ch <= U'\U00001773'))
    {
      return true;
    }
    else if ((ch >= U'\U00001780') && (ch <= U'\U000017D3'))
    {
      return true;
    }
    else if ((ch >= U'\U000017D7') && (ch <= U'\U000017D7'))
    {
      return true;
    }
    else if ((ch >= U'\U000017DC') && (ch <= U'\U000017DD'))
    {
      return true;
    }
    else if ((ch >= U'\U000017E0') && (ch <= U'\U000017E9'))
    {
      return true;
    }
    else if ((ch >= U'\U0000180B') && (ch <= U'\U0000180D'))
    {
      return true;
    }
    else if ((ch >= U'\U00001810') && (ch <= U'\U00001819'))
    {
      return true;
    }
    else if ((ch >= U'\U00001820') && (ch <= U'\U00001877'))
    {
      return true;
    }
    else if ((ch >= U'\U00001880') && (ch <= U'\U000018AA'))
    {
      return true;
    }
    else if ((ch >= U'\U000018B0') && (ch <= U'\U000018F5'))
    {
      return true;
    }
    else if ((ch >= U'\U00001900') && (ch <= U'\U0000191E'))
    {
      return true;
    }
    else if ((ch >= U'\U00001920') && (ch <= U'\U0000192B'))
    {
      return true;
    }
    else if ((ch >= U'\U00001930') && (ch <= U'\U0000193B'))
    {
      return true;
    }
    else if ((ch >= U'\U00001946') && (ch <= U'\U0000196D'))
    {
      return true;
    }
    else if ((ch >= U'\U00001970') && (ch <= U'\U00001974'))
    {
      return true;
    }
    else if ((ch >= U'\U00001980') && (ch <= U'\U000019AB'))
    {
      return true;
    }
    else if ((ch >= U'\U000019B0') && (ch <= U'\U000019C9'))
    {
      return true;
    }
    else if ((ch >= U'\U000019D0') && (ch <= U'\U000019DA'))
    {
      return true;
    }
    else if ((ch >= U'\U00001A00') && (ch <= U'\U00001A1B'))
    {
      return true;
    }
    else if ((ch >= U'\U00001A20') && (ch <= U'\U00001A5E'))
    {
      return true;
    }
    else if ((ch >= U'\U00001A60') && (ch <= U'\U00001A7C'))
    {
      return true;
    }
    else if ((ch >= U'\U00001A7F') && (ch <= U'\U00001A89'))
    {
      return true;
    }
    else if ((ch >= U'\U00001A90') && (ch <= U'\U00001A99'))
    {
      return true;
    }
    else if ((ch >= U'\U00001AA7') && (ch <= U'\U00001AA7'))
    {
      return true;
    }
    else if ((ch >= U'\U00001AB0') && (ch <= U'\U00001ABD'))
    {
      return true;
    }
    else if ((ch >= U'\U00001B00') && (ch <= U'\U00001B4B'))
    {
      return true;
    }
    else if ((ch >= U'\U00001B50') && (ch <= U'\U00001B59'))
    {
      return true;
    }
    else if ((ch >= U'\U00001B6B') && (ch <= U'\U00001B73'))
    {
      return true;
    }
    else if ((ch >= U'\U00001B80') && (ch <= U'\U00001BF3'))
    {
      return true;
    }
    else if ((ch >= U'\U00001C00') && (ch <= U'\U00001C37'))
    {
      return true;
    }
    else if ((ch >= U'\U00001C40') && (ch <= U'\U00001C49'))
    {
      return true;
    }
    else if ((ch >= U'\U00001C4D') && (ch <= U'\U00001C7D'))
    {
      return true;
    }
    else if ((ch >= U'\U00001C80') && (ch <= U'\U00001C88'))
    {
      return true;
    }
    else if ((ch >= U'\U00001CD0') && (ch <= U'\U00001CD2'))
    {
      return true;
    }
    else if ((ch >= U'\U00001CD4') && (ch <= U'\U00001CF6'))
    {
      return true;
    }
    else if ((ch >= U'\U00001CF8') && (ch <= U'\U00001CF9'))
    {
      return true;
    }
    else if ((ch >= U'\U00001D00') && (ch <= U'\U00001DF5'))
    {
      return true;
    }
    else if ((ch >= U'\U00001DFB') && (ch <= U'\U00001F15'))
    {
      return true;
    }
    else if ((ch >= U'\U00001F18') && (ch <= U'\U00001F1D'))
    {
      return true;
    }
    else if ((ch >= U'\U00001F20') && (ch <= U'\U00001F45'))
    {
      return true;
    }
    else if ((ch >= U'\U00001F48') && (ch <= U'\U00001F4D'))
    {
      return true;
    }
    else if ((ch >= U'\U00001F50') && (ch <= U'\U00001F57'))
    {
      return true;
    }
    else if ((ch >= U'\U00001F59') && (ch <= U'\U00001F59'))
    {
      return true;
    }
    else if ((ch >= U'\U00001F5B') && (ch <= U'\U00001F5B'))
    {
      return true;
    }
    else if ((ch >= U'\U00001F5D') && (ch <= U'\U00001F5D'))
    {
      return true;
    }
    else if ((ch >= U'\U00001F5F') && (ch <= U'\U00001F7D'))
    {
      return true;
    }
    else if ((ch >= U'\U00001F80') && (ch <= U'\U00001FB4'))
    {
      return true;
    }
    else if ((ch >= U'\U00001FB6') && (ch <= U'\U00001FBC'))
    {
      return true;
    }
    else if ((ch >= U'\U00001FBE') && (ch <= U'\U00001FBE'))
    {
      return true;
    }
    else if ((ch >= U'\U00001FC2') && (ch <= U'\U00001FC4'))
    {
      return true;
    }
    else if ((ch >= U'\U00001FC6') && (ch <= U'\U00001FCC'))
    {
      return true;
    }
    else if ((ch >= U'\U00001FD0') && (ch <= U'\U00001FD3'))
    {
      return true;
    }
    else if ((ch >= U'\U00001FD6') && (ch <= U'\U00001FDB'))
    {
      return true;
    }
    else if ((ch >= U'\U00001FE0') && (ch <= U'\U00001FEC'))
    {
      return true;
    }
    else if ((ch >= U'\U00001FF2') && (ch <= U'\U00001FF4'))
    {
      return true;
    }
    else if ((ch >= U'\U00001FF6') && (ch <= U'\U00001FFC'))
    {
      return true;
    }
    else if ((ch >= U'\U0000203F') && (ch <= U'\U00002040'))
    {
      return true;
    }
    else if ((ch >= U'\U00002054') && (ch <= U'\U00002054'))
    {
      return true;
    }
    else if ((ch >= U'\U00002071') && (ch <= U'\U00002071'))
    {
      return true;
    }
    else if ((ch >= U'\U0000207F') && (ch <= U'\U0000207F'))
    {
      return true;
    }
    else if ((ch >= U'\U00002090') && (ch <= U'\U0000209C'))
    {
      return true;
    }
    else if ((ch >= U'\U000020D0') && (ch <= U'\U000020DC'))
    {
      return true;
    }
    else if ((ch >= U'\U000020E1') && (ch <= U'\U000020E1'))
    {
      return true;
    }
    else if ((ch >= U'\U000020E5') && (ch <= U'\U000020F0'))
    {
      return true;
    }
    else if ((ch >= U'\U00002102') && (ch <= U'\U00002102'))
    {
      return true;
    }
    else if ((ch >= U'\U00002107') && (ch <= U'\U00002107'))
    {
      return true;
    }
    else if ((ch >= U'\U0000210A') && (ch <= U'\U00002113'))
    {
      return true;
    }
    else if ((ch >= U'\U00002115') && (ch <= U'\U00002115'))
    {
      return true;
    }
    else if ((ch >= U'\U00002118') && (ch <= U'\U0000211D'))
    {
      return true;
    }
    else if ((ch >= U'\U00002124') && (ch <= U'\U00002124'))
    {
      return true;
    }
    else if ((ch >= U'\U00002126') && (ch <= U'\U00002126'))
    {
      return true;
    }
    else if ((ch >= U'\U00002128') && (ch <= U'\U00002128'))
    {
      return true;
    }
    else if ((ch >= U'\U0000212A') && (ch <= U'\U00002139'))
    {
      return true;
    }
    else if ((ch >= U'\U0000213C') && (ch <= U'\U0000213F'))
    {
      return true;
    }
    else if ((ch >= U'\U00002145') && (ch <= U'\U00002149'))
    {
      return true;
    }
    else if ((ch >= U'\U0000214E') && (ch <= U'\U0000214E'))
    {
      return true;
    }
    else if ((ch >= U'\U00002160') && (ch <= U'\U00002188'))
    {
      return true;
    }
    else if ((ch >= U'\U00002C00') && (ch <= U'\U00002C2E'))
    {
      return true;
    }
    else if ((ch >= U'\U00002C30') && (ch <= U'\U00002C5E'))
    {
      return true;
    }
    else if ((ch >= U'\U00002C60') && (ch <= U'\U00002CE4'))
    {
      return true;
    }
    else if ((ch >= U'\U00002CEB') && (ch <= U'\U00002CF3'))
    {
      return true;
    }
    else if ((ch >= U'\U00002D00') && (ch <= U'\U00002D25'))
    {
      return true;
    }
    else if ((ch >= U'\U00002D27') && (ch <= U'\U00002D27'))
    {
      return true;
    }
    else if ((ch >= U'\U00002D2D') && (ch <= U'\U00002D2D'))
    {
      return true;
    }
    else if ((ch >= U'\U00002D30') && (ch <= U'\U00002D67'))
    {
      return true;
    }
    else if ((ch >= U'\U00002D6F') && (ch <= U'\U00002D6F'))
    {
      return true;
    }
    else if ((ch >= U'\U00002D7F') && (ch <= U'\U00002D96'))
    {
      return true;
    }
    else if ((ch >= U'\U00002DA0') && (ch <= U'\U00002DA6'))
    {
      return true;
    }
    else if ((ch >= U'\U00002DA8') && (ch <= U'\U00002DAE'))
    {
      return true;
    }
    else if ((ch >= U'\U00002DB0') && (ch <= U'\U00002DB6'))
    {
      return true;
    }
    else if ((ch >= U'\U00002DB8') && (ch <= U'\U00002DBE'))
    {
      return true;
    }
    else if ((ch >= U'\U00002DC0') && (ch <= U'\U00002DC6'))
    {
      return true;
    }
    else if ((ch >= U'\U00002DC8') && (ch <= U'\U00002DCE'))
    {
      return true;
    }
    else if ((ch >= U'\U00002DD0') && (ch <= U'\U00002DD6'))
    {
      return true;
    }
    else if ((ch >= U'\U00002DD8') && (ch <= U'\U00002DDE'))
    {
      return true;
    }
    else if ((ch >= U'\U00002DE0') && (ch <= U'\U00002DFF'))
    {
      return true;
    }
    else if ((ch >= U'\U00003005') && (ch <= U'\U00003007'))
    {
      return true;
    }
    else if ((ch >= U'\U00003021') && (ch <= U'\U0000302F'))
    {
      return true;
    }
    else if ((ch >= U'\U00003031') && (ch <= U'\U00003035'))
    {
      return true;
    }
    else if ((ch >= U'\U00003038') && (ch <= U'\U0000303C'))
    {
      return true;
    }
    else if ((ch >= U'\U00003041') && (ch <= U'\U00003096'))
    {
      return true;
    }
    else if ((ch >= U'\U00003099') && (ch <= U'\U0000309F'))
    {
      return true;
    }
    else if ((ch >= U'\U000030A1') && (ch <= U'\U000030FA'))
    {
      return true;
    }
    else if ((ch >= U'\U000030FC') && (ch <= U'\U000030FF'))
    {
      return true;
    }
    else if ((ch >= U'\U00003105') && (ch <= U'\U0000312D'))
    {
      return true;
    }
    else if ((ch >= U'\U00003131') && (ch <= U'\U0000318E'))
    {
      return true;
    }
    else if ((ch >= U'\U000031A0') && (ch <= U'\U000031BA'))
    {
      return true;
    }
    else if ((ch >= U'\U000031F0') && (ch <= U'\U000031FF'))
    {
      return true;
    }
    else if ((ch >= U'\U00003400') && (ch <= U'\U00004DB5'))
    {
      return true;
    }
    else if ((ch >= U'\U00004E00') && (ch <= U'\U00009FD5'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A000') && (ch <= U'\U0000A48C'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A4D0') && (ch <= U'\U0000A4FD'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A500') && (ch <= U'\U0000A60C'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A610') && (ch <= U'\U0000A62B'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A640') && (ch <= U'\U0000A66F'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A674') && (ch <= U'\U0000A67D'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A67F') && (ch <= U'\U0000A6F1'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A717') && (ch <= U'\U0000A71F'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A722') && (ch <= U'\U0000A788'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A78B') && (ch <= U'\U0000A7AE'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A7B0') && (ch <= U'\U0000A7B7'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A7F7') && (ch <= U'\U0000A827'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A840') && (ch <= U'\U0000A873'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A880') && (ch <= U'\U0000A8C5'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A8D0') && (ch <= U'\U0000A8D9'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A8E0') && (ch <= U'\U0000A8F7'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A8FB') && (ch <= U'\U0000A8FB'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A8FD') && (ch <= U'\U0000A8FD'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A900') && (ch <= U'\U0000A92D'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A930') && (ch <= U'\U0000A953'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A960') && (ch <= U'\U0000A97C'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A980') && (ch <= U'\U0000A9C0'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A9CF') && (ch <= U'\U0000A9D9'))
    {
      return true;
    }
    else if ((ch >= U'\U0000A9E0') && (ch <= U'\U0000A9FE'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AA00') && (ch <= U'\U0000AA36'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AA40') && (ch <= U'\U0000AA4D'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AA50') && (ch <= U'\U0000AA59'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AA60') && (ch <= U'\U0000AA76'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AA7A') && (ch <= U'\U0000AAC2'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AADB') && (ch <= U'\U0000AADD'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AAE0') && (ch <= U'\U0000AAEF'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AAF2') && (ch <= U'\U0000AAF6'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AB01') && (ch <= U'\U0000AB06'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AB09') && (ch <= U'\U0000AB0E'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AB11') && (ch <= U'\U0000AB16'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AB20') && (ch <= U'\U0000AB26'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AB28') && (ch <= U'\U0000AB2E'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AB30') && (ch <= U'\U0000AB5A'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AB5C') && (ch <= U'\U0000AB65'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AB70') && (ch <= U'\U0000ABEA'))
    {
      return true;
    }
    else if ((ch >= U'\U0000ABEC') && (ch <= U'\U0000ABED'))
    {
      return true;
    }
    else if ((ch >= U'\U0000ABF0') && (ch <= U'\U0000ABF9'))
    {
      return true;
    }
    else if ((ch >= U'\U0000AC00') && (ch <= U'\U0000D7A3'))
    {
      return true;
    }
    else if ((ch >= U'\U0000D7B0') && (ch <= U'\U0000D7C6'))
    {
      return true;
    }
    else if ((ch >= U'\U0000D7CB') && (ch <= U'\U0000D7FB'))
    {
      return true;
    }
    else if ((ch >= U'\U0000F900') && (ch <= U'\U0000FA6D'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FA70') && (ch <= U'\U0000FAD9'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FB00') && (ch <= U'\U0000FB06'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FB13') && (ch <= U'\U0000FB17'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FB1D') && (ch <= U'\U0000FB28'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FB2A') && (ch <= U'\U0000FB36'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FB38') && (ch <= U'\U0000FB3C'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FB3E') && (ch <= U'\U0000FB3E'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FB40') && (ch <= U'\U0000FB41'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FB43') && (ch <= U'\U0000FB44'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FB46') && (ch <= U'\U0000FBB1'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FBD3') && (ch <= U'\U0000FD3D'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FD50') && (ch <= U'\U0000FD8F'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FD92') && (ch <= U'\U0000FDC7'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FDF0') && (ch <= U'\U0000FDFB'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FE00') && (ch <= U'\U0000FE0F'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FE20') && (ch <= U'\U0000FE2F'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FE33') && (ch <= U'\U0000FE34'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FE4D') && (ch <= U'\U0000FE4F'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FE70') && (ch <= U'\U0000FE74'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FE76') && (ch <= U'\U0000FEFC'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FF10') && (ch <= U'\U0000FF19'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FF21') && (ch <= U'\U0000FF3A'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FF3F') && (ch <= U'\U0000FF3F'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FF41') && (ch <= U'\U0000FF5A'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FF66') && (ch <= U'\U0000FFBE'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FFC2') && (ch <= U'\U0000FFC7'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FFCA') && (ch <= U'\U0000FFCF'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FFD2') && (ch <= U'\U0000FFD7'))
    {
      return true;
    }
    else if ((ch >= U'\U0000FFDA') && (ch <= U'\U0000FFDC'))
    {
      return true;
    }
    else if ((ch >= U'\U00010000') && (ch <= U'\U0001000B'))
    {
      return true;
    }
    else if ((ch >= U'\U0001000D') && (ch <= U'\U00010026'))
    {
      return true;
    }
    else if ((ch >= U'\U00010028') && (ch <= U'\U0001003A'))
    {
      return true;
    }
    else if ((ch >= U'\U0001003C') && (ch <= U'\U0001003D'))
    {
      return true;
    }
    else if ((ch >= U'\U0001003F') && (ch <= U'\U0001004D'))
    {
      return true;
    }
    else if ((ch >= U'\U00010050') && (ch <= U'\U0001005D'))
    {
      return true;
    }
    else if ((ch >= U'\U00010080') && (ch <= U'\U000100FA'))
    {
      return true;
    }
    else if ((ch >= U'\U00010140') && (ch <= U'\U00010174'))
    {
      return true;
    }
    else if ((ch >= U'\U000101FD') && (ch <= U'\U000101FD'))
    {
      return true;
    }
    else if ((ch >= U'\U00010280') && (ch <= U'\U0001029C'))
    {
      return true;
    }
    else if ((ch >= U'\U000102A0') && (ch <= U'\U000102D0'))
    {
      return true;
    }
    else if ((ch >= U'\U000102E0') && (ch <= U'\U000102E0'))
    {
      return true;
    }
    else if ((ch >= U'\U00010300') && (ch <= U'\U0001031F'))
    {
      return true;
    }
    else if ((ch >= U'\U00010330') && (ch <= U'\U0001034A'))
    {
      return true;
    }
    else if ((ch >= U'\U00010350') && (ch <= U'\U0001037A'))
    {
      return true;
    }
    else if ((ch >= U'\U00010380') && (ch <= U'\U0001039D'))
    {
      return true;
    }
    else if ((ch >= U'\U000103A0') && (ch <= U'\U000103C3'))
    {
      return true;
    }
    else if ((ch >= U'\U000103C8') && (ch <= U'\U000103CF'))
    {
      return true;
    }
    else if ((ch >= U'\U000103D1') && (ch <= U'\U000103D5'))
    {
      return true;
    }
    else if ((ch >= U'\U00010400') && (ch <= U'\U0001049D'))
    {
      return true;
    }
    else if ((ch >= U'\U000104A0') && (ch <= U'\U000104A9'))
    {
      return true;
    }
    else if ((ch >= U'\U000104B0') && (ch <= U'\U000104D3'))
    {
      return true;
    }
    else if ((ch >= U'\U000104D8') && (ch <= U'\U000104FB'))
    {
      return true;
    }
    else if ((ch >= U'\U00010500') && (ch <= U'\U00010527'))
    {
      return true;
    }
    else if ((ch >= U'\U00010530') && (ch <= U'\U00010563'))
    {
      return true;
    }
    else if ((ch >= U'\U00010600') && (ch <= U'\U00010736'))
    {
      return true;
    }
    else if ((ch >= U'\U00010740') && (ch <= U'\U00010755'))
    {
      return true;
    }
    else if ((ch >= U'\U00010760') && (ch <= U'\U00010767'))
    {
      return true;
    }
    else if ((ch >= U'\U00010800') && (ch <= U'\U00010805'))
    {
      return true;
    }
    else if ((ch >= U'\U00010808') && (ch <= U'\U00010808'))
    {
      return true;
    }
    else if ((ch >= U'\U0001080A') && (ch <= U'\U00010835'))
    {
      return true;
    }
    else if ((ch >= U'\U00010837') && (ch <= U'\U00010838'))
    {
      return true;
    }
    else if ((ch >= U'\U0001083C') && (ch <= U'\U0001083C'))
    {
      return true;
    }
    else if ((ch >= U'\U0001083F') && (ch <= U'\U00010855'))
    {
      return true;
    }
    else if ((ch >= U'\U00010860') && (ch <= U'\U00010876'))
    {
      return true;
    }
    else if ((ch >= U'\U00010880') && (ch <= U'\U0001089E'))
    {
      return true;
    }
    else if ((ch >= U'\U000108E0') && (ch <= U'\U000108F2'))
    {
      return true;
    }
    else if ((ch >= U'\U000108F4') && (ch <= U'\U000108F5'))
    {
      return true;
    }
    else if ((ch >= U'\U00010900') && (ch <= U'\U00010915'))
    {
      return true;
    }
    else if ((ch >= U'\U00010920') && (ch <= U'\U00010939'))
    {
      return true;
    }
    else if ((ch >= U'\U00010980') && (ch <= U'\U000109B7'))
    {
      return true;
    }
    else if ((ch >= U'\U000109BE') && (ch <= U'\U000109BF'))
    {
      return true;
    }
    else if ((ch >= U'\U00010A00') && (ch <= U'\U00010A03'))
    {
      return true;
    }
    else if ((ch >= U'\U00010A05') && (ch <= U'\U00010A06'))
    {
      return true;
    }
    else if ((ch >= U'\U00010A0C') && (ch <= U'\U00010A13'))
    {
      return true;
    }
    else if ((ch >= U'\U00010A15') && (ch <= U'\U00010A17'))
    {
      return true;
    }
    else if ((ch >= U'\U00010A19') && (ch <= U'\U00010A33'))
    {
      return true;
    }
    else if ((ch >= U'\U00010A38') && (ch <= U'\U00010A3A'))
    {
      return true;
    }
    else if ((ch >= U'\U00010A3F') && (ch <= U'\U00010A3F'))
    {
      return true;
    }
    else if ((ch >= U'\U00010A60') && (ch <= U'\U00010A7C'))
    {
      return true;
    }
    else if ((ch >= U'\U00010A80') && (ch <= U'\U00010A9C'))
    {
      return true;
    }
    else if ((ch >= U'\U00010AC0') && (ch <= U'\U00010AC7'))
    {
      return true;
    }
    else if ((ch >= U'\U00010AC9') && (ch <= U'\U00010AE6'))
    {
      return true;
    }
    else if ((ch >= U'\U00010B00') && (ch <= U'\U00010B35'))
    {
      return true;
    }
    else if ((ch >= U'\U00010B40') && (ch <= U'\U00010B55'))
    {
      return true;
    }
    else if ((ch >= U'\U00010B60') && (ch <= U'\U00010B72'))
    {
      return true;
    }
    else if ((ch >= U'\U00010B80') && (ch <= U'\U00010B91'))
    {
      return true;
    }
    else if ((ch >= U'\U00010C00') && (ch <= U'\U00010C48'))
    {
      return true;
    }
    else if ((ch >= U'\U00010C80') && (ch <= U'\U00010CB2'))
    {
      return true;
    }
    else if ((ch >= U'\U00010CC0') && (ch <= U'\U00010CF2'))
    {
      return true;
    }
    else if ((ch >= U'\U00011000') && (ch <= U'\U00011046'))
    {
      return true;
    }
    else if ((ch >= U'\U00011066') && (ch <= U'\U0001106F'))
    {
      return true;
    }
    else if ((ch >= U'\U0001107F') && (ch <= U'\U000110BA'))
    {
      return true;
    }
    else if ((ch >= U'\U000110D0') && (ch <= U'\U000110E8'))
    {
      return true;
    }
    else if ((ch >= U'\U000110F0') && (ch <= U'\U000110F9'))
    {
      return true;
    }
    else if ((ch >= U'\U00011100') && (ch <= U'\U00011134'))
    {
      return true;
    }
    else if ((ch >= U'\U00011136') && (ch <= U'\U0001113F'))
    {
      return true;
    }
    else if ((ch >= U'\U00011150') && (ch <= U'\U00011173'))
    {
      return true;
    }
    else if ((ch >= U'\U00011176') && (ch <= U'\U00011176'))
    {
      return true;
    }
    else if ((ch >= U'\U00011180') && (ch <= U'\U000111C4'))
    {
      return true;
    }
    else if ((ch >= U'\U000111CA') && (ch <= U'\U000111CC'))
    {
      return true;
    }
    else if ((ch >= U'\U000111D0') && (ch <= U'\U000111DA'))
    {
      return true;
    }
    else if ((ch >= U'\U000111DC') && (ch <= U'\U000111DC'))
    {
      return true;
    }
    else if ((ch >= U'\U00011200') && (ch <= U'\U00011211'))
    {
      return true;
    }
    else if ((ch >= U'\U00011213') && (ch <= U'\U00011237'))
    {
      return true;
    }
    else if ((ch >= U'\U0001123E') && (ch <= U'\U0001123E'))
    {
      return true;
    }
    else if ((ch >= U'\U00011280') && (ch <= U'\U00011286'))
    {
      return true;
    }
    else if ((ch >= U'\U00011288') && (ch <= U'\U00011288'))
    {
      return true;
    }
    else if ((ch >= U'\U0001128A') && (ch <= U'\U0001128D'))
    {
      return true;
    }
    else if ((ch >= U'\U0001128F') && (ch <= U'\U0001129D'))
    {
      return true;
    }
    else if ((ch >= U'\U0001129F') && (ch <= U'\U000112A8'))
    {
      return true;
    }
    else if ((ch >= U'\U000112B0') && (ch <= U'\U000112EA'))
    {
      return true;
    }
    else if ((ch >= U'\U000112F0') && (ch <= U'\U000112F9'))
    {
      return true;
    }
    else if ((ch >= U'\U00011300') && (ch <= U'\U00011303'))
    {
      return true;
    }
    else if ((ch >= U'\U00011305') && (ch <= U'\U0001130C'))
    {
      return true;
    }
    else if ((ch >= U'\U0001130F') && (ch <= U'\U00011310'))
    {
      return true;
    }
    else if ((ch >= U'\U00011313') && (ch <= U'\U00011328'))
    {
      return true;
    }
    else if ((ch >= U'\U0001132A') && (ch <= U'\U00011330'))
    {
      return true;
    }
    else if ((ch >= U'\U00011332') && (ch <= U'\U00011333'))
    {
      return true;
    }
    else if ((ch >= U'\U00011335') && (ch <= U'\U00011339'))
    {
      return true;
    }
    else if ((ch >= U'\U0001133C') && (ch <= U'\U00011344'))
    {
      return true;
    }
    else if ((ch >= U'\U00011347') && (ch <= U'\U00011348'))
    {
      return true;
    }
    else if ((ch >= U'\U0001134B') && (ch <= U'\U0001134D'))
    {
      return true;
    }
    else if ((ch >= U'\U00011350') && (ch <= U'\U00011350'))
    {
      return true;
    }
    else if ((ch >= U'\U00011357') && (ch <= U'\U00011357'))
    {
      return true;
    }
    else if ((ch >= U'\U0001135D') && (ch <= U'\U00011363'))
    {
      return true;
    }
    else if ((ch >= U'\U00011366') && (ch <= U'\U0001136C'))
    {
      return true;
    }
    else if ((ch >= U'\U00011370') && (ch <= U'\U00011374'))
    {
      return true;
    }
    else if ((ch >= U'\U00011400') && (ch <= U'\U0001144A'))
    {
      return true;
    }
    else if ((ch >= U'\U00011450') && (ch <= U'\U00011459'))
    {
      return true;
    }
    else if ((ch >= U'\U00011480') && (ch <= U'\U000114C5'))
    {
      return true;
    }
    else if ((ch >= U'\U000114C7') && (ch <= U'\U000114C7'))
    {
      return true;
    }
    else if ((ch >= U'\U000114D0') && (ch <= U'\U000114D9'))
    {
      return true;
    }
    else if ((ch >= U'\U00011580') && (ch <= U'\U000115B5'))
    {
      return true;
    }
    else if ((ch >= U'\U000115B8') && (ch <= U'\U000115C0'))
    {
      return true;
    }
    else if ((ch >= U'\U000115D8') && (ch <= U'\U000115DD'))
    {
      return true;
    }
    else if ((ch >= U'\U00011600') && (ch <= U'\U00011640'))
    {
      return true;
    }
    else if ((ch >= U'\U00011644') && (ch <= U'\U00011644'))
    {
      return true;
    }
    else if ((ch >= U'\U00011650') && (ch <= U'\U00011659'))
    {
      return true;
    }
    else if ((ch >= U'\U00011680') && (ch <= U'\U000116B7'))
    {
      return true;
    }
    else if ((ch >= U'\U000116C0') && (ch <= U'\U000116C9'))
    {
      return true;
    }
    else if ((ch >= U'\U00011700') && (ch <= U'\U00011719'))
    {
      return true;
    }
    else if ((ch >= U'\U0001171D') && (ch <= U'\U0001172B'))
    {
      return true;
    }
    else if ((ch >= U'\U00011730') && (ch <= U'\U00011739'))
    {
      return true;
    }
    else if ((ch >= U'\U000118A0') && (ch <= U'\U000118E9'))
    {
      return true;
    }
    else if ((ch >= U'\U000118FF') && (ch <= U'\U000118FF'))
    {
      return true;
    }
    else if ((ch >= U'\U00011AC0') && (ch <= U'\U00011AF8'))
    {
      return true;
    }
    else if ((ch >= U'\U00011C00') && (ch <= U'\U00011C08'))
    {
      return true;
    }
    else if ((ch >= U'\U00011C0A') && (ch <= U'\U00011C36'))
    {
      return true;
    }
    else if ((ch >= U'\U00011C38') && (ch <= U'\U00011C40'))
    {
      return true;
    }
    else if ((ch >= U'\U00011C50') && (ch <= U'\U00011C59'))
    {
      return true;
    }
    else if ((ch >= U'\U00011C72') && (ch <= U'\U00011C8F'))
    {
      return true;
    }
    else if ((ch >= U'\U00011C92') && (ch <= U'\U00011CA7'))
    {
      return true;
    }
    else if ((ch >= U'\U00011CA9') && (ch <= U'\U00011CB6'))
    {
      return true;
    }
    else if ((ch >= U'\U00012000') && (ch <= U'\U00012399'))
    {
      return true;
    }
    else if ((ch >= U'\U00012400') && (ch <= U'\U0001246E'))
    {
      return true;
    }
    else if ((ch >= U'\U00012480') && (ch <= U'\U00012543'))
    {
      return true;
    }
    else if ((ch >= U'\U00013000') && (ch <= U'\U0001342E'))
    {
      return true;
    }
    else if ((ch >= U'\U00014400') && (ch <= U'\U00014646'))
    {
      return true;
    }
    else if ((ch >= U'\U00016800') && (ch <= U'\U00016A38'))
    {
      return true;
    }
    else if ((ch >= U'\U00016A40') && (ch <= U'\U00016A5E'))
    {
      return true;
    }
    else if ((ch >= U'\U00016A60') && (ch <= U'\U00016A69'))
    {
      return true;
    }
    else if ((ch >= U'\U00016AD0') && (ch <= U'\U00016AED'))
    {
      return true;
    }
    else if ((ch >= U'\U00016AF0') && (ch <= U'\U00016AF4'))
    {
      return true;
    }
    else if ((ch >= U'\U00016B00') && (ch <= U'\U00016B36'))
    {
      return true;
    }
    else if ((ch >= U'\U00016B40') && (ch <= U'\U00016B43'))
    {
      return true;
    }
    else if ((ch >= U'\U00016B50') && (ch <= U'\U00016B59'))
    {
      return true;
    }
    else if ((ch >= U'\U00016B63') && (ch <= U'\U00016B77'))
    {
      return true;
    }
    else if ((ch >= U'\U00016B7D') && (ch <= U'\U00016B8F'))
    {
      return true;
    }
    else if ((ch >= U'\U00016F00') && (ch <= U'\U00016F44'))
    {
      return true;
    }
    else if ((ch >= U'\U00016F50') && (ch <= U'\U00016F7E'))
    {
      return true;
    }
    else if ((ch >= U'\U00016F8F') && (ch <= U'\U00016F9F'))
    {
      return true;
    }
    else if ((ch >= U'\U00016FE0') && (ch <= U'\U00016FE0'))
    {
      return true;
    }
    else if ((ch >= U'\U00017000') && (ch <= U'\U000187EC'))
    {
      return true;
    }
    else if ((ch >= U'\U00018800') && (ch <= U'\U00018AF2'))
    {
      return true;
    }
    else if ((ch >= U'\U0001B000') && (ch <= U'\U0001B001'))
    {
      return true;
    }
    else if ((ch >= U'\U0001BC00') && (ch <= U'\U0001BC6A'))
    {
      return true;
    }
    else if ((ch >= U'\U0001BC70') && (ch <= U'\U0001BC7C'))
    {
      return true;
    }
    else if ((ch >= U'\U0001BC80') && (ch <= U'\U0001BC88'))
    {
      return true;
    }
    else if ((ch >= U'\U0001BC90') && (ch <= U'\U0001BC99'))
    {
      return true;
    }
    else if ((ch >= U'\U0001BC9D') && (ch <= U'\U0001BC9E'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D165') && (ch <= U'\U0001D169'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D16D') && (ch <= U'\U0001D172'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D17B') && (ch <= U'\U0001D182'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D185') && (ch <= U'\U0001D18B'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D1AA') && (ch <= U'\U0001D1AD'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D242') && (ch <= U'\U0001D244'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D400') && (ch <= U'\U0001D454'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D456') && (ch <= U'\U0001D49C'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D49E') && (ch <= U'\U0001D49F'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D4A2') && (ch <= U'\U0001D4A2'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D4A5') && (ch <= U'\U0001D4A6'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D4A9') && (ch <= U'\U0001D4AC'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D4AE') && (ch <= U'\U0001D4B9'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D4BB') && (ch <= U'\U0001D4BB'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D4BD') && (ch <= U'\U0001D4C3'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D4C5') && (ch <= U'\U0001D505'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D507') && (ch <= U'\U0001D50A'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D50D') && (ch <= U'\U0001D514'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D516') && (ch <= U'\U0001D51C'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D51E') && (ch <= U'\U0001D539'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D53B') && (ch <= U'\U0001D53E'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D540') && (ch <= U'\U0001D544'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D546') && (ch <= U'\U0001D546'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D54A') && (ch <= U'\U0001D550'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D552') && (ch <= U'\U0001D6A5'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D6A8') && (ch <= U'\U0001D6C0'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D6C2') && (ch <= U'\U0001D6DA'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D6DC') && (ch <= U'\U0001D6FA'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D6FC') && (ch <= U'\U0001D714'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D716') && (ch <= U'\U0001D734'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D736') && (ch <= U'\U0001D74E'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D750') && (ch <= U'\U0001D76E'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D770') && (ch <= U'\U0001D788'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D78A') && (ch <= U'\U0001D7A8'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D7AA') && (ch <= U'\U0001D7C2'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D7C4') && (ch <= U'\U0001D7CB'))
    {
      return true;
    }
    else if ((ch >= U'\U0001D7CE') && (ch <= U'\U0001D7FF'))
    {
      return true;
    }
    else if ((ch >= U'\U0001DA00') && (ch <= U'\U0001DA36'))
    {
      return true;
    }
    else if ((ch >= U'\U0001DA3B') && (ch <= U'\U0001DA6C'))
    {
      return true;
    }
    else if ((ch >= U'\U0001DA75') && (ch <= U'\U0001DA75'))
    {
      return true;
    }
    else if ((ch >= U'\U0001DA84') && (ch <= U'\U0001DA84'))
    {
      return true;
    }
    else if ((ch >= U'\U0001DA9B') && (ch <= U'\U0001DA9F'))
    {
      return true;
    }
    else if ((ch >= U'\U0001DAA1') && (ch <= U'\U0001DAAF'))
    {
      return true;
    }
    else if ((ch >= U'\U0001E000') && (ch <= U'\U0001E006'))
    {
      return true;
    }
    else if ((ch >= U'\U0001E008') && (ch <= U'\U0001E018'))
    {
      return true;
    }
    else if ((ch >= U'\U0001E01B') && (ch <= U'\U0001E021'))
    {
      return true;
    }
    else if ((ch >= U'\U0001E023') && (ch <= U'\U0001E024'))
    {
      return true;
    }
    else if ((ch >= U'\U0001E026') && (ch <= U'\U0001E02A'))
    {
      return true;
    }
    else if ((ch >= U'\U0001E800') && (ch <= U'\U0001E8C4'))
    {
      return true;
    }
    else if ((ch >= U'\U0001E8D0') && (ch <= U'\U0001E8D6'))
    {
      return true;
    }
    else if ((ch >= U'\U0001E900') && (ch <= U'\U0001E94A'))
    {
      return true;
    }
    else if ((ch >= U'\U0001E950') && (ch <= U'\U0001E959'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE00') && (ch <= U'\U0001EE03'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE05') && (ch <= U'\U0001EE1F'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE21') && (ch <= U'\U0001EE22'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE24') && (ch <= U'\U0001EE24'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE27') && (ch <= U'\U0001EE27'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE29') && (ch <= U'\U0001EE32'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE34') && (ch <= U'\U0001EE37'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE39') && (ch <= U'\U0001EE39'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE3B') && (ch <= U'\U0001EE3B'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE42') && (ch <= U'\U0001EE42'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE47') && (ch <= U'\U0001EE47'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE49') && (ch <= U'\U0001EE49'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE4B') && (ch <= U'\U0001EE4B'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE4D') && (ch <= U'\U0001EE4F'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE51') && (ch <= U'\U0001EE52'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE54') && (ch <= U'\U0001EE54'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE57') && (ch <= U'\U0001EE57'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE59') && (ch <= U'\U0001EE59'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE5B') && (ch <= U'\U0001EE5B'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE5D') && (ch <= U'\U0001EE5D'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE5F') && (ch <= U'\U0001EE5F'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE61') && (ch <= U'\U0001EE62'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE64') && (ch <= U'\U0001EE64'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE67') && (ch <= U'\U0001EE6A'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE6C') && (ch <= U'\U0001EE72'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE74') && (ch <= U'\U0001EE77'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE79') && (ch <= U'\U0001EE7C'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE7E') && (ch <= U'\U0001EE7E'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE80') && (ch <= U'\U0001EE89'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EE8B') && (ch <= U'\U0001EE9B'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EEA1') && (ch <= U'\U0001EEA3'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EEA5') && (ch <= U'\U0001EEA9'))
    {
      return true;
    }
    else if ((ch >= U'\U0001EEAB') && (ch <= U'\U0001EEBB'))
    {
      return true;
    }
    else if ((ch >= U'\U00020000') && (ch <= U'\U0002A6D6'))
    {
      return true;
    }
    else if ((ch >= U'\U0002A700') && (ch <= U'\U0002B734'))
    {
      return true;
    }
    else if ((ch >= U'\U0002B740') && (ch <= U'\U0002B81D'))
    {
      return true;
    }
    else if ((ch >= U'\U0002B820') && (ch <= U'\U0002CEA1'))
    {
      return true;
    }
    else if ((ch >= U'\U0002F800') && (ch <= U'\U0002FA1D'))
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * 
   */
  bool isSpaceSeparator(char32_t ch)
  {
    switch (ch)
    {
    case u'\u0020':
    case u'\u00A0':
    case u'\u1780':
    case u'\u202F':
    case u'\u205F':
    case u'\u3000':
      return true;
    default:
      return ((ch >= u'\u2000') && (ch <= u'\u200A'));
    }
  }

  /**
   * 
   */
  number_type parseNumber(const CUString &str)
  {
    return static_cast<number_type>(std::strtold(str.c_str(), nullptr));
  }
}
#pragma once

#include "unicode/CUString.hpp"

#include "values/CJSONNumber.hpp"

namespace json5::util
{
  /**
   * @param ch The character to check
   * @return True if @ch is a decimal digit
   */
  bool isDecDigit(char32_t ch);

  /**
   * @param ch The character to check
   * @return True if @ch is a hexadecimal digit
   */
  bool isHexDigit(char32_t ch);

  /**
   * @param ch The character to check
   * @return True if @ch can be used to start an identifier name
   */
  bool isIdStartChar(char32_t ch);

  /**
   * @param ch The character to check
   * @return True if @ch can be used to continue an identifier name
   */
  bool isIdContinueChar(char32_t ch);

  /**
   * @param ch The character to check
   * @return True if @ch is a space separator
   */
  bool isSpaceSeparator(char32_t ch);

  /**
   * Parses a string to extract the number.
   * Assumes the number has been processed by the lexer.
   * @param str The string
   * @return The number
   */
  number_type parseNumber(const CUString &str);
}
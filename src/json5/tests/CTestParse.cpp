#include "CTestParse.hpp"

#include "json5/exceptions/CInvalidCharacter.hpp"

#include "json5/json.hpp"

#include <cmath>


namespace json5::test::parse
{
  /**
   * How far off numbers are allowed to be
   */
  constexpr number_type k_dbRoundingTolerance{ 9.0E-17 };

  /**
   * 
   */
  bool emptyObject(void)
  {
    try
    {
      object obj{ JSON.parse("{}") };

      return obj->value.empty();
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool doubleQuotePropertyName(void)
  {
    try
    {
      object obj{ JSON.parse("{ \"a\": 1 }") };

      if (obj->value.size() != 1)
      {
        return false;
      }

      if (number n{ obj->getNumber("a") })
      {
        if (n->value != 1.0)
        {
          return false;
        }
      }
      else
      {
        return false;
      }

      return true;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool singleQuotePropertyName(void)
  {
    try
    {
      object obj{ JSON.parse("{ \'a\': 1 }") };

      if (obj->value.size() != 1)
      {
        return false;
      }

      if (number n{ obj->getNumber("a") })
      {
        if (n->value != 1.0)
        {
          return false;
        }
      }
      else
      {
        return false;
      }

      return true;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool unquotedPropertyName(void)
  {
    try
    {
      object obj{ JSON.parse("{ a: 1 }") };

      if (obj->value.size() != 1)
      {
        return false;
      }

      if (number n{ obj->getNumber("a") })
      {
        if (n->value != 1.0)
        {
          return false;
        }
      }
      else
      {
        return false;
      }

      return true;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool specialCharacterPropertyName(void)
  {
    try
    {
      object obj{ JSON.parse(u8"{ $_: 1, _$: 2, a\u200C: 3 }") };

      if (obj->value.size() != 3)
      {
        return false;
      }

      if (number n{ obj->getNumber("$_") })
      {
        if (n->value != 1.0)
        {
          return false;
        }
      }
      else
      {
        return false;
      }

      if (number n{ obj->getNumber("_$") })
      {
        if (n->value != 2.0)
        {
          return false;
        }
      }
      else
      {
        return false;
      }

      if (number n{ obj->getNumber(u8"a\u200C") })
      {
        if (n->value != 3.0)
        {
          return false;
        }
      }
      else
      {
        return false;
      }

      return true;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool unicodePropertyName(void)
  {
    try
    {
      object obj{ JSON.parse(u8"{ ùńîċõďë: 1 }") };

      if (obj->value.size() != 1)
      {
        return false;
      }

      if (number n{ obj->getNumber(u8"ùńîċõďë") })
      {
        if (n->value != 1.0)
        {
          return false;
        }
      }
      else
      {
        return false;
      }

      return true;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool escapedPropertyName(void)
  {
    //try
    {
      object obj{ JSON.parse(u8"{ \\u0061\\u0062: 1, \\u0024\\u005F: 2, \\u005F\\u0024: 3 }") };

      if (obj->value.size() != 3)
      {
        return false;
      }

      if (number n{ obj->getNumber("ab") })
      {
        if (n->value != 1.0)
        {
          return false;
        }
      }
      else
      {
        return false;
      }

      if (number n{ obj->getNumber("$_") })
      {
        if (n->value != 2.0)
        {
          return false;
        }
      }
      else
      {
        return false;
      }

      if (number n{ obj->getNumber("_$") })
      {
        if (n->value != 3.0)
        {
          return false;
        }
      }
      else
      {
        return false;
      }

      return true;
    }
    //catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool multipleProperties(void)
  {
    try
    {
      object obj{ JSON.parse("{ abc: 1, def: 2 }") };

      if (obj->value.size() != 2)
      {
        return false;
      }

      if (number abc{ obj->getNumber("abc") })
      {
        if (abc->value != 1.0)
        {
          return false;
        }
      }
      else
      {
        return false;
      }

      if (number def{ obj->getNumber("def") })
      {
        if (def->value != 2.0)
        {
          return false;
        }
      }
      else
      {
        return false;
      }

      return true;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool nestedObjects(void)
  {
    try
    {
      object obj{ JSON.parse("{ a: { b: 2 } }") };

      if (obj->value.size() != 1)
      {
        return false;
      }

      if (object a{ obj->getObject("a") })
      {
        if (a->value.size() != 1)
        {
          return false;
        }

        if (number b{ a->getNumber("b") })
        {
          if (b->value != 2.0)
          {
            return false;
          }
        }
        else
        {
          return false;
        }
      }
      else
      {
        return false;
      }

      return true;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool emptyArray(void)
  {
    try
    {
      array arr{ JSON.parse("[]") };

      if (arr->value.size() != 0)
      {
        return false;
      }

      return true;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool arrayValue(void)
  {
    try
    {
      array arr{ JSON.parse("[ 1 ]") };

      if (arr->value.size() != 1)
      {
        return false;
      }

      if (number n{ arr->getNumber(0) })
      {
        if (n->value != 1.0)
        {
          return false;
        }
      }
      else
      {
        return false;
      }

      return true;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool arrayMultipleValues(void)
  {
    try
    {
      array arr{ JSON.parse("[ 1, 2 ]") };

      if (arr->value.size() != 2)
      {
        return false;
      }

      if (number n{ arr->getNumber(0) })
      {
        if (n->value != 1.0)
        {
          return false;
        }
      }
      else
      {
        return false;
      }

      if (number n{ arr->getNumber(1) })
      {
        if (n->value != 2.0)
        {
          return false;
        }
      }
      else
      {
        return false;
      }

      return true;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool nestedArray(void)
  {
    try
    {
      array arr{ JSON.parse("[ 1, [ 2, 3 ] ]") };

      if (arr->value.size() != 2)
      {
        return false;
      }

      if (number n{ arr->getNumber(0) })
      {
        if (n->value != 1.0)
        {
          return false;
        }
      }
      else
      {
        return false;
      }

      if (array a{ arr->getArray(1) })
      {
        if (a->length() == 2)
        {
          if (number n{ a->getNumber(0) })
          {
            if (n->value != 2.0)
            {
              return false;
            }
          }
          else
          {
            return false;
          }
          if (number n{ a->getNumber(1) })
          {
            if (n->value != 3.0)
            {
              return false;
            }
          }
          else
          {
            return false;
          }
        }
        else
        {
          return false;
        }
      }
      else
      {
        return false;
      }

      return true;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool parseNull(void)
  {
    try
    {
      let n{ JSON.parse("null") };

      return n->isNull();
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool parseTrue(void)
  {
    try
    {
      boolean b{ JSON.parse("true") };

      return b->value;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool parseFalse(void)
  {
    try
    {
      boolean b{ JSON.parse("false") };

      return !b->value;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool leadingZero(void)
  {
    try
    {
      array arr{ JSON.parse("[ 0, 0., 0e0 ]") };

      if (arr->length() != 3)
      {
        return false;
      }

      for (int i{ 0 }; i < 3; ++i)
      {
        if (number n{ arr->getNumber(i) })
        {
          if (n->value != 0.0)
          {
            return false;
          }
        }
        else
        {
          return false;
        }
      }

      return true;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool integers(void)
  {
    try
    {
      std::array<number_type, 4> arrCompareEquals{ 1.0, 23.0, 456.0, 7890.0 };

      array arr{ JSON.parse("[ 1, 23, 456, 7890 ]") };

      if (arr->length() != arrCompareEquals.size())
      {
        return false;
      }

      for (int i{ 0 }; i < static_cast<int>(arrCompareEquals.size()); ++i)
      {
        if (number n{ arr->getNumber(i) })
        {
          if (n->value != arrCompareEquals.at(i))
          {
            return false;
          }
        }
        else
        {
          return false;
        }
      }

      return true;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool signedNumbers(void)
  {
    try
    {
      std::array<number_type, 4> arrCompareEquals{ -1.0, 2.0, -0.1, 0.0 };

      array arr{ JSON.parse("[ -1, +2, -0.1, -0 ]") };

      if (arr->length() != arrCompareEquals.size())
      {
        return false;
      }

      for (int i{ 0 }; i < static_cast<int>(arrCompareEquals.size()); ++i)
      {
        if (number n{ arr->getNumber(i) })
        {
          if (std::abs(n->value - arrCompareEquals.at(i)) > k_dbRoundingTolerance)
          {
            return false;
          }
        }
        else
        {
          return false;
        }
      }

      return true;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool leadingDecimalPoints(void)
  {
    try
    {
      std::array<number_type, 2> arrCompareEquals{ 0.1, 0.23 };

      array arr{ JSON.parse("[ .1, .23 ]") };

      if (arr->length() != arrCompareEquals.size())
      {
        return false;
      }

      for (int i{ 0 }; i < static_cast<int>(arrCompareEquals.size()); ++i)
      {
        if (number n{ arr->getNumber(i) })
        {
          if (std::abs(n->value - arrCompareEquals.at(i)) > k_dbRoundingTolerance)
          {
            return false;
          }
        }
        else
        {
          return false;
        }
      }

      return true;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool fractionalNumbers(void)
  {
    try
    {
      std::array<number_type, 2> arrCompareEquals{ 1.0, 1.23 };

      array arr{ JSON.parse("[ 1.0, 1.23 ]") };

      if (arr->length() != arrCompareEquals.size())
      {
        return false;
      }

      for (int i{ 0 }; i < static_cast<int>(arrCompareEquals.size()); ++i)
      {
        if (number n{ arr->getNumber(i) })
        {
          if (std::abs(n->value - arrCompareEquals.at(i)) > k_dbRoundingTolerance)
          {
            return false;
          }
        }
        else
        {
          return false;
        }
      }

      return true;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool exponents(void)
  {
    try
    {
      std::array<number_type, 7> arrCompareEquals{ 1e0, 1e1, 1e01, 1.e0, 1.1e0, 1e-1, 1e+1 };

      array arr{ JSON.parse("[ 1e0, 1e1, 1e01, 1.e0, 1.1e0, 1e-1, 1e+1 ]") };

      if (arr->length() != arrCompareEquals.size())
      {
        return false;
      }

      for (int i{ 0 }; i < static_cast<int>(arrCompareEquals.size()); ++i)
      {
        if (number n{ arr->getNumber(i) })
        {
          if (std::abs(n->value - arrCompareEquals.at(i)) > k_dbRoundingTolerance)
          {
            return false;
          }
        }
        else
        {
          return false;
        }
      }

      return true;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool hexadecimalNumber(void)
  {
    try
    {
      std::array<number_type, 4> arrCompareEquals{ 0x1, 0x10, 0xFF, 0xFF };

      array arr{ JSON.parse("[ 0x1, 0x10, 0xff, 0xFF ]") };

      if (arr->length() != arrCompareEquals.size())
      {
        return false;
      }

      for (int i{ 0 }; i < static_cast<int>(arrCompareEquals.size()); ++i)
      {
        if (number n{ arr->getNumber(i) })
        {
          if (n->value != arrCompareEquals.at(i))
          {
            return false;
          }
        }
        else
        {
          return false;
        }
      }

      return true;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool parseInfinity(void)
  {
    try
    {
      std::array<number_type, 2> arrCompareEquals{ std::numeric_limits<number_type>::infinity(), -std::numeric_limits<number_type>::infinity() };

      array arr{ JSON.parse("[ Infinity, -Infinity ]") };

      if (arr->length() != arrCompareEquals.size())
      {
        return false;
      }

      for (int i{ 0 }; i < static_cast<int>(arrCompareEquals.size()); ++i)
      {
        if (number n{ arr->getNumber(i) })
        {
          if (n->value != arrCompareEquals.at(i))
          {
            return false;
          }
        }
        else
        {
          return false;
        }
      }

      return true;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool parseNaN(void)
  {
    try
    {
      array arr{ JSON.parse("[ NaN, -NaN ]") };

      if (arr->length() != 2)
      {
        return false;
      }

      for (int i{ 0 }; i < 2; ++i)
      {
        if (number n{ arr->getNumber(i) })
        {
          if (!std::isnan(n->value))
          {
            return false;
          }
        }
        else
        {
          return false;
        }
      }

      return true;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool doubleQuotedString(void)
  {
    try
    {
      string str{ JSON.parse("\"abc\"") };

      return (str->value == "abc");

      return true;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool singleQuotedString(void)
  {
    try
    {
      string str{ JSON.parse("\'abc\'") };

      return (str->value == "abc");
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool nestedQuotedString(void)
  {
    try
    {
      array arr{ JSON.parse("[ \'\"\', \"\'\" ]") };

      if (string s{ arr->get(0) })
      {
        if (s->value != "\"")
        {
          return false;
        }
      }
      else
      {
        return false;
      }

      if (string s{ arr->get(1) })
      {
        if (s->value != "\'")
        {
          return false;
        }
      }
      else
      {
        return false;
      }

      return true;
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool escapedCharacters(void)
  {
    try
    {
      CUString s{ "\b\f\n\r\t" };

      s.append('\v');

      string str{ JSON.parse("\"\\b\\f\\n\\r\\t\\v\\0\\x0f\\u01fF\\\n\\\r\n\\\r\\\u2028\\\u2029\\a\\\'\\\"\"") };

      return (str->value == CUString{ u8"\b\f\n\r\t\v\0\x0F\u01FF\a\'\"", 14 });
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool singleLineComment(void)
  {
    try
    {
      object obj{ JSON.parse("{// comment\n }") };

      return obj->value.empty();
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool singleLineCommentAtEnd(void)
  {
    try
    {
      object obj{ JSON.parse("{ }// comment") };

      return obj->value.empty();
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool multiLineComment(void)
  {
    try
    {
      object obj{ JSON.parse("{/*comment\n** */}") };

      return obj->value.empty();
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool whiteSpace(void)
  {
    try
    {
      object obj{ JSON.parse(u8"{\t\v\f \u00A0\uFEFF\n\r\u2028\u2029\u2003}") };

      return obj->value.empty();
    }
    catch (...)
    {
      return false;
    }
  }

  /**
   * 
   */
  bool CTestParse::run(int *pSuccess, int *pFail)
  {
    this->addTest({ "parses empty objects", emptyObject });
    this->addTest({ "parses double string property names", doubleQuotePropertyName });
    this->addTest({ "parses single string property names", singleQuotePropertyName });
    this->addTest({ "parses unquoted property names", unquotedPropertyName });
    this->addTest({ "parses special character property names", specialCharacterPropertyName });
    this->addTest({ "parses unicode property names", unicodePropertyName });
    this->addTest({ "parses escaped property names", escapedPropertyName });
    this->addTest({ "parses multiple properties", multipleProperties });
    this->addTest({ "parses nested objects", nestedObjects });
    this->addTest({ "parses empty arrays", emptyArray });
    this->addTest({ "parses array values", arrayValue });
    this->addTest({ "parses multiple array values", arrayMultipleValues });
    this->addTest({ "parses nested arrays", nestedArray });
    this->addTest({ "parses nulls", parseNull });
    this->addTest({ "parses true", parseTrue });
    this->addTest({ "parses false", parseFalse });
    this->addTest({ "parses leading zeroes", leadingZero });
    this->addTest({ "parses integers", integers });
    this->addTest({ "parses signed numbers", signedNumbers });
    this->addTest({ "parses leading decimal points", leadingDecimalPoints });
    this->addTest({ "parses fractional numbers", fractionalNumbers });
    this->addTest({ "parses exponents", exponents });
    this->addTest({ "parses hexadecimal numbers", hexadecimalNumber });
    this->addTest({ "parses signed and unsiged Infinity", parseInfinity });
    this->addTest({ "parses signed and unsigned NaN", parseNaN });
    this->addTest({ "parses double quoted strings", doubleQuotedString });
    this->addTest({ "parses single quoted strings", singleQuotedString });
    this->addTest({ "parses nested quoted strings", nestedQuotedString });
    this->addTest({ "parses escaped characters", escapedCharacters });
    // parses line and paragraph separators with a warning
    this->addTest({ "parses single-line comments", singleLineComment });
    this->addTest({ "parses single-line comments at end of input", singleLineCommentAtEnd });
    this->addTest({ "parses multi-line comments", multiLineComment });
    this->addTest({ "parses whitespace", whiteSpace });
    // TODO: Reviver

    return CTestHost::run(pSuccess, pFail);
  }

  /**
   * 
   */
  CTestParse::~CTestParse(void)
  {
  }
}
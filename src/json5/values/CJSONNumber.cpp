#include "CJSONNumber.hpp"

#include <cmath>
#include <sstream>

namespace json5
{
  /**
   * 
   */
  std::string CJSONNumber::toString(const CFormatOptions &fmt) const
  {
    if (std::isinf(this->value))
    {
      if (this->value > 0.0l)
      {
        return "Infinity";
      }
      else
      {
        return "-Infinity";
      }
    }
    else if (std::isnan(this->value))
    {
      return "NaN";
    }

    std::stringstream ss{};

    bool bHexCaps{ fmt.nsNumbers == CFormatOptions::NumberStyle::kHexadecimalCaps };

    if (bHexCaps || (fmt.nsNumbers == CFormatOptions::NumberStyle::kHexadecimal))
    {
      std::int64_t iFloor{ static_cast<std::int64_t>(this->value) };
      if (bool bHasMantissa{ this->value != static_cast<number_type>(iFloor) })
      {
        ss << this->value;
      }
      else
      {
        ss << "0x" << std::hex << iFloor;
      }

      std::string strResult{ ss.str() };

      if (bHexCaps)
      {
        for (int i{ 2 }; i < static_cast<int>(strResult.length()); ++i)
        {
          strResult[i] = std::toupper(strResult[i]);
        }
      }

      return strResult;
    }
    else
    {
      ss << this->value;

      if (fmt.nsNumbers == CFormatOptions::NumberStyle::kFloating)
      {
        std::int64_t iFloor{ static_cast<std::int64_t>(this->value) };
        if (this->value == static_cast<number_type>(iFloor))
        {
          ss << ".0";
        } 
      }

      return ss.str();
    }
  }

  /**
   * 
   */
  CJSONNumber::CJSONNumber(void)
      : value{ 0.0 }
  {
  }

  /**
   * 
   */
  CJSONNumber::CJSONNumber(number_type dbValue)
      : value{ dbValue }
  {
  }

  /**
   * 
   */
  CJSONNumber::~CJSONNumber(void)
  {
  }
}
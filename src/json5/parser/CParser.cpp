#include "CParser.hpp"

#include "CLexer.hpp"

#include "json5/values/CJSONArray.hpp"
#include "json5/values/CJSONBoolean.hpp"
#include "json5/values/CJSONContainer.hpp"
#include "json5/values/CJSONNull.hpp"
#include "json5/values/CJSONNumber.hpp"
#include "json5/values/CJSONObject.hpp"
#include "json5/values/CJSONString.hpp"

#include "json5/values/CJSONValue.hpp"

#include "json5/exceptions/CInvalidCharacter.hpp"

namespace json5
{
  /**
   * 
   */
  void CParser::push(void)
  {
    std::shared_ptr<CJSONValue> pValue{ nullptr };

    switch (this->m_token.getType())
    {
    case TokenType::kPunctuator:
      if (std::dynamic_pointer_cast<CJSONString>(this->m_token.getValue())->value == "{")
      {
        pValue.reset(new CJSONObject{});
      }
      else if (std::dynamic_pointer_cast<CJSONString>(this->m_token.getValue())->value == "[")
      {
        pValue.reset(new CJSONArray{});
      }
      break;
    case TokenType::kNull:
    case TokenType::kBoolean:
    case TokenType::kNumeric:
    case TokenType::kString:
      pValue = this->m_token.getValue();
      break;
    }

    if (this->m_pRoot == nullptr)
    {
      this->m_pRoot = pValue;
    }
    else
    {
      std::shared_ptr<CJSONValue> pParent{ this->m_stack.top() };

      if (pParent->isArray())
      {
        std::dynamic_pointer_cast<CJSONArray>(pParent)->push(pValue);
      }
      else
      {
        std::dynamic_pointer_cast<CJSONObject>(pParent)->set(this->m_strKey, pValue);
      }
    }

    if ((pValue != nullptr) && (pValue->isObject() || pValue->isArray()))
    {
      this->m_stack.push(pValue);

      if (pValue->isArray())
      {
        this->m_state = State::kBeforeArrayValue;
      }
      else
      {
        this->m_state = State::kBeforePropertyName;
      }
    }
    else
    {
      if (this->m_stack.empty())
      {
        this->m_state = State::kEnd;
      }
      else
      {
        std::shared_ptr<CJSONValue> pCurrent{ this->m_stack.top() };

        if (pCurrent->isArray())
        {
          this->m_state = State::kAfterArrayValue;
        }
        else
        {
          this->m_state = State::kAfterPropertyValue;
        }
      }
    }
  }

  /**
   * 
   */
  void CParser::pop(void)
  {
    this->m_stack.pop();

    if (this->m_stack.empty())
    {
      this->m_state = State::kEnd;
    }
    else
    {
      std::shared_ptr<CJSONValue> pCurrent{ this->m_stack.top() };

      if (pCurrent->isArray())
      {
        this->m_state = State::kAfterArrayValue;
      }
      else
      {
        this->m_state = State::kAfterPropertyValue;
      }
    }
  }

  /**
   * 
   */
  void CParser::parseStart(void)
  {
    if (this->m_token.getType() == TokenType::kEOF)
    {
      throw CInvalidCharacter{ 0, this->m_ssInput.getLine(), this->m_ssInput.getColumn() };
    }
    else
    {
      this->push();
    }
  }

  /**
   * 
   */
  void CParser::parseBeforePropertyName(void)
  {
    switch (this->m_token.getType())
    {
    case TokenType::kIdentifier:
    case TokenType::kString:
      this->m_strKey = std::dynamic_pointer_cast<CJSONString>(this->m_token.getValue())->value;
      this->m_state = State::kAfterPropertyName;
      break;
    case TokenType::kPunctuator:
      this->pop();
      break;
    case TokenType::kEOF:
      throw CInvalidCharacter{ 0, this->m_ssInput.getLine(), this->m_ssInput.getColumn() };
      break;
    }
  }

  /**
   * 
   */
  void CParser::parseAfterPropertyName(void)
  {
    if (this->m_token.getType() == TokenType::kEOF)
    {
      throw CInvalidCharacter{ 0, this->m_ssInput.getLine(), this->m_ssInput.getColumn() };
    }
    else
    {
      this->m_state = State::kBeforePropertyValue;
    }
  }

  /**
   * 
   */
  void CParser::parseBeforePropertyValue(void)
  {
    if (this->m_token.getType() == TokenType::kEOF)
    {
      throw CInvalidCharacter{ 0, this->m_ssInput.getLine(), this->m_ssInput.getColumn() };
    }
    else
    {
      this->push();
    }
  }

  /**
   * 
   */
  void CParser::parseAfterPropertyValue(void)
  {
    if (this->m_token.getType() == TokenType::kEOF)
    {
      throw CInvalidCharacter{ 0, this->m_ssInput.getLine(), this->m_ssInput.getColumn() };
    }
    else
    {
      std::shared_ptr<CJSONString> pValue{ std::dynamic_pointer_cast<CJSONString>(this->m_token.getValue()) };

      if (pValue->value == ",")
      {
        this->m_state = State::kBeforePropertyName;
      }
      else if (pValue->value == "}")
      {
        this->pop();
      }
    }
  }

  /**
   * 
   */
  void CParser::parseBeforeArrayValue(void)
  {
    if (this->m_token.getType() == TokenType::kEOF)
    {
      throw CInvalidCharacter{ 0, this->m_ssInput.getLine(), this->m_ssInput.getColumn() };
    }
    else if ((this->m_token.getType() == TokenType::kPunctuator) && (std::dynamic_pointer_cast<CJSONString>(this->m_token.getValue())->value == "]"))
    {
      this->pop();
    }
    else
    {
      this->push();
    }
  }

  /**
   * 
   */
  void CParser::parseAfterArrayValue(void)
  {
    if (this->m_token.getType() == TokenType::kEOF)
    {
      throw CInvalidCharacter{ 0, this->m_ssInput.getLine(), this->m_ssInput.getColumn() };
    }
    else
    {
      std::shared_ptr<CJSONString> pValue{ std::dynamic_pointer_cast<CJSONString>(this->m_token.getValue()) };

      if (pValue->value == ",")
      {
        this->m_state = State::kBeforeArrayValue;
      }
      else if (pValue->value == "]")
      {
        this->pop();
      }
    }
  }

  /**
   * 
   */
  void CParser::parse(void)
  {
    // std::printf("Parser start %i\n", static_cast<int>(this->m_state));

    switch (this->m_state)
    {
    case State::kStart:
      this->parseStart();
      break;
    case State::kBeforePropertyName:
      this->parseBeforePropertyName();
      break;
    case State::kAfterPropertyName:
      this->parseAfterPropertyName();
      break;
    case State::kBeforePropertyValue:
      this->parseBeforePropertyValue();
      break;
    case State::kAfterPropertyValue:
      this->parseAfterPropertyValue();
      break;
    case State::kBeforeArrayValue:
      this->parseBeforeArrayValue();
      break;
    case State::kAfterArrayValue:
      this->parseAfterArrayValue();
      break;
    }
  }

  /**
   * 
   */
  std::shared_ptr<CJSONValue> CParser::parse(const std::string &strSource)
  {
    this->m_ssInput.reset(strSource);
    this->m_state = State::kStart;
    this->m_token = CToken::invalid();
    this->m_strKey.clear();

    CLexer lexer{ &this->m_ssInput };

    do
    {
      this->m_token = lexer.lex(this->m_state);

      this->parse();

    } while (this->m_token.getType() != TokenType::kEOF);

    return this->m_pRoot;
  }
}
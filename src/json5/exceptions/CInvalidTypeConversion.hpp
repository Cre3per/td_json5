#pragma once

#include "CTextException.hpp"

#include "json5/values/CJSONValue.hpp"

// #include "json5/values/CJSONArray.hpp"
// #include "json5/values/CJSONBoolean.hpp"
// #include "json5/values/CJSONNumber.hpp"
// #include "json5/values/CJSONString.hpp"
// #include "json5/values/CJSONObject.hpp"

namespace json5
{
  // class CJSONArray;
  // class CJSONBoolean;
  // class CJSONNumber;
  // class CJSONObject;
  // class CJSONString;

  class CInvalidTypeConversion : public CTextException
  {
  public:
    /**
     * Constructor
     * @param from From type
     * @param to To type
     */
    CInvalidTypeConversion(JSONValueType from, JSONValueType to);

  public:

    /**
     * @param to To type
     */
    template <class TTo>
    static CInvalidTypeConversion create(JSONValueType from);
  };
}
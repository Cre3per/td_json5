#include "CTextException.hpp"

namespace json5
{

  /**
   * 
   */
  const char *CTextException::what(void) const throw()
  {
    return this->m_strMessage.c_str();
  }


  /**
   * 
   */
  CTextException::CTextException(void)
  {

  }

  /**
   * 
   */
  CTextException::CTextException(const std::string &strMessage)
      : m_strMessage{ strMessage }
  {

  }

  /**
   * 
   */
  CTextException::~CTextException(void)
  {

  }
}
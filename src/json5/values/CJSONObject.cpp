#include "CJSONObject.hpp"

#include "json5/util.hpp"

namespace json5
{

  /**
   * 
   */
  char CJSONObject::getKeyQuote(const CFormatOptions &fmt, const CUString &strKey) const
  {
    if (fmt.chKeyQuotationMark == '\x00')
    {
      bool bNeedsQuotation{ strKey.empty() || fmt.bASCII };

      if (!bNeedsQuotation)
      {
        for (int i{ 0 }; i < strKey.length(); ++i)
        {
          char32_t ch{ strKey.at(i) };

          if (!util::isIdContinueChar(ch))
          {
            bNeedsQuotation = true;
            break;
          }
        }
      }

      if (bNeedsQuotation)
      {
        return '\'';
      }
      else
      {
        return '\x00';
      }
    }
    else
    {
      return fmt.chKeyQuotationMark;
    }
  }

  /**
   * 
   */
  std::string CJSONObject::toString(const CFormatOptions &fmt) const
  {
    std::string strResult{ "" };

    bool bEmpty{ this->value.empty() };

    strResult.append("{");

    if (fmt.bObjectPairsOnNewLine)
    {
      if (!bEmpty)
      {
        strResult.append("\n" + fmt.strIndent);
      }
    }
    else if (fmt.bObjectSpaceBeforeFirstPair)
    {
      strResult.append(" ");
    }

    int i{ static_cast<int>(this->value.size()) };
    for (const auto &pair : this->value)
    {
      CUString strKey{ pair.first };
      strKey.escape(fmt.bASCII);

      char chKeyQuotationMark{ this->getKeyQuote(fmt, strKey) };

      if (chKeyQuotationMark != '\x00')
      {
        strResult += chKeyQuotationMark;
        strResult.append(strKey.str());
        strResult += chKeyQuotationMark;
      }
      else
      {
        strResult.append(strKey.str());
      }

      strResult.append(":");

      if (fmt.bObjectSpaceAfterColon)
      {
        strResult.append(" ");
      }

      std::string strChild{ pair.second->toString(fmt) };

      if (fmt.bObjectPairsOnNewLine && (pair.second->isArray() || pair.second->isObject()))
      {
        for (size_t s{ 1 }; (s + 1) < strChild.length(); ++s)
        {
          if ((strChild.at(s) == '\n') && (strChild.at(s - 1) != '\\'))
          {
            strChild.insert(s + 1, fmt.strIndent);
            s += fmt.strIndent.length();
          }
        }
      }

      strResult.append(strChild);

      if (--i != 0)
      {
        strResult.append(",");

        if (fmt.bObjectPairsOnNewLine)
        {
          strResult.append("\n" + fmt.strIndent);
        }
        else if (fmt.bObjectSpaceAfterComma)
        {
          strResult.append(" ");
        }
      }
    }

    if (fmt.bObjectPairsOnNewLine && !bEmpty)
    {
      strResult.append("\n");
    }
    else if (fmt.bObjectSpaceAfterLastPair)
    {
      strResult.append(" ");
    }

    strResult.append("}");

    return strResult;
  }

  /**
   * 
   */
  CJSONObject::~CJSONObject(void)
  {
  }
}
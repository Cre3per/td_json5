cmake_minimum_required(VERSION 3.0.0)
project(td_json5)

set(CMAKE_BUILD_TYPE Debug)

set(CMAKE_CXX_COMPILER "clang++")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++1z -Wall -Wextra -Wno-switch -Wno-unused-parameter -pedantic-errors -ferror-limit=4")

include_directories("src")

set(SOURCE
  ${SOURCE}

  "src/json5/json.cpp"

  "src/json5/exceptions/CInvalidCharacter.cpp"
  "src/json5/exceptions/CInvalidEncoding.cpp"
  "src/json5/exceptions/CInvalidIdentifier.cpp"
  "src/json5/exceptions/CInvalidTypeConversion.cpp"
  "src/json5/exceptions/CTextException.cpp"
  
  "src/json5/parser/CLexer.cpp"
  "src/json5/parser/CParser.cpp"
  "src/json5/parser/CStringStream.cpp"
  "src/json5/parser/CToken.cpp"
  
  "src/json5/tests/CTest.cpp"
  "src/json5/tests/CTestErrors.cpp"
  "src/json5/tests/CTestHost.cpp"
  "src/json5/tests/CTestParse.cpp"
  "src/json5/tests/CTestStringify.cpp"
  "src/json5/tests/test.cpp"

  "src/json5/unicode/CUString.cpp"
  "src/json5/util.cpp"

  "src/json5/values/CJSONArray.cpp"
  "src/json5/values/CJSONBoolean.cpp"
  "src/json5/values/CJSONContainer.cpp"
  "src/json5/values/CJSONNull.cpp"
  "src/json5/values/CJSONNumber.cpp"
  "src/json5/values/CJSONObject.cpp"
  "src/json5/values/CJSONString.cpp"
  "src/json5/values/CJSONValue.cpp"

  "src/main.cpp"
)

add_executable(${PROJECT_NAME} ${SOURCE})
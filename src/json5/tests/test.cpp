#include "test.hpp"

#include "CTestHost.hpp"
#include "CTestErrors.hpp"
#include "CTestParse.hpp"
#include "CTestStringify.hpp"

#include <iostream>
#include <memory>
#include <vector>

namespace json5::test
{
  /**
   * 
   */
  bool run(void)
  {
    int iTotalSuccess{ 0 };
    int iTotalFail{ 0 };

    std::vector<std::shared_ptr<CTestHost>> vTests{
      std::make_shared<errors::CTestErrors>(),
      std::make_shared<parse::CTestParse>(),
      std::make_shared<stringify::CTestStringify>()
    };

    std::printf("Running tests\n");

    for (auto &pTestHost : vTests)
    {
      int iSuccess{ 0 };
      int iFail{ 0 };

      pTestHost->run(&iSuccess, &iFail);

      iTotalSuccess += iSuccess;
      iTotalFail += iFail;
    }

    std::printf("All tests finished\n");
    std::printf("Successful: %i\n", iTotalSuccess);
    std::printf("Failed: %i\n", iTotalFail);

    return (iTotalFail == 0);
  }
}
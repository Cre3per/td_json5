#include "CJSONArray.hpp"

namespace json5
{

  /**
   * 
   */
  std::string CJSONArray::toString(const CFormatOptions &fmt) const
  {
    std::string strResult{ "" };

    strResult.append("[");

    std::int64_t iLength{ this->length() };
    bool bEmpty{ iLength == 0 };

    strResult.reserve(iLength * 8);

    if (!bEmpty)
    {
      if (fmt.bArrayValueOnNewLine)
      {
        strResult.append("\n" + fmt.strIndent);
      }
      else if (fmt.bSpaceBeforeFirstArrayValue)
      {
        strResult.append(" ");
      }
    }

    for (std::int64_t i{ 0 }; i < iLength; ++i)
    {
      if (const auto &it = this->value.find(i); it != this->value.end())
      {
        if (it->second->isString())
        {
          strResult += fmt.chQuotationMark;
          strResult.append(it->second->toString(fmt));
          strResult += fmt.chQuotationMark;
        }
        else
        {
          std::string strChild{ it->second->toString(fmt) };

          if (fmt.bArrayValueOnNewLine && (it->second->isArray() || it->second->isObject()))
          {
            for (size_t s{ 1 }; (s + 1) < strChild.length(); ++s)
            {
              if ((strChild.at(s) == '\n') && (strChild.at(s - 1) != '\\'))
              {
                strChild.insert(s + 1, fmt.strIndent);
                s += fmt.strIndent.length();
              }
            }
          }

          strResult.append(strChild);
        }
      }
      else
      {
        strResult.append("null");
      }

      bool bLastElement{ i == (iLength - 1) };

      if (!bLastElement)
      {
        strResult.append(",");
      }

      if (fmt.bArrayValueOnNewLine)
      {
        strResult.append("\n");
      }

      if (!bLastElement)
      {
        if (fmt.bArrayValueOnNewLine)
        {
          strResult.append(fmt.strIndent);
        }
        else if (fmt.bSpaceAfterArrayComma)
        {
          strResult.append(" ");
        }
      }
    }

    if (!bEmpty)
    {
      if (fmt.bArrayTrailingComma)
      {
        strResult.append(",");
      }

      if (!fmt.bArrayValueOnNewLine && fmt.bSpaceAfterLastArrayValue)
      {
        strResult.append(" ");
      }
    }

    strResult.append("]");
    strResult.reserve();

    return strResult;
  }

  /**
   * 
   */
  std::int64_t CJSONArray::length(void) const
  {
    std::int64_t iMaxIndex{ 0 };

    for (const auto it : this->value)
    {
      iMaxIndex = std::max(iMaxIndex, it.first + 1);
    }

    return iMaxIndex;
  }

  /**
   * 
   */
  void CJSONArray::push(std::shared_ptr<CJSONValue> value)
  {
    this->value[this->length()] = value;
  }

  /**
   * Destructor
   */
  CJSONArray::~CJSONArray(void)
  {
  }
}
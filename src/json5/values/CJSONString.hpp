#pragma once

#include "CJSONValue.hpp"

#include "json5/unicode/CUString.hpp"

namespace json5
{
  using string_type = CUString;

  class CJSONString : public CJSONValue
  {
  public:
    string_type value{ "" };

  public:

    using CJSONValue::toString;

    /**
     * 
     */
    virtual std::string toString(const CFormatOptions &fmt) const override;

  public:
    /**
     * Default constructor
     */
    CJSONString(void);

    /**
     * Constructor
     */
    CJSONString(const string_type &str);

    /**
     * 
     */
    virtual ~CJSONString(void);
  };
}
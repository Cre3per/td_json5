#pragma once

#include "CJSONContainer.hpp"

#include "json5/unicode/CUString.hpp"

namespace json5
{
  class CJSONObject : public CJSONContainer<CUString>
  {
  private:

    /**
     * @param fmt The formatting options
     * @param strKey The key
     * @return The quotation character (or 0) to be used to quote the key.
     */
    char getKeyQuote(const CFormatOptions &fmt, const CUString &strKey) const;

  public:

    using CJSONValue::toString;

    /**
     * 
     */
    virtual std::string toString(const CFormatOptions &fmt) const override;

    /**
     * Destructor
     */
    virtual ~CJSONObject(void);
  };
}
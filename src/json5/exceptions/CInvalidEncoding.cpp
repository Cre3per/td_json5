#include "CInvalidEncoding.hpp"

#include <sstream>

namespace json5
{
  /**
   * 
   */
  CInvalidEncoding::CInvalidEncoding(int iLine, int iColumn, const char *szMessage)
  {
    std::stringstream ss{};

    ss << "JSON5: Bad encoding at " << iLine << ":" << iColumn;

    if (szMessage != nullptr)
    {
      ss << szMessage;
    }

    this->m_strMessage = ss.str();
  }
}
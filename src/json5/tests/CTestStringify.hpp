#pragma once

#include "CTestHost.hpp"

/**
 * I'm not testing all the formatting options, because there are too many.
 */
namespace json5::test::stringify
{
  class CTestStringify : public CTestHost
  {
  private:

  public:

    /**
     * 
     */
    virtual bool run(int *pSuccess, int *pFail) override;

    /**
     * Destructor
     */
    virtual ~CTestStringify(void);
  };
}
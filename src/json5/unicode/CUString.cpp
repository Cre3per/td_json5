#include "CUString.hpp"

#include <cassert>
#include <codecvt>
#include <cstring>
#include <iomanip>
#include <locale>
#include <sstream>

namespace json5
{
  /**
   * 
   */
  void CUString::calcLength(void)
  {
    this->m_iLength = 0;

    if (this->m_data != nullptr)
    {
      for (int i{ 0 }; i < (this->m_iSize - 1); ++this->m_iLength)
      {
        char ch{ this->m_data[i] };

        i += CUString::getCharLength(ch);
      }
    }
  }

  /**
   * 
   */
  const char *CUString::c_str(void) const
  {
    return this->m_data;
  }

  /**
   * 
   */
  std::string CUString::str(void) const
  {
    return { this->c_str() };
  }

  /**
   * 
   */
  void CUString::reserve(int iCapacity)
  {
    if (iCapacity > this->m_iCapacity)
    {
      char *newData{ new char[iCapacity] };

      std::memcpy(newData, this->m_data, this->m_iSize);

      delete[] this->m_data;

      this->m_data = newData;
      this->m_iCapacity = iCapacity;
    }
  }

  /**
   * 
   */
  void CUString::shrink_to_fit(void)
  {
    if (this->m_iCapacity > this->m_iSize)
    {
      char *newData{ new char[this->m_iSize] };

      std::memcpy(newData, this->m_data, this->m_iSize);

      delete[] this->m_data;

      this->m_data = newData;
      this->m_iCapacity = this->m_iSize;
    }
  }

  /**
   * 
   */
  CUString &CUString::append(const CUString &str)
  {
    assert(this->m_data != nullptr);
    assert(str.m_data != nullptr);

    // -1, because we don't need 2 null terminators
    this->reserve(this->m_iSize + str.m_iSize - 1);

    std::memcpy(&this->m_data[this->m_iSize - 1], str.m_data, static_cast<size_t>(str.m_iSize));

    // @str's null-terminator was copied by @std::memcpy

    this->m_iLength += str.m_iLength;
    this->m_iSize += (str.m_iSize - 1);

    return *this;
  }

  /**
   *
   */
  CUString &CUString::append(char ch)
  {
    assert(this->m_data != nullptr);

    this->reserve(this->m_iSize + 1);

    this->m_data[this->m_iSize] = ch;
    this->m_data[this->m_iSize + 1] = '\x00';

    ++this->m_iLength;
    ++this->m_iSize;

    return *this;
  }

  /**
   *
   */
  CUString &CUString::append(char32_t ch)
  {
    if (ch == 0)
    {
      return this->append('\x00');
    }
    else
    {
      return this->append(CUString{ CUString::encodeCodePoint(ch) });
    }
  }

  /**
   * 
   */
  int CUString::characterToByteIndex(int iIndex) const
  {
    assert(this->m_data != nullptr);
    assert(iIndex < this->m_iLength);

    int iByteIndex{ 0 };

    for (int iCharacterIndex{ 0 }; iCharacterIndex < iIndex; ++iCharacterIndex)
    {
      char ch{ this->m_data[iByteIndex] };

      int iCharLength{ CUString::getCharLength(ch) };
      iByteIndex += iCharLength;
    }

    return iByteIndex;
  }

  /**
   * 
   */
  int CUString::byteToCharacterIndex(int iIndex) const
  {
    assert(this->m_data != nullptr);
    assert(iIndex < this->m_iSize);

    int iCharacterIndex{ 0 };

    for (int iByteIndex{ 0 }; iByteIndex < iIndex; ++iCharacterIndex)
    {
      char ch{ this->m_data[iByteIndex] };

      int iCharLength{ CUString::getCharLength(ch) };

      iByteIndex += iCharLength;
    }

    return iCharacterIndex;
  }

  /**
   * 
   */
  char32_t CUString::at(int iIndex) const
  {
    assert(this->m_data != nullptr);
    assert(iIndex < this->m_iLength);

    int iByteIndex{ 0 };

    for (int iCharacterIndex{ 0 }; iCharacterIndex < iIndex; ++iCharacterIndex)
    {
      iByteIndex += CUString::getCharLength(this->m_data[iByteIndex]);
    }

    char32_t ch{ *reinterpret_cast<char32_t *>(&this->m_data[iByteIndex]) };

    return CUString::getCodePoint(ch);
  }

  /**
   * 
   */
  char &CUString::byteAt(int iIndex)
  {
    assert(this->m_data != nullptr);
    assert(iIndex < this->m_iSize);

    return this->m_data[iIndex];
  }

  /**
   * 
   */
  const char &CUString::byteAt(int iIndex) const
  {
    assert(this->m_data != nullptr);
    assert(iIndex < this->m_iSize);

    return this->m_data[iIndex];
  }

  /**
   * 
   */
  char &CUString::byteAtChar(int iIndex)
  {
    assert(this->m_data != nullptr);
    assert(iIndex < this->m_iSize);

    int iByteIndex{ this->characterToByteIndex(iIndex) };

    return this->m_data[iByteIndex];
  }

  /**
   * 
   */
  const char &CUString::byteAtChar(int iIndex) const
  {
    assert(this->m_data != nullptr);
    assert(iIndex < this->m_iSize);

    int iByteIndex{ this->characterToByteIndex(iIndex) };

    return this->m_data[iByteIndex];
  }

  /**
   * 
   */
  int CUString::length(void) const
  {
    return this->m_iLength;
  }

  /**
   * 
   */
  int CUString::capacity(void) const
  {
    return this->m_iCapacity;
  }

  /**
   * 
   */
  int CUString::size(void) const
  {
    return this->m_iSize;
  }

  /**
   * 
   */
  void CUString::clear(void)
  {
    delete this->m_data;
    this->m_data = new char[1]{};
    this->m_iCapacity = 1;
    this->m_iSize = 1;
    this->m_iLength = 0;
  }

  /**
   * 
   */
  bool CUString::empty(void) const
  {
    return (this->m_iLength == 0);
  }

  /**
   * 
   */
  CUString &CUString::escape(bool bFullASCII)
  {
    CUString strNew{ "" };

    int iExtraBytes{ this->m_iSize - this->m_iLength };

    // There's some thought behind this math, but not a lot.
    strNew.reserve(this->m_iSize + (iExtraBytes * 12));

    for (int i{ 0 }; i < this->m_iLength; ++i)
    {
      char32_t ch{ this->at(i) };

      switch (ch)
      {
      case '\a':
        strNew.append("\\a");
        break;
      case '\b':
        strNew.append("\\b");
        break;
      case '\f':
        strNew.append("\\f");
        break;
      case '\n':
        strNew.append("\\n");
        break;
      case '\r':
        strNew.append("\\r");
        break;
      case '\t':
        strNew.append("\\t");
        break;
      case '\v':
        strNew.append("\\v");
        break;
      case '\0':
        strNew.append("\\0");
        break;
      default:
      {
        if (bFullASCII)
        {
          if (ch <= 0x7F) // ASCII
          {
            strNew.append(ch);
          }
          else if (ch <= 0xFFFF) // Basic Multilingual Plane
          {
            std::stringstream ss{};

            ss << std::hex << std::uppercase << ch;

            std::string strCodePoint{ ss.str() };
            strCodePoint.reserve(4);

            strCodePoint.insert(0, 4 - strCodePoint.length(), '0');

            strNew.append("\\u" + strCodePoint);
          }
          else // More {@link https://en.wikipedia.org/wiki/UTF-16#U+10000_to_U+10FFFF}
          {
            std::stringstream ss{};

            int iSub{ static_cast<int>(ch) - 0x10000 };

            int iHighSurrogate{ ((iSub & 0b1111'1111'1100'0000'0000) >> 10) + 0xD800 };
            int iLowSurrogate{ (iSub & 0b0000'0000'0011'1111'1111) + 0xDC00 };

            ss << std::hex << std::uppercase << std::right;
            ss << "\\u" << std::setw(4) << iHighSurrogate;
            ss << "\\u" << std::setw(4) << iLowSurrogate;

            strNew.append(ss.str());
          }
        }
        else
        {
          strNew.append(ch);
        }
      }
      break;
      }
    }

    this->operator=(strNew);

    // Get rid of the extra space created by the @reserve call above
    this->shrink_to_fit();

    return *this;
  }

  /**
   * 
   */
  CUString &CUString::operator=(const CUString &that)
  {
    delete[] this->m_data;

    this->m_iCapacity = that.m_iSize;
    this->m_iSize = that.m_iSize;
    this->m_iLength = that.m_iLength;

    if (that.m_data != nullptr)
    {
      this->m_data = new char[this->m_iCapacity]{};
      std::memcpy(this->m_data, that.m_data, static_cast<size_t>(this->m_iCapacity));
    }

    return *this;
  }

  /**
   * 
   */
  bool CUString::operator==(const CUString &that) const
  {
    if (this->m_iSize != that.m_iSize)
    {
      return false;
    }
    else if (this->m_iLength != that.m_iLength)
    {
      return false;
    }
    else
    {
      return (std::memcmp(this->m_data, that.m_data, static_cast<size_t>(this->m_iSize)) == 0);
    }
  }

  /**
   * 
   */
  bool CUString::operator!=(const CUString &that) const
  {
    return !this->operator==(that);
  }

  /**
   * 
   */
  bool CUString::operator<(const CUString &that) const
  {
    // TODO: CUString::at is slow, use byte compare

    for (int i{ 0 }; (i < this->m_iLength) && (i < that.m_iLength); ++i)
    {
      char32_t a{ this->at(i) };
      char32_t b{ that.at(i) };

      if (a < b)
      {
        return true;
      }
      else if (b < a)
      {
        return false;
      }
    }

    return (this->m_iLength < that.m_iLength);
  }

  /**
   * 
   */
  CUString::CUString(void)
  {
  }

  /**
   * 
   */
  CUString::CUString(const CUString &that)
      : m_iCapacity{ that.m_iCapacity },
        m_iSize{ that.m_iSize },
        m_iLength{ that.m_iLength }
  {
    if (that.m_data != nullptr)
    {
      this->m_data = new char[this->m_iCapacity];
      std::memcpy(this->m_data, that.m_data, static_cast<size_t>(this->m_iSize));
    }
  }

  /**
   * 
   */
  CUString::CUString(const char *szText)
      : m_iCapacity{ static_cast<int>(std::strlen(szText) + 1) }
  {
    this->m_iSize = this->m_iCapacity;
    this->m_data = new char[this->m_iCapacity];

    std::strcpy(this->m_data, szText);

    this->calcLength();
  }

  /**
   * 
   */
  CUString::CUString(const char *szText, int iSize)
      : m_iCapacity{ iSize },
        m_iSize{ iSize }
  {
    this->m_data = new char[this->m_iCapacity];
    std::memcpy(this->m_data, szText, iSize);

    this->calcLength();
  }

  /**
   * 
   */
  CUString::CUString(const std::string &strText)
      : CUString{ strText.c_str() }
  {
  }

  /**
   * 
   */
  CUString::CUString(char32_t ch)
      : CUString{ CUString::encodeCodePoint(ch).c_str() }
  {
  }

  /**
   * 
   */
  CUString::~CUString(void)
  {
    delete[] this->m_data;
  }

  /**
   * {@url https://en.wikipedia.org/wiki/UTF-8#Description}
   */
  int CUString::getCharLength(char ch)
  {
    if (ch & (1 << 7))
    {
      int iBytes{ 1 };

      for (int i{ 6 }; i > 3; --i)
      {
        if (ch & (1 << i))
        {
          ++iBytes;
        }
        else
        {
          break;
        }
      }

      return ((iBytes == 1) ? 0 : iBytes);
    }
    else
    {
      return 1;
    }
  }

  /**
   *
   */
  int CUString::getCharLength(char32_t ch)
  {
    return getCharLength(*reinterpret_cast<char *>(&ch));
  }

  /**
   * 
   */
  int CUString::getCodePoint(char32_t ch)
  {
    int iCharacterLength{ CUString::getCharLength(*reinterpret_cast<char *>(&ch)) };

    assert(iCharacterLength > 0);

    if (iCharacterLength == 1)
    {
      return static_cast<int>(*reinterpret_cast<char *>(&ch));
    }
    else
    {
      char *arr{ reinterpret_cast<char *>(&ch) };

      int iResult{ static_cast<int>((arr[0] & (0xFF >> (1 + iCharacterLength))) << ((iCharacterLength - 1) * 6)) };

      for (int i{ 1 }; i < iCharacterLength; ++i)
      {
        iResult |= ((arr[i] & 0b0011'1111) << (((iCharacterLength - 1) - i) * 6));
      }

      return iResult;
    }
  }

  /**
   * 
   */
  std::string CUString::encodeCodePoint(char32_t ch)
  {
    static std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> convert{};

    return convert.to_bytes(ch);
  }
}
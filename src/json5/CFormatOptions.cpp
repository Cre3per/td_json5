#include "CFormatOptions.hpp"

namespace json5
{
  /**
   * 
   */
  void CFormatOptions::jsonCompatible(void)
  {
    this->chQuotationMark = '\"';
    this->chKeyQuotationMark = '\"';
    this->bArrayTrailingComma = false;
    this->nsNumbers = NumberStyle::kDecimal;
  }

  /**
   * 
   */
  void CFormatOptions::minumumStorage(void)
  {
    this->bSpaceBeforeFirstArrayValue = false;
    this->bSpaceAfterLastArrayValue = false;
    this->bSpaceAfterArrayComma = false;
    this->bArrayTrailingComma = false;
    this->bArrayValueOnNewLine = false;
    this->bObjectPairsOnNewLine = false;
    this->bObjectSpaceAfterColon = false;
    this->bObjectSpaceAfterComma = false;
    this->bObjectSpaceBeforeFirstPair = false;
    this->bObjectSpaceAfterLastPair = false;
  }
}
#pragma once

#include "CSharedPointer.hpp"

#include "values/CJSONArray.hpp"
#include "values/CJSONBoolean.hpp"
#include "values/CJSONNull.hpp"
#include "values/CJSONNumber.hpp"
#include "values/CJSONObject.hpp"
#include "values/CJSONString.hpp"
#include "values/CJSONValue.hpp"

namespace json5
{
  // class CJSONArray;
  // class CJSONBoolean;
  // class CJSONNumber;
  // class CJSONObject;
  // class CJSONString;
  // class CJSONValue;

  /**
   * User-accessible types.
   */

  using let = CSharedPointer<CJSONValue>;
  using var = let;

  using array = CSharedPointer<CJSONArray>;
  using boolean = CSharedPointer<CJSONBoolean>;
  using number = CSharedPointer<CJSONNumber>;
  using object = CSharedPointer<CJSONObject>;
  using string = CSharedPointer<CJSONString>;
}
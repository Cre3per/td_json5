#include "CJSONNull.hpp"

namespace json5
{
  CJSONNull null{ };

  /**
   * 
   */
  std::string CJSONNull::toString(const CFormatOptions &fmt) const
  {
    return "null";
  }

  CJSONNull::~CJSONNull(void)
  {

  }
}
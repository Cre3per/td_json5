#pragma once

#include "CStringStream.hpp"
#include "CToken.hpp"
#include "state.hpp"

#include "json5/unicode/CUString.hpp"
#include "json5/values/CJSONValue.hpp"

#include <cstdint>
#include <memory>
#include <stack>
#include <string>

namespace json5
{
  class CParser
  {
  private:
    /**
     * Text input
     */
    CStringStream m_ssInput{ };

    /**
     * Current state of the parser
     */
    State m_state{ State::kDefault };

    /**
     * Stack
     */
    std::stack<std::shared_ptr<CJSONValue>> m_stack{};

    /**
     * The root element
     */
    std::shared_ptr<CJSONValue> m_pRoot{ nullptr };

    /**
     * Current token
     */
    CToken m_token{};

    /**
     * Current property name
     */
    CUString m_strKey{ "" };

  private:
    /**
     * Pushes the current token onto the stack
     */
    void push(void);

    /**
     * Pops the top off the stack
     */
    void pop(void);

    /**
     * 
     */
    void parseStart(void);

    /**
     * 
     */
    void parseBeforePropertyName(void);

    /**
     * 
     */
    void parseAfterPropertyName(void);

    /**
     * 
     */
    void parseBeforePropertyValue(void);

    /**
     * 
     */
    void parseAfterPropertyValue(void);

    /**
     * 
     */
    void parseBeforeArrayValue(void);

    /**
     * 
     */
    void parseAfterArrayValue(void);

    /**
     * Parses in the current state
     */
    void parse(void);

  public:

    /**
     * Parses a JSON string, constructing the value or object described by the
     * string.
     * @param strSource The JSON string
     * @return The parsed value or object
     */
    std::shared_ptr<CJSONValue> parse(const std::string &strSource);
  };
}
#include "CLexer.hpp"

#include "CParser.hpp"
#include "CStringStream.hpp"

#include "json5/exceptions/CInvalidCharacter.hpp"
#include "json5/exceptions/CInvalidEncoding.hpp"
#include "json5/exceptions/CInvalidIdentifier.hpp"

#include "json5/values/CJSONBoolean.hpp"
#include "json5/values/CJSONNull.hpp"
#include "json5/values/CJSONNumber.hpp"
#include "json5/values/CJSONString.hpp"

#include "json5/util.hpp"

#include <cassert>
#include <memory>

namespace json5
{
  /**
   * 
   */
  char32_t CLexer::escapeHexCharacter(void)
  {
    char chBuffer[3]{};

    if (!util::isHexDigit(this->m_pInput->peek()))
    {
      throw CInvalidCharacter{ this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn() };
      return 0;
    }

    chBuffer[0] = this->m_pInput->read();

    if (!util::isHexDigit(this->m_pInput->peek()))
    {
      throw CInvalidCharacter{ this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn() };
      return 0;
    }

    chBuffer[1] = this->m_pInput->read();

    return static_cast<char32_t>(std::strtol(chBuffer, nullptr, 16));
  }

  /**
   * 
   */
  char32_t CLexer::escapeUnicodeCharacter(void)
  {
    char chBuffer[5]{};

    for (int i{ 0 }; i < 4; ++i)
    {
      if (util::isHexDigit(this->m_pInput->peek()))
      {
        chBuffer[i] = static_cast<char>(this->m_pInput->read());
      }
      else
      {
        throw CInvalidCharacter{ this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn() };
      }
    }

    char32_t chEscaped{ static_cast<char32_t>(std::strtol(chBuffer, nullptr, 16)) };

    if ((chEscaped > 0xD800) && (chEscaped < 0xDBFF)) // High surrogate
    {
      // I don't want UTF-16, try to re-encoded as UTF-8

      this->literal("\\u");

      for (int i{ 0 }; i < 4; ++i)
      {
        if (util::isHexDigit(this->m_pInput->peek()))
        {
          chBuffer[i] = static_cast<char>(this->m_pInput->read());
        }
        else
        {
          throw CInvalidCharacter{ this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn() };
        }
      }

      int iHighSurrogate{ static_cast<int>(chEscaped) };
      int iLowSurrogate{ static_cast<int>(std::strtol(chBuffer, nullptr, 16)) };

      if ((iLowSurrogate < 0xDC00) || (iLowSurrogate > 0xDFFF))
      {
        throw CInvalidEncoding{ this->m_pInput->getLine(), this->m_pInput->getColumn() - 6, "Invalid UTF-16 low surrogate" };
      }

      chEscaped = 0;

      chEscaped |= (iLowSurrogate - 0xDC00);
      chEscaped |= ((iHighSurrogate - 0xD800) << 10);

      chEscaped += 0x10000;
    }

    return chEscaped;
  }

  /**
   * 
   */
  char32_t CLexer::unescapeCharacter(char32_t ch, bool *pSkip)
  {
    switch (ch)
    {
    case 'a':
      // TODO: Why doesn't jordans code include bell? He has a test for it.
      this->m_pInput->read();
      return '\a';
      break;
    case 'b':
      this->m_pInput->read();
      return '\b';
      break;
    case 'f':
      this->m_pInput->read();
      return '\f';
      break;
    case 'n':
      this->m_pInput->read();
      return '\n';
      break;
    case 'r':
      this->m_pInput->read();
      return '\r';
      break;
    case 't':
      this->m_pInput->read();
      return '\t';
      break;
    case 'v':
      this->m_pInput->read();
      return '\v';
      break;
    case '0':
      this->m_pInput->read();
      if (util::isDecDigit(this->m_pInput->peek()))
      {
        throw CInvalidCharacter{ this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn() };
      }

      return '\0';
      break;
    case 'x':
      this->m_pInput->read();
      return this->escapeHexCharacter();
      break;
    case 'u':
      this->m_pInput->read();
      return this->escapeUnicodeCharacter();
      break;
    case '\n':
    case u'\u2028':
    case u'\u2029':
      this->m_pInput->read();
      *pSkip = true;
      return '\0';
      break;
    case '\r':
      this->m_pInput->read();
      if (this->m_pInput->peek() == '\n')
      {
        this->m_pInput->read();
      }
      *pSkip = true;
      return '\0';
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
      throw CInvalidCharacter(this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn());
      break;
      return '\0';
    }

    // ' "
    return this->m_pInput->read();
  }

  /**
   * 
   */
  CToken CLexer::newToken(TokenType type, std::shared_ptr<CJSONValue> pValue)
  {
    return CToken(type, pValue, this->m_pInput->getLine(), this->m_pInput->getColumn());
  }

  /**
   * 
   */
  void CLexer::literal(const CUString &str) const
  {
    for (int i{ 0 }; i < str.length(); ++i)
    {
      char32_t ch{ this->m_pInput->peek() };

      if (ch != str.at(i))
      {
        throw CInvalidCharacter(ch, this->m_pInput->getLine(), this->m_pInput->getColumn());
      }

      this->m_pInput->read();
    }
  }

  /**
   * 
   */
  CToken CLexer::lexDefault(char32_t ch)
  {
    if (this->m_pInput->eof())
    {
      this->m_pInput->read();
      return this->newToken(TokenType::kEOF);
    }

    switch (ch)
    {
    case '\t':
    case '\v':
    case '\f':
    case ' ':
    case u'\u00A0':
    case u'\uFEFF':
    case '\n':
    case '\r':
    case u'\u2028':
    case u'\u2029':
      this->m_pInput->read();
      return CToken::invalid();
      break;
    case '/':
      this->m_pInput->read();
      this->m_state = State::kComment;
      return CToken::invalid();
      break;
    }

    if (util::isSpaceSeparator(ch))
    {
      this->m_pInput->read();
      return CToken::invalid();
    }

    this->m_state = this->m_sParser;
    return this->lex(ch);
  }

  /**
   * 
   */
  CToken CLexer::lexComment(char32_t ch)
  {
    switch (ch)
    {
    case '*':
      this->m_pInput->read();
      this->m_state = State::kCommentMultiLine;
      return CToken::invalid();
      break;
    case '/':
      this->m_pInput->read();
      this->m_state = State::kSingleLineComment;
      return CToken::invalid();
      break;
    default:
      throw CInvalidCharacter(this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn());
      break;
    }
  }

  /**
   * 
   */
  CToken CLexer::lexCommentMultiLine(char32_t ch)
  {
    switch (ch)
    {
    case '*':
      this->m_pInput->read();
      this->m_state = State::kCommentMultiLineAsterisk;
      return CToken::invalid();
      break;
    case '\x00':
      throw CInvalidCharacter(this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn());
      break;
    }

    this->m_pInput->read();

    return CToken::invalid();
  }

  /**
   * 
   */
  CToken CLexer::lexCommentMultiLineAsterisk(char32_t ch)
  {
    switch (ch)
    {
    case '*':
      this->m_pInput->read();
      return CToken::invalid();
      break;
    case '/':
      this->m_pInput->read();
      this->m_state = State::kDefault;
      return CToken::invalid();
      break;
    }

    this->m_pInput->read();
    this->m_state = State::kCommentMultiLine;

    return CToken::invalid();
  }

  /**
   * 
   */
  CToken CLexer::lexCommentSingleLine(char32_t ch)
  {
    if (this->m_pInput->eof())
    {
      this->m_pInput->read();
      return this->newToken(TokenType::kEOF);
    }

    switch (ch)
    {
    case '\n':
    case '\r':
    case u'\u2028':
    case u'\u2029':
      this->m_pInput->read();
      this->m_state = State::kDefault;
      return CToken::invalid();
      break;
    }

    this->m_pInput->read();
    return CToken::invalid();
  }

  /**
   * 
   */
  CToken CLexer::lexValue(char32_t ch)
  {
    switch (ch)
    {
    case '{':
    case '[':
      return this->newToken(TokenType::kPunctuator, std::make_shared<CJSONString>(this->m_pInput->read()));
      break;
    case 'n':
      this->m_pInput->read();
      this->literal("ull");
      return this->newToken(TokenType::kNull, std::make_shared<CJSONNull>());
      break;
    case 't':
      this->m_pInput->read();
      this->literal("rue");
      return this->newToken(TokenType::kBoolean, std::make_shared<CJSONBoolean>(true));
      break;
    case 'f':
      this->m_pInput->read();
      this->literal("alse");
      return this->newToken(TokenType::kBoolean, std::make_shared<CJSONBoolean>(false));
      break;
    case '-':
    case '+':
      this->m_iSign = ((this->m_pInput->read() == U'-') ? -1 : 1);

      this->m_state = State::kSign;
      return CToken::invalid();
      break;
    case '.':
      this->m_strBuffer = this->m_pInput->read();
      this->m_state = State::kDecimalPointLeading;
      return CToken::invalid();
      break;
    case '0':
      this->m_strBuffer = this->m_pInput->read();
      this->m_state = State::kZero;
      return CToken::invalid();
      break;
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
      this->m_strBuffer = this->m_pInput->read();
      this->m_state = State::kDecimalInteger;
      return CToken::invalid();
      break;
    case 'I':
      this->m_pInput->read();
      this->literal("nfinity");
      return this->newToken(TokenType::kNumeric, std::make_shared<CJSONNumber>(this->m_iSign * std::numeric_limits<number_type>::infinity()));
      break;
    case 'N':
      this->m_pInput->read();
      this->literal("aN");
      return this->newToken(TokenType::kNumeric, std::make_shared<CJSONNumber>(std::numeric_limits<number_type>::quiet_NaN()));
      break;
    case '\"':
    case '\'':
      this->m_bDoubleQuote = (this->m_pInput->read() == U'\"');
      this->m_strBuffer.clear();
      this->m_state = State::kString;
      return CToken::invalid();
      break;
    }

    throw CInvalidCharacter{ this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn() };
  }

  /**
   * 
   */
  CToken CLexer::lexIdentifierNameStartEscape(char32_t ch)
  {
    if (ch == U'u')
    {
      this->m_pInput->read();

      char32_t chEscaped{ this->escapeUnicodeCharacter() };

      if (!util::isIdStartChar(chEscaped))
      {
        throw CInvalidIdentifier{ this->m_pInput->getLine(), this->m_pInput->getColumn() };
      }

      this->m_strBuffer.append(chEscaped);
      this->m_state = State::kIdentifierName;
    }
    else
    {
      throw CInvalidCharacter{ this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn() };
    }

    return CToken::invalid();
  }

  /**
   * 
   */
  CToken CLexer::lexIdentifierName(char32_t ch)
  {
    if (ch == '\\')
    {
      this->m_pInput->read();
      this->m_state = State::kIdentifierNameEscape;
      return CToken::invalid();
    }
    else if (util::isIdContinueChar(ch))
    {
      this->m_strBuffer.append(this->m_pInput->read());
      return CToken::invalid();
    }
    else
    {
      return this->newToken(TokenType::kIdentifier, std::make_shared<CJSONString>(this->m_strBuffer));
    }
  }

  /**
   * 
   */
  CToken CLexer::lexIdentifierNameEscape(char32_t ch)
  {
    if (ch == 'u')
    {
      this->m_pInput->read();
      char32_t chEscaped{ this->escapeUnicodeCharacter() };

      if (!util::isIdContinueChar(chEscaped))
      {
        throw CInvalidIdentifier{ this->m_pInput->getLine(), this->m_pInput->getColumn() };
      }

      this->m_strBuffer.append(chEscaped);
      this->m_state = State::kIdentifierName;

      return CToken::invalid();
    }
    else
    {
      throw CInvalidCharacter{ this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn() };
      return CToken::invalid();
    }
  }

  /**
   * 
   */
  CToken CLexer::lexSign(char32_t ch)
  {
    switch (ch)
    {
    case '.':
      this->m_strBuffer = this->m_pInput->read();
      this->m_state = State::kDecimalPointLeading;
      return CToken::invalid();
      break;
    case '0':
      this->m_strBuffer = this->m_pInput->read();
      this->m_state = State::kZero;
      return CToken::invalid();
      break;
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
      this->m_strBuffer = this->m_pInput->read();
      this->m_state = State::kDecimalInteger;
      return CToken::invalid();
      break;
    case 'I':
      this->m_pInput->read();
      this->literal("nfinity");
      return this->newToken(TokenType::kNumeric, std::make_shared<CJSONNumber>(this->m_iSign * std::numeric_limits<number_type>::infinity()));
      break;
    case 'N':
      this->m_pInput->read();
      this->literal("aN");
      return this->newToken(TokenType::kNumeric, std::make_shared<CJSONNumber>(std::numeric_limits<number_type>::quiet_NaN()));
      break;
    }

    throw CInvalidCharacter{ this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn() };
  }

  /**
   * 
   */
  CToken CLexer::lexZero(char32_t ch)
  {
    switch (ch)
    {
    case '.':
      this->m_strBuffer.append(this->m_pInput->read());
      this->m_state = State::kDecimalPoint;
      return CToken::invalid();
      break;
    case 'e':
    case 'E':
      this->m_strBuffer.append(this->m_pInput->read());
      this->m_state = State::kDecimalExponent;
      return CToken::invalid();
      break;
    case 'x':
    case 'X':
      this->m_strBuffer.append(this->m_pInput->read());
      this->m_state = State::kHexadecimal;
      return CToken::invalid();
      break;
    }

    return this->newToken(TokenType::kNumeric, std::make_shared<CJSONNumber>(0.0));
  }

  /**
   * 
   */
  CToken CLexer::lexDecimalInteger(char32_t ch)
  {
    switch (ch)
    {
    case '.':
      this->m_strBuffer.append(this->m_pInput->read());
      this->m_state = State::kDecimalPoint;
      return CToken::invalid();
      break;
    case 'e':
    case 'E':
      this->m_strBuffer.append(this->m_pInput->read());
      this->m_state = State::kDecimalExponent;
      return CToken::invalid();
      break;
    default:
      if (util::isDecDigit(ch))
      {
        this->m_strBuffer.append(this->m_pInput->read());
        return CToken::invalid();
      }
      break;
    }

    return this->newToken(TokenType::kNumeric, std::make_shared<CJSONNumber>(this->m_iSign * util::parseNumber(this->m_strBuffer)));
  }

  /**
   * 
   */
  CToken CLexer::lexDecimalPointLeading(char32_t ch)
  {
    if (util::isDecDigit(ch))
    {
      this->m_strBuffer.append(this->m_pInput->read());
      this->m_state = State::kDecimalFraction;
      return CToken::invalid();
    }
    else
    {
      throw CInvalidCharacter{ this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn() };
      return CToken::invalid();
    }
  }

  /**
   * 
   */
  CToken CLexer::lexDecimalPoint(char32_t ch)
  {
    if ((ch == 'e') || (ch == 'E'))
    {
      this->m_strBuffer.append(this->m_pInput->read());
      this->m_state = State::kDecimalExponent;
      return CToken::invalid();
    }
    else if (util::isDecDigit(ch))
    {
      this->m_strBuffer.append(this->m_pInput->read());
      this->m_state = State::kDecimalFraction;
      return CToken::invalid();
    }
    else
    {
      return this->newToken(TokenType::kNumeric, std::make_shared<CJSONNumber>(this->m_iSign * util::parseNumber(this->m_strBuffer)));
    }
  }

  /**
   * 
   */
  CToken CLexer::lexDecimalFraction(char32_t ch)
  {
    switch (ch)
    {
    case 'e':
    case 'E':
      this->m_strBuffer.append(this->m_pInput->read());
      this->m_state = State::kDecimalExponent;
      return CToken::invalid();
      break;
    }

    if (util::isDecDigit(ch))
    {
      this->m_strBuffer.append(this->m_pInput->read());
      return CToken::invalid();
    }

    return this->newToken(TokenType::kNumeric, std::make_shared<CJSONNumber>(this->m_iSign * util::parseNumber(this->m_strBuffer)));
  }

  /**
   * 
   */
  CToken CLexer::lexDecimalExponent(char32_t ch)
  {
    switch (ch)
    {
    case '+':
    case '-':
      this->m_strBuffer.append(this->m_pInput->read());
      this->m_state = State::kDecimalExponentSign;
      return CToken::invalid();
      break;
    default:
      if (util::isDecDigit(ch))
      {
        this->m_strBuffer.append(this->m_pInput->read());
        this->m_state = State::kDecimalExponentInteger;
        return CToken::invalid();
      }
      else
      {
        throw CInvalidCharacter{ this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn() };
      }

      break;
    }
  }

  /**
   * 
   */
  CToken CLexer::lexDecimalExponentSign(char32_t ch)
  {
    if (util::isDecDigit(ch))
    {
      this->m_strBuffer.append(this->m_pInput->read());
      this->m_state = State::kDecimalExponentInteger;
      return CToken::invalid();
    }
    else
    {
      throw CInvalidCharacter{ this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn() };
    }
  }

  /**
   * 
   */
  CToken CLexer::lexDecimalExponentInteger(char32_t ch)
  {
    if (util::isDecDigit(ch))
    {
      this->m_strBuffer.append(this->m_pInput->read());
      return CToken::invalid();
    }
    else
    {
      return this->newToken(TokenType::kNumeric, std::make_shared<CJSONNumber>(this->m_iSign * util::parseNumber(this->m_strBuffer)));
    }
  }

  /**
   * 
   */
  CToken CLexer::lexHexadecimal(char32_t ch)
  {
    if (util::isHexDigit(ch))
    {
      this->m_strBuffer.append(this->m_pInput->read());
      this->m_state = State::kHexadecimalInteger;
      return CToken::invalid();
    }
    else
    {
      throw CInvalidCharacter{ this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn() };
    }
  }

  /**
   * 
   */
  CToken CLexer::lexHexadecimalInteger(char32_t ch)
  {
    if (util::isHexDigit(ch))
    {
      this->m_strBuffer.append(this->m_pInput->read());
      return CToken::invalid();
    }
    else
    {
      return this->newToken(TokenType::kNumeric, std::make_shared<CJSONNumber>(this->m_iSign * util::parseNumber(this->m_strBuffer)));
    }
  }

  /**
   * 
   */
  CToken CLexer::lexString(char32_t ch)
  {
    switch (ch)
    {
    case '\\':
    {
      bool bSkip{ false };
      this->m_pInput->read();
      if (char32_t chEscaped{ this->unescapeCharacter(this->m_pInput->peek(), &bSkip) }; !bSkip)
      {
        this->m_strBuffer.append(chEscaped);
      }

      return CToken::invalid();
      break;
    }
    case '\"':
      if (this->m_bDoubleQuote)
      {
        this->m_pInput->read();
        return this->newToken(TokenType::kString, std::make_shared<CJSONString>(this->m_strBuffer));
      }
      this->m_strBuffer.append(this->m_pInput->read());
      return CToken::invalid();
      break;
    case '\'':
      if (!this->m_bDoubleQuote)
      {
        this->m_pInput->read();
        return this->newToken(TokenType::kString, std::make_shared<CJSONString>(this->m_strBuffer));
      }
      this->m_strBuffer.append(this->m_pInput->read());
      return CToken::invalid();
      break;
    case '\r':
    case '\n':
      throw CInvalidCharacter{ this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn() };
      return CToken::invalid();
      break;
    case '\0':
      throw CInvalidCharacter{ this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn() };
      return CToken::invalid();
      break;

      // Skipping invalid ECMAScript warnings.
    }

    this->m_strBuffer.append(this->m_pInput->read());
    return CToken::invalid();
  }

  /**
   *
   */
  CToken CLexer::lexStart(char32_t ch)
  {
    switch (ch)
    {
    case '{':
    case '[':
      return this->newToken(TokenType::kPunctuator, std::make_shared<CJSONString>(this->m_pInput->read()));
      break;
    default:
      this->m_state = State::kValue;
      return CToken::invalid();
      break;
    }
  }

  /**
   * 
   */
  CToken CLexer::lexBeforePropertyName(char32_t ch)
  {
    switch (ch)
    {
    case '\\':
      this->m_pInput->read();
      this->m_state = State::kIdentifierNameStartEscape;
      return CToken::invalid();
      break;
    case '}':
      return this->newToken(TokenType::kPunctuator, std::make_shared<CJSONString>(this->m_pInput->read()));
      break;
    case '\"':
    case '\'':
      this->m_bDoubleQuote = (this->m_pInput->read() == '\"');
      this->m_state = State::kString;
      return CToken::invalid();
      break;
    }

    if (util::isIdStartChar(ch))
    {
      this->m_strBuffer = this->m_pInput->read();
      this->m_state = State::kIdentifierName;

      return CToken::invalid();
    }

    throw CInvalidCharacter{ this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn() };
    return CToken::invalid();
  }

  /**
   * 
   */
  CToken CLexer::lexAfterPropertyName(char32_t ch)
  {
    if (ch == ':')
    {
      return this->newToken(TokenType::kPunctuator, std::make_shared<CJSONString>(this->m_pInput->read()));
    }
    else
    {
      throw CInvalidCharacter{ this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn() };
    }
  }

  /**
   * 
   */
  CToken CLexer::lexAfterPropertyValue(char32_t ch)
  {
    switch (ch)
    {
    case ',':
    case '}':
      return this->newToken(TokenType::kPunctuator, std::make_shared<CJSONString>(this->m_pInput->read()));
    default:
      throw CInvalidCharacter{ this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn() };
      return CToken::invalid();
    }
  }

  /**
   * 
   */
  CToken CLexer::lexBeforeArrayValue(char32_t ch)
  {
    if (ch == ']')
    {
      return this->newToken(TokenType::kPunctuator, std::make_shared<CJSONString>(this->m_pInput->read()));
    }

    this->m_state = State::kValue;
    return CToken::invalid();
  }

  /**
   * 
   */
  CToken CLexer::lexAfterArrayValue(char32_t ch)
  {
    switch (ch)
    {
    case ',':
    case ']':
      return this->newToken(TokenType::kPunctuator, std::make_shared<CJSONString>(this->m_pInput->read()));
      break;
    default:
      throw CInvalidCharacter{ this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn() };
      return CToken::invalid();
    }
  }

  /**
   * 
   */
  CToken CLexer::lexEnd(char32_t ch)
  {
    throw CInvalidCharacter{ this->m_pInput->read(), this->m_pInput->getLine(), this->m_pInput->getColumn() };
    return CToken::invalid();
  }

  /**
   * 
   */
  CToken CLexer::lex(char32_t ch)
  {
    switch (this->m_state)
    {
    case State::kDefault:
      return this->lexDefault(ch);
      break;
    case State::kAfterArrayValue:
      return this->lexAfterArrayValue(ch);
      break;
    case State::kAfterPropertyName:
      return this->lexAfterPropertyName(ch);
      break;
    case State::kAfterPropertyValue:
      return this->lexAfterPropertyValue(ch);
      break;
    case State::kBeforeArrayValue:
      return this->lexBeforeArrayValue(ch);
      break;
    case State::kBeforePropertyName:
      return this->lexBeforePropertyName(ch);
      break;
    case State::kComment:
      return this->lexComment(ch);
      break;
    case State::kCommentMultiLine:
      return this->lexCommentMultiLine(ch);
      break;
    case State::kCommentMultiLineAsterisk:
      return this->lexCommentMultiLineAsterisk(ch);
      break;
    case State::kDecimalExponent:
      return this->lexDecimalExponent(ch);
      break;
    case State::kDecimalExponentInteger:
      return this->lexDecimalExponentInteger(ch);
      break;
    case State::kDecimalExponentSign:
      return this->lexDecimalExponentSign(ch);
      break;
    case State::kDecimalFraction:
      return this->lexDecimalFraction(ch);
      break;
    case State::kDecimalInteger:
      return this->lexDecimalInteger(ch);
      break;
    case State::kDecimalPoint:
      return this->lexDecimalPoint(ch);
      break;
    case State::kDecimalPointLeading:
      return this->lexDecimalPointLeading(ch);
      break;
    case State::kHexadecimal:
      return this->lexHexadecimal(ch);
      break;
    case State::kHexadecimalInteger:
      return this->lexHexadecimalInteger(ch);
      break;
    case State::kIdentifierName:
      return this->lexIdentifierName(ch);
      break;
    case State::kIdentifierNameEscape:
      return this->lexIdentifierNameEscape(ch);
      break;
    case State::kIdentifierNameStartEscape:
      return this->lexIdentifierNameStartEscape(ch);
      break;
    case State::kSign:
      return this->lexSign(ch);
      break;
    case State::kSingleLineComment:
      return this->lexCommentSingleLine(ch);
      break;
    case State::kStart:
      return this->lexStart(ch);
      break;
    case State::kString:
      return this->lexString(ch);
      break;
    case State::kValue:
    case State::kBeforePropertyValue:
      return this->lexValue(ch);
      break;
    case State::kZero:
      return this->lexZero(ch);
      break;
    case State::kEnd:
      return this->lexEnd(ch);
      break;
    default:
      assert(false);
      break;
    }

    return CToken::invalid();
  }

  /**
   * 
   */
  CToken CLexer::lex(State sParser)
  {
    this->m_sParser = sParser;
    this->m_state = State::kDefault;
    this->m_strBuffer.clear();
    this->m_bDoubleQuote = false;
    this->m_iSign = 1;

    while (true)
    {
      char32_t chNextCharacter{ this->m_pInput->peek() };

      CToken token{ this->lex(chNextCharacter) };

      if (token.getType() != TokenType::kInvalid)
      {
        return token;
      }
    }

    assert(false);
    return CToken::invalid();
  }

  /**
   * 
   */
  CLexer::CLexer(CStringStream *pInput)
      : m_pInput{ pInput }
  {
  }
}
#include "CJSONValue.hpp"

namespace json5
{
  class CJSONNull : public CJSONValue
  {
  public:
    using CJSONValue::toString;

    /**
     * 
     */
    virtual std::string toString(const CFormatOptions &fmt) const override;

  public:
    virtual ~CJSONNull(void);
  };

  extern CJSONNull null;
}
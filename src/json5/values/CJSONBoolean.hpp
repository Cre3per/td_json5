#pragma once

#include "CJSONValue.hpp"

namespace json5
{
  using boolean_type = bool;

  class CJSONBoolean : public CJSONValue
  {
  public:
    boolean_type value{ false };

  public:
    using CJSONValue::toString;
    
    /**
     * 
     */
    virtual std::string toString(const CFormatOptions &fmt) const override;

  public:
    /**
     * Default constructor
     */
    CJSONBoolean(void);

    /**
     * Constructor
     */
    CJSONBoolean(boolean_type bValue);

    /**
     * 
     */
    virtual ~CJSONBoolean(void);
  };
}
#include "CToken.hpp"

namespace json5
{
  /**
   * 
   */
  TokenType CToken::getType(void) const
  {
    return this->m_type;
  }

  /**
   * 
   */
  std::shared_ptr<CJSONValue> CToken::getValue(void)
  {
    return this->m_pValue;
  }

  /**
   * 
   */
  CToken::CToken(void)
  {

  }

  /**
   * 
   */
  CToken::CToken(TokenType type, std::shared_ptr<CJSONValue> pValue, int iLine, int iColumn)
      : m_type{ type },
        m_pValue{ pValue },
        m_iLine{ iLine },
        m_iColumn{ iColumn }
  {
  }

  /**
   * 
   */
  CToken CToken::invalid(void)
  {
    return CToken(TokenType::kInvalid, nullptr, 0, 0);
  }
}
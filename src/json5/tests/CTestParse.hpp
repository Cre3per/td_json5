#pragma once

#include "CTestHost.hpp"

namespace json5::test::parse
{
  class CTestParse : public CTestHost
  {
  private:

  public:

    /**
     * 
     */
    virtual bool run(int *pSuccess, int *pFail) override;

    /**
     * Destructor
     */
    virtual ~CTestParse(void);
  };
}
#include "CJSONBoolean.hpp"

namespace json5
{

  /**
   * 
   */
  std::string CJSONBoolean::toString(const CFormatOptions &fmt) const 
  {
    return (this->value ? "true" : "false");
  }

  /**
   * 
   */
  CJSONBoolean::CJSONBoolean(void)
      : value{ false }
  {

  }

  /**
   * 
   */
  CJSONBoolean::CJSONBoolean(boolean_type bValue)
      : value{ bValue }
  {

  }

  /**
   * 
   */
  CJSONBoolean::~CJSONBoolean(void)
  {
  }
}
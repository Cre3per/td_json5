#pragma once

#include <functional>
#include <string>

namespace json5::test
{
  class CTest
  {
  private:
    /**
     * Name
     */
    std::string m_strName{ "" };

    /**
     * The test function
     */
    std::function<bool(void)> m_fnTest{ nullptr };

  public:

    /**
     * Runs the test and prints status to the standard output.
     * @return True on success
     */
    bool run(void);

  public:

    /**
     * Default constructor
     */
    CTest(void) = delete;

    /**
     * Constructor
     * @param strName Test name
     * @param fnTest Test function
     */
    CTest(const std::string &strName, const std::function<bool(void)> fnTest);
  };
}
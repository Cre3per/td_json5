#pragma once

#include <string>

namespace json5
{
  class CFormatOptions
  {
  private:
  public:
    enum class NumberStyle
    {
      kDecimal,
      kHexadecimal,
      kHexadecimalCaps,
      kFloating,
    };

  public:
    /**
     * Quotation marks used for strings.
     * Valid options are ' "
     */
    char chQuotationMark{ '\"' };

    /**
     * Quotation marks used for keys.
     * Valid options are ' " \0
     */
    char chKeyQuotationMark{ '\'' };

    /**
     * Indention.
     * Valid options are an empty string or any amount of whitespace characters.
     */
    std::string strIndent{ "  " };

    /**
     * Insert a space after the opening square bracket.
     */
    bool bSpaceBeforeFirstArrayValue{ true };

    /**
     * Insert a space before the closing square bracket.
     */
    bool bSpaceAfterLastArrayValue{ true };

    /**
     * Insert a space after each comma, except the trailing one, if {@link
     * bSpaceAfterLastArrayValue} is enabled.
     */
    bool bSpaceAfterArrayComma{ true };

    /**
     * Add a trailing comma after the last array value.
     */
    bool bArrayTrailingComma{ false };

    /**
     * Each array value is on it's own line.
     */
    bool bArrayValueOnNewLine{ false };

    /**
     * Each key-value pair is on it's own line.
     */
    bool bObjectPairsOnNewLine{ true };

    /**
     * Space after the colon between key and value.
     */
    bool bObjectSpaceAfterColon{ true };

    /**
     * If {@link bObjectPairsOnNewLine} is false, place a space after commas.
     */
    bool bObjectSpaceAfterComma{ true };

    /**
     * Space after the opening curly bracket
     */
    bool bObjectSpaceBeforeFirstPair{ true };

    /**
     * Space before the closing curly bracket
     */
    bool bObjectSpaceAfterLastPair{ true };

    /**
     * Output is plain ASCII
     */
    bool bASCII{ false };

    /**
     * Style of numbers.
     */
    NumberStyle nsNumbers{ NumberStyle::kDecimal };

  public:

    /**
     * Enables classic JSON compatibility
     */
    void jsonCompatible(void);

    /**
     * Enables minimum storage usage
     */
    void minumumStorage(void);
  };
}
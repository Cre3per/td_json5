#include "CJSONString.hpp"

namespace json5
{

  /**
   * 
   */
  std::string CJSONString::toString(const CFormatOptions &fmt) const
  {
    std::string strResult{ "" };
    std::string strValue{ CUString{ this->value }.escape(fmt.bASCII).str() };

    for (size_t s{ 0 }; s < strValue.length(); ++s)
    {
      if (strValue.at(s) == fmt.chQuotationMark)
      {
        strValue.replace(s, 1, std::string{ "\\" } + fmt.chQuotationMark);
        ++s;
      }
    }

    strResult += fmt.chQuotationMark;

    strResult += strValue;

    strResult += fmt.chQuotationMark;

    return strResult;
  }

  /**
   * 
   */
  CJSONString::CJSONString(void)
      : value{ "" }
  {
  }

  /**
   * 
   */
  CJSONString::CJSONString(const string_type &str)
      : value{ str }
  {
  }

  /**
   * 
   */
  CJSONString::~CJSONString(void)
  {
  }
}
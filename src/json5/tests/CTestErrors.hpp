#pragma once

#include "CTestHost.hpp"

namespace json5::test::errors
{
  class CTestErrors : public CTestHost
  {
  private:

  public:

    /**
     * 
     */
    virtual bool run(int *pSuccess, int *pFail) override;

    /**
     * Destructor
     */
    virtual ~CTestErrors(void);
  };
}
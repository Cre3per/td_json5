#pragma once

#include "json5/CFormatOptions.hpp"

#include <memory>

namespace json5
{
  class CJSONBoolean;
  class CJSONArray;
  class CJSONObject;

  enum class JSONValueType
  {
    kBoolean,
    kNumber,
    kString,
    kArray,
    kObject,

    kUnknown
  };

  class CJSONValue
  {
  private:
  public:

    /**
     * @return True if this value a null
     */
    bool isNull(void) const;

    /**
     * @return True if this value is a boolean
     */
    bool isBoolean(void) const;

    /**
     * @return True if this value is a number
     */
    bool isNumber(void) const;

    /**
     * @return True if this value is a string
     */
    bool isString(void) const;

    /**
     * @return True if this value is an array
     */
    bool isArray(void) const;

    /**
     * @return True if this value is an object
     */
    bool isObject(void) const;

    /**
     * @return Value type
     */
    JSONValueType getType(void) const;

    /**
     * @param fmt Formatting options
     * @return String representation of this object
     */
    virtual std::string toString(const CFormatOptions &fmt) const = 0;

    /**
     * Uses default formatting options
     * @return String representation of this object
     */
    std::string toString(void) const;

  public:
    /**
     * 
     */
    virtual ~CJSONValue(void);

  public:
    /**
     * @param type Type to get the name of
     * @return Name of the type
     */
    static const char *typeName(JSONValueType type);
  };
}
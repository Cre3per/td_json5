#pragma once

#include "json5/unicode/CUString.hpp"

namespace json5
{
  class CStringStream
  {
  private:
    /**
     * The input string
     */
    CUString m_strSource{ "" };

    /**
     * Current character index in {@link m_strSource}
     */
    int m_iPosition{ 0 };

    /**
     * Current line, starting at 1
     */
    int m_iLine{ 1 };

    /**
     * Current column, starting at 1
     */
    int m_iColumn{ 1 };

  public:

    /**
     * @return True if the end of input has been reached
     */
    bool eof(void) const;

    /**
     * @return The code point of the next character 
     */
    char32_t peek(void) const;

    /**
     * Reads the next character and moves on.
     * @return The code point of the next character
     */
    char32_t read(void);

    /**
     * @return Current line, starting at 1
     */
    int getLine(void) const;

    /**
     * @return Current column, starting at 1
     */
    int getColumn(void) const;

    /**
     * Excerpts characters from the input source
     * @param iLeading How many characters before the current character should
     *                 be shown
     * @param iTrailing How many characters after the current character should
     *                  be shown
     * @return
     */
    CUString excerpt(int iLeading = 3, int iTrailing = 3) const;

    /**
     * Resets the stream to it's initial state.
     * @param strText Text to serve
     */
    void reset(const CUString &strText);

  public:

    /**
     * Default constructor
     */
    CStringStream(void);

    /**
     * Constructor
     * @param strText Text to serve
     */
    CStringStream(const CUString &strText);
  };
}
#pragma once

namespace json5
{
  /**
   * Parser- and lexer states
   */
  enum class State
  {
    kDefault,
    kAfterArrayValue,
    kAfterPropertyName,
    kAfterPropertyValue,
    kBeforeArrayValue,
    kBeforePropertyName,
    kBeforePropertyValue,
    kComment,
    kCommentMultiLine,
    kCommentMultiLineAsterisk,
    kDecimalExponent,
    kDecimalExponentInteger,
    kDecimalExponentSign,
    kDecimalFraction,
    kDecimalInteger,
    kDecimalPoint,
    kDecimalPointLeading,
    kHexadecimal,
    kHexadecimalInteger,
    kIdentifierName,
    kIdentifierNameEscape,
    kIdentifierNameStartEscape,
    kSign,
    kSingleLineComment,
    kStart,
    kString,
    kValue,
    kZero,

    kEnd,

    kInvalid
  };
}
#pragma once

#include <stdexcept>
#include <string>

namespace json5
{
  class CTextException : public std::exception
  {
  protected:

    /**
     * Display text
     */
    std::string m_strMessage{ "" };
  public:

    /**
     * 
     */
    virtual const char *what(void) const throw() override;

  public:

    /**
     * Default constructor
     */
    CTextException(void);

    /**
     * Constructor
     * @param strMessage Message
     */
    CTextException(const std::string &strMessage);

    /**
     * Destructor
     */
    virtual ~CTextException(void);
  };
}
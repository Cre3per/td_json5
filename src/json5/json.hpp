#pragma once

#include "types.hpp"
#include "CFormatOptions.hpp"

#include <string>

namespace json5
{
  class CJSON
  {
  private:
  public:
    /**
     * Parses a JSON string, constructing the value or object described by the
     * string.
     * @param strSource The JSON string
     * @return The parsed value or object
     */
    var parse(const std::string &strSource) const;

    /**
     * Converts a value or object to a JSON string.
     * @param obj The value or object to convert
     * @return The JSON string
     */
    std::string stringify(var obj) const;

    /**
     * Converts a value or object to a JSON string.
     * @param obj The value or object to convert
     * @param fmt Formatting options
     * @return The JSON string
     */
    std::string stringify(var obj, const CFormatOptions &fmt) const;
  };

  extern CJSON JSON;
}
#pragma once

#include "json5/values/CJSONValue.hpp"

#include <memory>

namespace json5
{
  enum class TokenType
  {
    kBoolean,
    kIdentifier,
    kNull,
    kNumeric,
    kPunctuator,
    kString,

    kEOF,
    
    kInvalid
  };

  class CToken
  {
  private:

    /**
     * Type
     */
    TokenType m_type{ TokenType::kEOF };

    /**
     * Value
     */
    std::shared_ptr<CJSONValue> m_pValue{ nullptr };

    /**
     * Line
     */
    int m_iLine{ 0 };

    /**
     * Column
     */
    int m_iColumn{ 0 };

  public:

    /**
     * @return Type
     */
    TokenType getType(void) const;

    /**
     * @return Value
     */
    std::shared_ptr<CJSONValue> getValue(void);

  public:

    /**
     * 
     */
    bool operator==(const CToken &that) const;

  public:

    /**
     * Default constructor
     */
    CToken(void);

    /**
     * Constructor
     * @param type Type
     * @param pValue Value
     * @param iLine Line
     * @param iColumn Column
     */
    CToken(TokenType type, std::shared_ptr<CJSONValue> pValue, int iLine, int iColumn);

  public:

    static CToken invalid(void);
  };
}
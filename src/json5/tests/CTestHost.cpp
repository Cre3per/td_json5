#include "CTestHost.hpp"

namespace json5::test
{

  /**
   * 
   */
  void CTestHost::addTest(const CTest &test)
  {
    this->m_vTests.push_back(test);
  }

  /**
   * 
   */
  bool CTestHost::run(int *pSuccess, int *pFail)
  {
    bool bAllSuccessful{ true };

    for (auto &test : this->m_vTests)
    {
      if (test.run())
      {
        ++(*pSuccess);
      }
      else
      {
        ++(*pFail);
        bAllSuccessful = false;
      }
    }

    return bAllSuccessful;
  }


  /**
   * 
   */
  CTestHost::~CTestHost(void)
  {
    
  }
}
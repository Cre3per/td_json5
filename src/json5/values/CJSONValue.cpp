#include "CJSONValue.hpp"

#include <cassert>

#include "CJSONArray.hpp"
#include "CJSONBoolean.hpp"
#include "CJSONNull.hpp"
#include "CJSONNumber.hpp"
#include "CJSONObject.hpp"
#include "CJSONString.hpp"

namespace json5
{
  /**
   * 
   */
  bool CJSONValue::isNull(void) const
  {
    return (dynamic_cast<const CJSONNull *>(this) != nullptr);
  }

  /**
   * 
   */
  bool CJSONValue::isBoolean(void) const
  {
    return (dynamic_cast<const CJSONBoolean *>(this) != nullptr);
  }

  /**
   * 
   */
  bool CJSONValue::isNumber(void) const
  {
    return (dynamic_cast<const CJSONNumber *>(this) != nullptr);
  }

  /**
   * 
   */
  bool CJSONValue::isString(void) const
  {
    return (dynamic_cast<const CJSONString *>(this) != nullptr);
  }

  /**
   * 
   */
  bool CJSONValue::isArray(void) const
  {
    return (dynamic_cast<const CJSONArray *>(this) != nullptr);
  }

  /**
   * 
   */
  bool CJSONValue::isObject(void) const
  {
    return (dynamic_cast<const CJSONObject *>(this) != nullptr);
  }

  /**
   * 
   */
  JSONValueType CJSONValue::getType(void) const
  {
    if (this->isBoolean())
    {
      return JSONValueType::kBoolean;
    }
    else if (this->isNumber())
    {
      return JSONValueType::kNumber;
    }
    else if (this->isString())
    {
      return JSONValueType::kString;
    }
    else if (this->isArray())
    {
      return JSONValueType::kArray;
    }
    else if (this->isObject())
    {
      return JSONValueType::kObject;
    }
    else
    {
      return JSONValueType::kUnknown;
    }
  }

  /**
   * 
   */
  std::string CJSONValue::toString(void) const
  {
    static CFormatOptions fmt{};

    return this->toString(fmt);
  }

  /**
   * 
   */
  CJSONValue::~CJSONValue(void)
  {
  }

  /**
   *
   */
  const char *CJSONValue::typeName(JSONValueType type)
  {
    switch (type)
    {
    case JSONValueType::kBoolean:
      return "boolean";
      break;
    case JSONValueType::kNumber:
      return "number";
      break;
    case JSONValueType::kString:
      return "string";
      break;
    case JSONValueType::kArray:
      return "array";
      break;
    case JSONValueType::kObject:
      return "object";
      break;
    default:
      return "unknown";
      break;
    }
  }
}
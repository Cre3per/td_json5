#pragma once

#include <string>

namespace json5
{
  /**
   * Null-terminated UTF-8 string.
   * Allows null characters.
   * An @std::u32string may be faster, but it uses 4 bytes even for ASCII
   * characters, which is pretty wasteful.
   */
  class CUString
  {
  private:
    /**
     * Capacity in bytes, including the null-terminator
     */
    int m_iCapacity{ 0 };

    /**
     * Number of occupied bytes, including the null-terminator
     */
    int m_iSize{ 0 };

    /**
     * Length in characters
     */
    int m_iLength{ 0 };

    /**
     * Raw data, including the null-terminator
     */
    char *m_data{ nullptr };

  private:
    /**
     * Calculates the length
     */
    void calcLength(void);

  public:
    /**
     * @return Raw buffer
     */
    const char *c_str(void) const;

    /**
     * @return String
     */
    std::string str(void) const;

    /**
     * Sets the strings capacity to at least @iCapacity
     * @param iCapacity New minimum capacity
     */
    void reserve(int iCapacity);

    /**
     * Shrinks the capacity to match the used size.
     */
    void shrink_to_fit(void);

    /**
     * Appends another string
     * @param str The string to append
     * @return this
     */
    CUString &append(const CUString &str);

    /**
     * Appends a character
     * @param ch The character to append
     * @return this
     */
    CUString &append(char ch);

    /**
     * Appends a character
     * @param ch The character to append
     * @return this
     */
    CUString &append(char32_t ch);

    /**
     * @param iIndex Character index
     * @return Byte index at @iIndex
     */
    int characterToByteIndex(int iIndex) const;

    /**
     * @param iIndex Byte index
     * @return Character index at @iIndex
     */
    int byteToCharacterIndex(int iIndex) const;

    /**
     * @param iIndex Character index
     * @return Code point of the character at @iIndex
     */
    char32_t at(int iIndex) const;

    /**
     * @param iIndex Byte index
     * @return Byte at @iIndex
     */
    char &byteAt(int iIndex);
    const char &byteAt(int iIndex) const;

    /**
     * @param iIndex Character index
     * @return Byte at @iIndex
     */
    char &byteAtChar(int iIndex);
    const char &byteAtChar(int iIndex) const;

    /**
     * @return Length in characters
     */
    int length(void) const;

    /**
     * @return Capacity in bytes
     */
    int capacity(void) const;

    /**
     * @return Size in bytes, including the null terminator
     */
    int size(void) const;

    /**
     * Clears the string.
     */
    void clear(void);

    /**
     * @return True if the string is empty
     */
    bool empty(void) const;

    /**
     * Escapes escape characters
     * @param bFullASCII Escape all escape and non-ascii characters
     * @return *this
     */
    CUString &escape(bool bFullASCII = false);

  public:
    // TODO: Overload operators

    /**
     * 
     */
    CUString &operator=(const CUString &that);

    /**
     * 
     */
    bool operator==(const CUString &that) const;

    /**
     * 
     */
    bool operator!=(const CUString &that) const;

    /**
     * 
     */
    bool operator<(const CUString &that) const;

  public:
    /**
     * Default constructor
     */
    CUString(void);

    /**
     * Copy constructor
     */
    CUString(const CUString &that);

    /**
     * Constructor
     * @param szText Text
     */
    CUString(const char *szText);

    /**
     * Constructor
     * @param szText Text
     * @param iSize Size of the text in bytes, including the null-terminator.
     */
    CUString(const char *szText, int iSize);

    /**
     * Constructor
     * @param strText Text
     */
    CUString(const std::string &strText);

    /**
     * Constructor
     * @parm ch A code point. Will be encoded.
     */
    CUString(char32_t ch);

    /**
     * Destructor
     */
    virtual ~CUString(void);

  public:
    /**
     * @param ch First byte of a utf-8 character
     * @return How many bytes (in total) this character takes up. 0 if this byte
     *         is only a trailing part of a character.
     */
    static int getCharLength(char ch);

    /**
     * @param ch A utf-8 character
     * @return How many bytes (in total) this character takes up. 0 if this byte
     *         is only a trailing part of a character.
     */
    static int getCharLength(char32_t ch);

    /**
     * @param ch Encoded character to get the code point of
     * @return Code point of @ch
     */
    static int getCodePoint(char32_t ch);

    /**
     * @param ch A code point
     * @return The encoded character as a string
     */
    static std::string encodeCodePoint(char32_t ch);
  };
}